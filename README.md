# Unreal Engine Scripts

These scripts are designed to help you build and test Unreal Engine projects and plugins, as well as build custom versions of Unreal Engine to distribute to other team members.

## Getting started

To learn how to use these scripts [check out the wiki](https://gitlab.com/redpointgames/unreal-engine-scripts/-/wikis/home):

- [Setup guide for a game or project](https://gitlab.com/redpointgames/unreal-engine-scripts/-/wikis/Setup-guide-for-a-game-or-project)
- [Setup guide for a plugin](https://gitlab.com/redpointgames/unreal-engine-scripts/-/wikis/Setup-guide-for-a-plugin)
- [Setup guide for a custom engine](https://gitlab.com/redpointgames/unreal-engine-scripts/-/wikis/Setup-guide-for-a-custom-engine)

## Commercial support

These scripts are MIT licensed and available free of charge. However, if you're a studio or company and would like next business day support in using these scripts on your own projects, [take a look at our paid support plans](https://licensing.redpoint.games/support/plans).

## Features for projects

✔️ Build and package Unreal Engine games for multiple platforms with just one command.<br />
✔️ Uses BuildGraph to [massively accelerate builds](https://gitlab.com/redpointgames/unreal-engine-scripts/-/wikis/uploads/c54f09c823fab23d3c1864f9ec9a7970/image.png) and [Gauntlet tests](https://gitlab.com/redpointgames/unreal-engine-scripts/-/wikis/uploads/a26fec6b9c7d1faa1f71ce06365ce894/image.png) on GitLab across multiple build agents.<br />
✔️ Optimized "git clone" process for builds on GitLab specifically for Unreal Engine projects.<br />
✔️ Handles patching BuildGraph to make it work with installed engines.<br />
✔️ Run Gauntlet tests across devices for your project.<br />
✔️ Handles patching Gauntlet to fix platform support and various bugs.<br />
✔️ Handles rebuilding Gauntlet with your test script. Even works with Unreal Engine installed from the launcher.<br />
✔️ Format your source code with clang-format to be compliant with Unreal Engine code formatting standards.

## Features for plugins

✔️ Build and package Unreal Engine plugins for multiple platforms with just one command.<br />
✔️ Uses BuildGraph to massively accelerate builds and tests on GitLab across multiple build agents.<br />
✔️ Optimized "git clone" process for builds on GitLab specifically for Unreal Engine plugins.<br />
✔️ Run automation tests for your plugins, with automatic parallelization on the same machine.<br />
✔️ Test results compatible with GitLab.<br />
✔️ Packages your plugin in an Unreal Engine Marketplace compliant way.<br />
✔️ Upload your plugin to Backblaze B2 for Marketplace submission.<br />
✔️ Update copyright headers in all your C++ and C# files for consistency.<br />
✔️ Format your source code with clang-format to be compliant with Unreal Engine code formatting standards.

## Features for custom engines

✔️ Build custom versions of Unreal Engine for multiple platforms with just one command.<br />
✔️ Uses BuildGraph to massively accelerate builds and tests on GitLab across multiple build agents.<br />
✔️ Optimized "git clone" process for builds on GitLab specifically for Unreal Engine.<br />
✔️ Patches Unreal Engine to fix bugs in remote macOS/IOS compilation and the Android platform.<br />
✔️ Patches Unreal Engine to add support for "Sign in with Apple" on iOS.<br />
✔️ Patches Unreal Engine to add support running macOS Gauntlet tests from Windows.<br />
✔️ Patches Unreal Engine to add support running iOS Gauntlet tests from Windows.

## License

These scripts are made available under an MIT license, copyright Redpoint Games. Feel free to use them in any kind of project, commercial or otherwise.
