param(
    # The base path that all the input files are relative to.
    [Parameter(Mandatory=$true)][string] $InputBasePath,
    # The file that contains the list of input files.
    [Parameter(Mandatory=$true)][string] $InputFileList,
    # The base path to output the potentially mutated files to.
    [Parameter(Mandatory=$true)][string] $OutputPath,
    # Turn on this switch if this plugin is for Marketplace submission.
    [switch][bool] $IsForMarketplace
)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Internal_SetupEnvironment.ps1

$InputFiles = Get-Content $InputFileList
$FileMappings = @()
$InputBasePath = $InputBasePath.Replace("/", "\")
$OutputPath = $OutputPath.Replace("/", "\")
foreach ($InputFile in $InputFiles) {
    $InputFile = $InputFile.Replace("/", "\")
    if (!$InputFile.StartsWith($InputBasePath)) {
        Write-Error "Invalid input file list!"
        exit 1
    }
    $RelativeInputFile = $InputFile.Substring($InputBasePath.Length)
    $FileMappings += @{
        Input = $InputFile;
        Output = ($OutputPath + "\" + $RelativeInputFile);
    }
}

if ($IsForMarketplace) {
    # Leave files as-is, since the Marketplace needs to be able to build the plugin.
    $FileMappings | % {
        if (!(Test-Path "$($_.Output)\..")) {
            New-Item -ItemType Directory -Path "$($_.Output)\.." | Out-Null
        }
        Copy-Item -Force -Path "$($_.Input)" -Destination "$($_.Output)"
        Write-Host "copied: $($_.Input) -> $($_.Output)"
    }
} else {
    # Mutate each file as we copy it, so that recipients see the plugin as precompiled.
    $FileMappings | % {
        $BuildCs = Get-Content -Raw "$($_.Input)"
        $BuildCs = $BuildCs.Replace("bUsePrecompiled = false;", "bUsePrecompiled = true;")
        while ($BuildCs.Contains("/* PRECOMPILED REMOVE BEGIN */")) {
            if (!$BuildCs.Contains("/* PRECOMPILED REMOVE END */")) {
                Write-Host "error: Missing PRECOMPILED REMOVE END after PRECOMPILED REMOVE BEGIN in $ProjectRoot/$PackagingFolder/Source/$_ !"
                exit 1
            }
            $NewBuildCs = $BuildCs -replace "(?ms)^\s+/\* PRECOMPILED REMOVE BEGIN \*/.*?/\* PRECOMPILED REMOVE END \*/", ""
            if ($NewBuildCs -eq $BuildCs) {
                Write-Host "error: Did not make progress filtering out PRECOMPILED REMOVE sections in $ProjectRoot/$PackagingFolder/Source/$_ !"
                exit 1
            }
            $BuildCs = $NewBuildCs
        }
        if (!(Test-Path "$($_.Output)\..")) {
            New-Item -ItemType Directory -Path "$($_.Output)\.." | Out-Null
        }
        Set-Content -Path "$($_.Output)" -Value $BuildCs
        Write-Host "mutated: $($_.Input) -> $($_.Output)"
    }
}

exit 0