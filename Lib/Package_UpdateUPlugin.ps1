param(
    # The path to the input .uplugin file.
    [Parameter(Mandatory=$true)][string] $InputPath,
    # The path to the output .uplugin file.
    [Parameter(Mandatory=$true)][string] $OutputPath,
    # The engine version string to set in the .uplugin file.
    [Parameter(Mandatory=$true)][string] $EngineVersion,
    # The version number to set in the .uplugin file.
    [Parameter(Mandatory=$true)][string] $VersionName,
    # The version string to set in the .uplugin file.
    [Parameter(Mandatory=$true)][string] $VersionNumber,
    # Turn on this switch if this plugin is for Marketplace submission.
    [switch][bool] $IsForMarketplace
)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Internal_SetupEnvironment.ps1

[Reflection.Assembly]::LoadFile("$PSScriptRoot\Newtonsoft.Json.dll") | Out-Null

$Json = (Get-Content $InputPath | Out-String)
$Plugin = [Newtonsoft.Json.Linq.JObject]::Parse($Json)

$Plugin["EngineVersion"] = [Newtonsoft.Json.Linq.JToken]::Parse("`"$EngineVersion`"")
$Plugin["VersionName"] = [Newtonsoft.Json.Linq.JToken]::Parse("`"$VersionName`"")
$Plugin["Version"] = [Newtonsoft.Json.Linq.JToken]::Parse("$VersionNumber")
$Plugin["Installed"] = [Newtonsoft.Json.Linq.JToken]::Parse("true")

if (!$IsForMarketplace) {
    $Plugin["EnabledByDefault"] = [Newtonsoft.Json.Linq.JToken]::Parse("false")
} else {
    $Plugin.Remove("EnabledByDefault") | Out-Null

    foreach ($Module in $Plugin["Modules"]) {
        if ($null -eq $Module["WhitelistPlatforms"] -and $null -eq $Module["BlacklistPlatforms"]) {
            # Required by Marketplace Guidelines, even though it can be empty to allow all platforms.
            $Module["BlacklistPlatforms"] = [Newtonsoft.Json.Linq.JToken]::Parse("[]")
        }
    }
}

if (!(Test-Path "$OutputPath\..")) {
    New-Item -ItemType Directory -Path "$OutputPath\.." | Out-Null
}
Set-Content -Force -Path $OutputPath -Value ($Plugin.ToString())