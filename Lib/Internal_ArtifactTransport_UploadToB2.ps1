param(
    [Parameter(Mandatory=$true)][string] $SharedStorageFolderName,
    [Parameter(Mandatory=$true)][string] $RelativePath
)

$ErrorActionPreference = "Stop"

$B2Command = Get-Command -Name "b2" -ErrorAction "SilentlyContinue"
if ($B2Command -eq $null){
    Write-Error "The b2 command is not available on this system, but is required for artifact transport. Please install the B2 Command Line Tools. On Windows you can install these with 'pip install --upgrade b2' (after installing Python 3.9). On macOS you can install these with 'brew install b2-tools'."
    exit 1
}

if ($env:ARTIFACT_BACKBLAZE_B2_KEY_ID -eq $null -or
    $env:ARTIFACT_BACKBLAZE_B2_APPLICATION_KEY -eq $null -or
    $env:ARTIFACT_BACKBLAZE_B2_BUCKET_NAME -eq $null) {
    Write-Error "Missing one or more of these environment variables: ARTIFACT_BACKBLAZE_B2_KEY_ID, ARTIFACT_BACKBLAZE_B2_APPLICATION_KEY, ARTIFACT_BACKBLAZE_B2_BUCKET_NAME."
}

$LocalRelativePath = $RelativePath
$RemoteRelativePath = $RelativePath
$RemoteRelativePath = $RemoteRelativePath.Replace("\","/")
if ($RemoteRelativePath.StartsWith("./")) {
    $RemoteRelativePath = $RemoteRelativePath.Substring(2)
}
if ($RemoteRelativePath.StartsWith("/")) {
    $RemoteRelativePath = $RemoteRelativePath.Substring(1)
}

$env:B2_ACCOUNT_INFO = Join-Path -Path (Resolve-Path -Path ".") -ChildPath ".b2.db"
try {
    Write-Output "Authorizing B2..."
    b2 authorize-account "$env:ARTIFACT_BACKBLAZE_B2_KEY_ID" "$env:ARTIFACT_BACKBLAZE_B2_APPLICATION_KEY"
    if ($LASTEXITCODE -ne 0) {
        exit $LASTEXITCODE
    }

    while ($true) {
        if (Test-Path "b2.log") {
            Remove-Item -Force -Path "b2.log"
        }
        Write-Output "Syncing artifacts up to B2..."
        b2 sync --replaceNewer "$LocalRelativePath" "b2://$env:ARTIFACT_BACKBLAZE_B2_BUCKET_NAME/$SharedStorageFolderName/$RemoteRelativePath" | Tee-Object "b2.log"
        if ($LASTEXITCODE -ne 0) {
            exit $LASTEXITCODE
        }
        if (Test-Path "b2.log") {
            if ((Get-Content -Raw -Path "b2.log").Contains("sync is incomplete")) {
                Write-Host "Sync is incomplete, restarting sync..."
                continue
            }
        }
        break
    }
} finally {
    if (Test-Path $env:B2_ACCOUNT_INFO) {
        Write-Output "Cleaning up B2 authentication file..."
        Remove-Item -Force -Path $env:B2_ACCOUNT_INFO
    }
}

exit 0