param(
    # The distribution name, as it appears in BuildConfig.json.
    [Parameter(Mandatory=$true)][string] $Distribution,
    # If true, allows Visual Studio 2019 to be used to build the engine.
    [switch][bool] $Allow2019
)

. $PSScriptRoot\Internal_SetupEnvironment.ps1

$BuildConfig = Get-BuildConfig
$DistributionConfig = Get-Distribution $Distribution

# We map the engine to a drive letter for the duration of the run, to avoid issues
# with long paths.
$local:ShortHash = "W" + (Get-StringHash -InputString "$local:Distribution-$($null)" -Length 4)
$RealEnginePath = "$ProjectRoot\$local:ShortHash\BuildScripts\Temp\UE"
if (!(Test-Path "$RealEnginePath")) {
    New-Item -ItemType Directory -Path "$RealEnginePath" | Out-Null
}
ls function:[d-z]: -n | % { subst $_ /D | Out-Null }
$EnginePath = (ls function:[d-z]: -n | ?{ !(test-path $_) } | select -Last 1)
try {
    Write-Output "Mapping real engine path $RealEnginePath to virtual drive $EnginePath..."
    subst $EnginePath "$RealEnginePath"
    if (!(Test-Path $EnginePath)) {
        Write-Error "Unable to map real engine path to virtual drive!"
        exit 1
    }
    
    # Prepare the engine. We always do this (even on every step of GitLab)
    # because this is what clones the engine and patches it for the build.
    & "$PSScriptRoot\Prepare_Engine.ps1" `
        -Distribution $Distribution `
        -EnginePath $EnginePath

    # Patch BuildGraph. We do this before computing settings, since BuildGraphSettings_Engine.ps1
    # will invoke BuildGraph to detect all of the available platforms.
    & "$PSScriptRoot\Patch_BuildGraph.ps1" -Engine "$EnginePath"
    if ($LastExitCode -ne 0) {
        exit $LastExitCode
    }

    # Determine if the engine is Unreal Engine 5.
    $local:IsUnrealEngine5 = $false
    if ($global:IsMacOS -or $global:IsLinux) {
        $local:RunUAT = Get-Content -Raw -Path "$EnginePath/Engine/Build/BatchFiles/RunUAT.sh"
        if ($local:RunUAT.Contains("Unreal Engine AutomationTool setup script")) {
            $local:IsUnrealEngine5 = $true
        }
    } else {
        $local:RunUAT = Get-Content -Raw -Path "$EnginePath\Engine\Build\BatchFiles\RunUAT.bat"
        if ($local:RunUAT.Contains("SET_TURNKEY_VARIABLES")) {
            $local:IsUnrealEngine5 = $true
        }
    }

    # Compute the BuildGraph settings.
    . $PSScriptRoot\BuildGraphSettings_Engine.ps1
    $Settings = Get-BuildGraphSettingsForEngine $DistributionConfig $EnginePath $Allow2019 $local:IsUnrealEngine5

    # BuildGraph in Unreal Engine 5.0 causes input files to be unnecessarily modified. Just allow mutation since I'm not sure what the bug is.
    $env:BUILD_GRAPH_ALLOW_MUTATION="true"

    # Execute BuildGraph.
    $BuildGraphArgsArray = @()
    $Settings.Windows.Keys | % {
        $BuildGraphArgsArray += "-set:$_=`"$($Settings.Windows.Item($_).Replace("__REPOSITORY_ROOT__", "$ProjectRoot").Replace("/", "\").TrimEnd("\"))`""
    }
    taskkill /f /t /im AutomationToolLauncher.exe | Out-Null
    foreach ($Value in $BuildGraphArgsArray) {
        Write-Host $Value
    }
    while ($true) {
        & "$PSScriptRoot\Internal_RunUAT.ps1" -UATEnginePath "$EnginePath" BuildGraph -Script="$EnginePath\Engine\Build\InstalledEngineBuild.xml" -Target="Make Installed Build Win64" $BuildGraphArgsArray | Tee-Object -FilePath $ProjectRoot\BuildScripts\Temp\Build.log
        if ($LASTEXITCODE -ne 0) {
            $BuildLogLines = (Get-Content "$ProjectRoot\BuildScripts\Temp\Build.log")
            if ($BuildLogLines -is [string]) {
                $BuildLogLines = @($BuildLogLines)
            }
            $PCHRetryOn = $true
            $NeedsRetry = $false
            for ($i = 0; $i -lt $BuildLogLines.Length; $i++) {
                if ($BuildLogLines[$i].Contains("error C3859") -and $PCHRetryOn) {
                    $NeedsRetry = $true
                    break
                }
                if ($BuildLogLines[$i].Contains("@buildgraph PCH-RETRY-ON")) {
                    $PCHRetryOn = $true
                }
                if ($BuildLogLines[$i].Contains("@buildgraph PCH-RETRY-OFF")) {
                    $PCHRetryOn = $false
                }
            }
            if ($NeedsRetry) {
                Write-Warning "Detected PCH memory error. Automatically retrying..."
                continue
            }
            exit $LASTEXITCODE
        } else {
            break
        }
    }
    exit $LastExitCode
} finally {
    Write-Output "Unmapping virtual drive $EnginePath..."
    subst $EnginePath /D
}