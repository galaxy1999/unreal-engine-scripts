param(
    [string]$UATEnginePath
)

$global:ErrorActionPreference = "Stop"

trap {
    Write-Error $_.ToString()
    exit 1
}

#
# This is a simple wrapper script around RunUAT.bat that deals with a bug
# in Unreal Engine 5 where the Turnkey scripts cause the exit code to always
# be 0.
#

$ExtraArgs = ""
$local:IsUnrealEngine5 = $false
if ($global:IsMacOS -or $global:IsLinux) {
    if (!(Test-Path "$UATEnginePath/Engine/Build/BatchFiles/RunUAT.sh")) {
        Write-Host "The RunUAT.sh script does not exist at: $UATEnginePath/Engine/Build/BatchFiles/RunUAT.sh"
        exit 1
    }
    $RunUAT = Get-Content -Raw -Path "$UATEnginePath/Engine/Build/BatchFiles/RunUAT.sh"
    if ($RunUAT.Contains("Unreal Engine AutomationTool setup script")) {
        $local:IsUnrealEngine5 = $true
    }
}
else {
    if (!(Test-Path "$UATEnginePath\Engine\Build\BatchFiles\RunUAT.bat")) {
        Write-Host "The RunUAT.sh script does not exist at: $UATEnginePath/Engine/Build/BatchFiles/RunUAT.sh"
        exit 1
    }
    $RunUAT = Get-Content -Raw -Path "$UATEnginePath\Engine\Build\BatchFiles\RunUAT.bat"
    if ($RunUAT.Contains("SET_TURNKEY_VARIABLES")) {
        $ExtraArgs = "-noturnkeyvariables"
        $local:IsUnrealEngine5 = $true
    }
}

function Expand-Array($In) {
    $R = @()
    foreach ($I in $In) {
        if ($I -is [array]) {
            foreach ($E in (Expand-Array $I)) {
                $R += $E
            }
        } else {
            $R += $I
        }
    }
    return $R
}

$FinalArgs = @()
$DoScriptWorkaround = $false
foreach ($local:Arg in (Expand-Array $args)) {
    if ($local:IsUnrealEngine5 -and $local:Arg.StartsWith("-Script=") -and $local:Arg.EndsWith("Engine\Build\InstalledEngineBuild.xml")) {
        # Hack for UE5 engine builds that don't include console platforms properly
        # if the script is an absolute path.
        $FinalArgs += ("-Script=Engine/Build/InstalledEngineBuild.xml")
        $DoScriptWorkaround = $true
    }
    elseif ($local:IsUnrealEngine5 -and $local:Arg.StartsWith("-Script=")) {
        $FinalArgs += ($local:Arg.Replace("\", "/"))
    }
    elseif ($global:IsMacOS -or $global:IsLinux) {
        if ($Arg.StartsWith("-set__")) {
            $FinalArgs += ("-set:" + $local:Arg.Substring("-set__".Length))
        }
        else {
            $FinalArgs += $Arg
        }
    }
    else {
        $FinalArgs += $Arg
    }
}
if ($ExtraArgs -ne "") {
    $FinalArgs += $ExtraArgs
}
$FinalArgsString = $FinalArgs -join " "

if ($IsUnrealEngine5) {
    # We have to set up BuildConfiguration.xml so that we uncap the processor and memory requirements. We already handle automatic retries at the build server level, which is much more efficient that arbitrarily capping it at the UBT level.
    $XmlConfigFilePath = [System.IO.Path]::Combine([System.Environment]::GetFolderPath("ApplicationData"), "Unreal Engine", "UnrealBuildTool", "BuildConfiguration.xml")
    Write-Host "Build configuration is located at: $XmlConfigFilePath"
    $XmlConfigDirectoryPath = [System.IO.Path]::GetDirectoryName($XmlConfigFilePath)
    if (!(Test-Path -Path $XmlConfigDirectoryPath)) {
        Write-Host "Creating required directory for build configuration"
        New-Item -ItemType Directory $XmlConfigDirectoryPath
    }
    if ((Test-Path -Path $XmlConfigFilePath) -and !(Test-Path -Path "$XmlConfigFilePath.backup")) {
        Write-Host "Moving existing build configuration file out of the way"
        Move-Item -Force -Path $XmlConfigFilePath -Destination "$XmlConfigFilePath.backup"
    }
    try {
        Write-Host "Configured build to maximize core and memory usage by updating: $XmlConfigFilePath"
        $ProcessorMultiplier = "2"
        if ($global:IsMacOS) {
            # macOS doesn't do hyper-threading, don't overload the CPU here.
            $ProcessorMultiplier = "1"
        }
        Set-Content -Path $XmlConfigFilePath -Value @"
<?xml version="1.0" encoding="utf-8" ?>
<Configuration xmlns="https://www.unrealengine.com/BuildConfiguration">
    <TaskExecutor>
        <ProcessorCountMultiplier>$ProcessorMultiplier</ProcessorCountMultiplier>
        <MemoryPerActionBytes>0</MemoryPerActionBytes>
        <bShowCompilationTimes>true</bShowCompilationTimes>
    </TaskExecutor>
    <ParallelExecutor>
        <ProcessorCountMultiplier>$ProcessorMultiplier</ProcessorCountMultiplier>
        <MemoryPerActionBytes>0</MemoryPerActionBytes>
        <bShowCompilationTimes>true</bShowCompilationTimes>
    </ParallelExecutor>
</Configuration>
"@
        if ($global:IsMacOS -or $global:IsLinux) {
            Write-Host "Invoking: `"$UATEnginePath/Engine/Build/BatchFiles/RunUAT.sh`" $FinalArgsString"
            if ($DoScriptWorkaround) {
                Push-Location $UATEnginePath
                try {
                    & "$UATEnginePath/Engine/Build/BatchFiles/RunUAT.sh" $FinalArgs
                }
                finally {
                    Pop-Location
                }
            }
            else {
                & "$UATEnginePath/Engine/Build/BatchFiles/RunUAT.sh" $FinalArgs
            }
        }
        else {
            Write-Host "Invoking: `"$UATEnginePath\Engine\Build\BatchFiles\RunUAT.bat`" $FinalArgsString"
            if ($DoScriptWorkaround) {
                Push-Location $UATEnginePath
                try {
                    & "$UATEnginePath\Engine\Build\BatchFiles\RunUAT.bat" $FinalArgs
                }
                finally {
                    Pop-Location
                }
            }
            else {
                & "$UATEnginePath\Engine\Build\BatchFiles\RunUAT.bat" $FinalArgs
            }
        }
        exit $LASTEXITCODE
    }
    finally {
        if (Test-Path -Path $XmlConfigFilePath) {
            Remove-Item -Force $XmlConfigFilePath
        }
        if (Test-Path -Path "$XmlConfigFilePath.backup") {
            Write-Host "Moved back existing BuildConfiguration.xml"
            Move-Item -Force -Path "$XmlConfigFilePath.backup" -Destination $XmlConfigFilePath
        }

        # Also, kill any dotnet.exe processes that belong to the engine.

    }
}
else {
    if ($global:IsMacOS -or $global:IsLinux) {
        Write-Host "Invoking: `"$UATEnginePath/Engine/Build/BatchFiles/RunUAT.sh`" $FinalArgsString"
        & "$UATEnginePath/Engine/Build/BatchFiles/RunUAT.sh" $FinalArgs
    }
    else {
        Write-Host "Invoking: `"$UATEnginePath\Engine\Build\BatchFiles\RunUAT.bat`" $FinalArgsString"
        & "$UATEnginePath\Engine\Build\BatchFiles\RunUAT.bat" $FinalArgs
    }
    exit $LASTEXITCODE
}