param([string] $Engine)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Internal_SetupEnvironment.ps1

Write-Output (Resolve-EnginePath $Engine)