function Convert-BuildPipelineToGitLab(
    [Parameter(Mandatory=$true)] $BuildPipeline,
    [Parameter(Mandatory=$true)][string] $AgentTagPrefix
) {
    $local:GitLabYaml = ""

    # Generate the GitLab CI/CD YAML for the environment variables.
    $local:GitLabYaml = @"
variables:

"@
    foreach ($local:EnvKey in $local:BuildPipeline.GlobalEnvironmentVariables.Keys) {
        $local:GitLabYaml += @"
  $($local:EnvKey): $($local:BuildPipeline.GlobalEnvironmentVariables.Item($local:EnvKey) | ConvertTo-Json -Compress)

"@
    }

    # Generate the GitLab CI/CD YAML for the stages.
    $local:GitLabYaml += @"

stages:

"@
    foreach ($local:Stage in $local:BuildPipeline.Stages) {
        $local:GitLabYaml += @"
  - `"$local:Stage`"

"@
    }
    $local:GitLabYaml += @"


"@

    # Generate the jobs for GitLab.
    foreach ($local:Job in $local:BuildPipeline.Jobs) {
        # Determine needs array.
        $local:Needs = "__ERROR__"
        if ($local:Job.Needs.Length -eq 0) {
            $local:Needs = "[]"
        } elseif ($local:Job.Needs.Length -eq 1) {
            $local:Needs = "[" + ($local:Job.Needs[0] | ConvertTo-Json -Compress) + "]"
        } else {
            $local:Needs = ($local:Job.Needs | ConvertTo-Json -Compress)
        }

        # Determine rules, based on whether this job is manual.
        $local:Rules = "- if: '`$CI == `"true`" && `$GITLAB_INTENT != `"manual`"'"
        if ($local:Job.IsManual) {
            if ($local:Job.IsIntermediateJob) {
                $local:Rules = "- if: '`$CI == `"true`" && `$GITLAB_INTENT == `"manual`"'"
            } else {
                $local:Rules = "- if: '`$CI == `"true`" && `$GITLAB_INTENT == `"manual`"'`n      when: manual"
            }
            $local:Needs = "[]"
        }

        # Determine agent tag.
        $local:Tag = ""
        if ($null -ne $local:Job.AgentOverrideTag) {
            $local:Tag = $local:Job.AgentOverrideTag
        } elseif ($local:Job.Platform -eq "Win64") {
            $local:Tag = "$local:AgentTagPrefix-windows"
        } elseif ($local:Job.Platform -eq "Mac") {
            $local:Tag = "$local:AgentTagPrefix-mac"
        } else {
            Write-Error "Unsupported platform in GitLab generation!"
        }

        $local:GitLabYaml += @"
`"$($local:Job.Name)`":
  stage: `"$($local:Job.Stage)`"
  needs: $($local:Needs)
  tags: [`"$($local:Tag)`"]
  interruptible: true
  rules:
    $($local:Rules)

"@
        if ($null -ne $local:Job.ResourceGroup) {
            $local:GitLabYaml += @"
  resource_group: `"$($local:Job.ResourceGroup)`"

"@
        }
        if ($null -ne $local:Job.Retries) {
            $local:GitLabYaml += @"
  retry: $($local:Job.Retries)

"@
        }
        if ($local:Job.ArtifactPaths.Length -gt 0 -or
            $null -ne $local:Job.ArtifactJUnitReportPath) {
            $local:ArtifactPaths = "[]"
            if ($local:Job.ArtifactPaths.Length -eq 1) {
                $local:ArtifactPaths = "[" + ($local:Job.ArtifactPaths[0] | ConvertTo-Json -Compress) + "]"
            } else {
                $local:ArtifactPaths = ($local:Job.ArtifactPaths | ConvertTo-Json -Compress)
            }
            $local:GitLabYaml += @"
  artifacts:
    when: always
    paths: $local:ArtifactPaths

"@
            if ($null -ne $local:Job.ArtifactJUnitReportPath) {
                $local:GitLabYaml += @"
    reports: 
      junit: $($local:Job.ArtifactJUnitReportPath)

"@
            }
        }
        if ($true) {
            $local:GitLabYaml += @"
  variables:
    BUILD_GRAPH_ALLOW_MUTATION: "true"

"@
            foreach ($local:EnvKey in $local:Job.EnvironmentVariables.Keys) {
                $local:GitLabYaml += @"
    $($local:EnvKey): $($local:Job.EnvironmentVariables.Item($local:EnvKey) | ConvertTo-Json -Compress)

"@
            }
        }
        $local:GitLabYaml += @"
  script: |
$(Indent-String -Value $local:Job.Script -Indent 4)

"@
        if ($local:Job.AfterScript -ne "") {
            $local:GitLabYaml += @"
  after_script: [$($local:Job.AfterScript.Trim().Replace("`r`n", "`n") | ConvertTo-Json -Compress)]

"@
        }
        $local:GitLabYaml += @"


"@
    }

    return $local:GitLabYaml
}
