param(
    # The path to the engine.
    [Parameter(Mandatory=$true)][string] $EnginePath,
    # The distribution we are testing under.
    [Parameter(Mandatory=$true)][string] $Distribution,
    # The test we are running (as defined in BuildConfig.json).
    [Parameter(Mandatory=$true)][string] $TestName
)

$ErrorActionPreference = "Stop"

# Determine project root.
$ProjectRoot = (Resolve-Path -Path $PSScriptRoot\..\..).Path

# Load project configuration.
if (!(Test-Path $ProjectRoot\BuildConfig.json)) {
    Write-Error "Please create BuildConfig.json in your project root."
    exit 1
}
$BuildConfig = Get-Content -Raw $ProjectRoot\BuildConfig.json | ConvertFrom-Json
if ($BuildConfig.Type -ne "Project") {
    Write-Error "Build_Project.ps1 called but Type!=Project in BuildConfig.json"
    exit 1
}

# Load distribution configuration.
$DistributionConfig = $null
foreach ($CandidateDistribution in $BuildConfig.Distributions) {
    if ($CandidateDistribution.Name -eq $Distribution) {
        $DistributionConfig = $CandidateDistribution
        break
    }
}
if ($DistributionConfig -eq $null) {
    Write-Error "Distribution '$Distribution' does not appear in BuildConfig.json"
    exit 1
}

# Set up common variables.
$ProjectName = $DistributionConfig.ProjectName
$FolderName = $DistributionConfig.FolderName

# Find the Gauntlet test information.
$GauntletConfig = $null
foreach ($Test in $DistributionConfig.Tests) {
    if ($Test.Type -eq "Gauntlet" -and $Test.Name -eq "$TestName") {
        $GauntletConfig = $Test.Gauntlet
        break
    }
}
if ($GauntletConfig -eq $null) {
    Write-Error "Gauntlet tests is not defined in BuildConfig.json"
    exit 1
}

# Set up environment variables so we can patch Gauntlet.
$env:uebp_UATMutexNoWait = "1"
$env:IsBuildMachine = "1"

# Open the firewall to allow external devices to connect to a listen server.
netsh advfirewall firewall add rule name="Unreal Engine Game" dir=in action=allow protocol=UDP localport=7777 | Out-Null

# Try to use adb tcpip to change to TCP/IP mode.
try {
    adb tcpip 5555
} catch {}

Write-Host ">> Executing Gauntlet test..."
Push-Location "$EnginePath\Engine\Build\BatchFiles"
try {
    $env:USE_PLATFORM_ALLOWLIST = "true";
    if ($DistributionConfig.Build.Game -ne $null) {
        foreach ($TargetPlatform in $DistributionConfig.Build.Game.Platforms) {
            Set-Item -Force "env:ALLOW_PLATFORM_$TargetPlatform" "true"
        }
    }
    if ($DistributionConfig.Build.Client -ne $null) {
        foreach ($TargetPlatform in $DistributionConfig.Build.Client.Platforms) {
            Set-Item -Force "env:ALLOW_PLATFORM_$TargetPlatform" "true"
        }
    }
    if ($DistributionConfig.Build.Server -ne $null) {
        foreach ($TargetPlatform in $DistributionConfig.Build.Server.Platforms) {
            Set-Item -Force "env:ALLOW_PLATFORM_$TargetPlatform" "true"
        }
    }
    & "$PSScriptRoot\Internal_RunUAT.ps1" -UATEnginePath "$EnginePath" RunUnreal -noP4 -project="$ProjectRoot\$FolderName\$ProjectName.uproject" `
        -configuration="$($GauntletConfig.Configuration)" `
        -test="$($GauntletConfig.TestName)" `
        -build=local `
        -devices="$ProjectRoot\$($GauntletConfig.DeviceListPath)" `
        -tempdir="$ProjectRoot\$FolderName\GauntletArtifacts"
    if ($LASTEXITCODE -ne 0) {
        exit $LASTEXITCODE
    }
} finally {
    Pop-Location
}

exit 0