<#
    WindowsEnvironment contains the Windows-specific environment
    settings for jobs that run on Windows.
#>
class WindowsEnvironment {
    # Specifies the project root on Windows, as a stringified PowerShell expression.
    [string] $ProjectRoot;
    # All of the -set: parameters to pass to BuildGraph on Windows.
    [string] $BuildGraphSettings;
    # The absolute path to shared storage on Windows. Must start with a drive letter (like X:\). Must have a trailing slash.
    [string] $SharedStorageAbsolutePath;
    # The absolute path to the Linux toolchain on Windows. This only needs to be set if the project is targeting Linux.
    [string] $LinuxToolchainPath;
    # If true, the working directory for each job will be mapped to a virtual drive so that file paths are shorter.
    [bool] $MapDriveForShorterPaths;
}

enum MacArtifactTransport {
    # The shared storage location is directly mounted on macOS, and BuildGraph can access it directly.
    Direct
    # Input and output artifacts should be transported by the build server's artifact mechanism.
    BuildServer
    # Input and output artifacts should be transported by uploading and download to BackblazeB2. This can be used if your artifacts exceed the maximum size allowable by your build server. The ARTIFACT_BACKBLAZE_B2_KEY_ID, ARTIFACT_BACKBLAZE_B2_APPLICATION_KEY and ARTIFACT_BACKBLAZE_B2_BUCKET_NAME environment variables must be set on both Windows and macOS agents.
    BackblazeB2
}

<#
    MacEnvironment contains the macOS-specific environment
    settings for jobs that run on macOS.
#>
class MacEnvironment {
    # Specifies the project root on macOS, as a stringified PowerShell expression.
    [string] $ProjectRoot;
    # All of the -set: parameters to pass to BuildGraph on macOS.
    [string] $BuildGraphSettings;
    # The engine path to use on macOS agents. This is used if you're targeting a custom engine where the paths different on Windows and macOS. You don't need to set it if you're just using a version number like "4.27" with the -Engine parameter.
    [string] $MacEnginePathOverride;
    # The absolute path to shared storage on macOS. This will not be set if the artifact transport is not Direct.
    [string] $SharedStorageAbsolutePath;
    # The type of artifact transport to use when running a build job on macOS.
    [MacArtifactTransport] $ArtifactTransport;
}

<#
    BuildGraphEnvironment encapsulates all of the environment settings that surround
    how BuildGraph jobs are run on build server agents.
#>
class BuildGraphEnvironment {
    # The engine version to build against. This will be null for custom Unreal Engine builds.
    [string] $Engine;
    # If true, this is building a custom Unreal Engine install, not a project or plugin.
    [bool] $IsEngineBuild;
    # The pipeline ID of the build on your build server. On GitLab, this is the CI_PIPELINE_ID environment variable.
    [string] $PipelineId;
    # The Windows build environment.
    [WindowsEnvironment] $Windows
    # The macOS build environment.
    [MacEnvironment] $Mac
}

function Internal-Generate-BuildJobs(
    # The pipeline to emit the build jobs into.
    $BuildPipeline, 
    # The distribution that is being built.
    [Parameter(Mandatory = $true)][string] $Distribution,
    # The distribution configuration.
    [Parameter(Mandatory = $true)] $DistributionConfig,
    # The BuildGraph JSON data.
    [Parameter(Mandatory = $true)] $BuildGraph,
    # The name of the BuildGraph script underneath BuildScripts. This is only used for non-engine builds.
    [string] $BuildGraphScriptName,
    # The environment settings for the build.
    [Parameter(Mandatory = $true)][BuildGraphEnvironment] $Environment,
    # The map of nodes to their full dependencies, if this is an engine build.
    $NodeToFullDependenciesArray,
    # The agent types map for the platforms.
    [Parameter(Mandatory = $true)] $AgentTypes,
    # The short hash for the build.
    [Parameter(Mandatory = $true)][string] $ShortHash,
    # The shared storage folder name for the build.
    [Parameter(Mandatory = $true)][string] $SharedStorageFolderName,
    # The name of the BuildScripts folder.
    [Parameter(Mandatory = $true)][string] $BuildScriptsFolderName,
    # The name of the BuildScripts\Lib folder.
    [Parameter(Mandatory = $true)][string] $BuildScriptsLibFolderName,
    # The agent type to generate for.
    [Parameter(Mandatory = $true)][string] $TargetAgentType,
    # Is this building or targeting Unreal Engine 5?
    [Parameter(Mandatory = $true)][bool] $IsUnrealEngine5
) {
    $local:GitCheckoutDecompressor = "function Decompress-Data([string] `$RawData) { `$local:BinaryData = [System.Convert]::FromBase64String(`$RawData); `$local:MS = New-Object System.IO.MemoryStream; `$local:MS.Write(`$local:BinaryData, 0, `$local:BinaryData.Length) | Out-Null; `$local:MS.Seek(0, 0) | Out-Null; `$local:CS = New-Object System.IO.Compression.GZipStream(`$local:MS, [IO.Compression.CompressionMode]`"Decompress`"); `$local:SR = New-Object System.IO.StreamReader(`$local:CS); `$local:Value = `$local:SR.ReadToEnd(); return `$local:Value; }"

    $local:GitCloneFlagsGlobal = ""
    if ($local:Environment.IsEngineBuild) {
        $local:GitCloneFlagsGlobal = " -IsEngineBuild"
    }
    foreach ($local:Group in $local:BuildGraph.Groups) {
        $local:AgentType = $local:Group."Agent Types"[0]
        if ($local:AgentTypes[$local:TargetAgentType] -contains $local:AgentType) {
            continue
        }
        foreach ($local:Node in $local:Group.Nodes) {
            if ($local:Node.Name -eq "End") {
                # We don't actually care about this task; it's only used to construct the
                # dependency graph when BuildGraph runs.
                continue
            }

            # Create the build job entry that we'll put all the information into.
            $local:BuildJob = @{
                # The name of this build job.
                Name                    = "";
                # The stage this build job runs in.
                Stage                   = "";
                # The names of other build jobs that this build job depends on.
                Needs                   = @();
                # The platform for this build job to run on; either "Win64" or "Mac".
                Platform                = "None";
                # If true, this build job is a manual build job.
                IsManual                = $false;
                # If true, this build job is an auto-generated intermediate build job. These are used
                # for artifact transfer for remote macOS builds.
                IsIntermediateJob       = $false;
                # If true, this build job can write to shared storage.
                CanWriteToSharedStorage = $false;
                # A list of artifact paths to upload for this build job.
                ArtifactPaths           = @();
                # The path to the JUnit test report to upload for this build job.
                ArtifactJUnitReportPath = $null;
                # If set, the resource that this build job needs to obtain from the build server
                # in order to execute. This can be used to prevent parallelism between Gauntlet
                # tests when the Gauntlet tests would share hardware.
                ResourceGroup           = $null;
                # If set, specifies the number of times the job will be automatically retried on
                # failure.
                Retries                 = $null;
                # If set, this sets the "agent override tag" which is custom data that the build
                # server can use to place this build job on a *specific build machine*. This
                # can be used for Gauntlet tests where e.g. iOS devices might only be plugged into
                # one physical machine.
                AgentOverrideTag        = $null;
                # The environment variables to set for this build job.
                EnvironmentVariables    = @{};
                # The PowerShell or Bash script to run for this build job (depending on the platform).
                Script                  = "";
                # The PowerShell or Bash script to run after this build job (this should always execute, even if the build job script fails).
                AfterScript             = "";
            }

            # Figure out the dependencies of this build job.
            if ($local:Environment.IsEngineBuild) {
                $local:BuildJob.Needs = $local:NodeToFullDependenciesArray[$local:Node.Name]
                if ($local:BuildJob.Needs -is [string]) {
                    $local:BuildJob.Needs = @($local:BuildJob.Needs)
                }
            }
            else {
                $local:BuildJob.Needs = $local:Node.DependsOn.Split(";")
                if ($local:BuildJob.Needs.Length -eq 1 -and $local:BuildJob.Needs[0] -eq "") {
                    $local:BuildJob.Needs = @()
                }
            }

            # Determine if this build job is a manual build job.
            $local:BuildJob.IsManual = $false
            if ($local:Node.Name.StartsWith("Deploy Manual ")) {
                $local:BuildJob.IsManual = $true
            }

            # Set the name and stage.
            $local:BuildJob.Name = $local:Node.Name;
            $local:BuildJob.Stage = $local:Group.Name;

            # Adjust settings if we're running automation tests.
            if ($local:Node.Name.StartsWith("Automation ")) {
                $local:BuildJob.ArtifactPaths = @(
                    "$local:ShortHash/BuildScripts/Temp/*/TestResults_*.xml",
                    "$local:ShortHash/BuildScripts/Temp/T*/Saved/Logs/Worker*.log"
                )
                $local:BuildJob.ArtifactJUnitReportPath = "$local:ShortHash/BuildScripts/Temp/*/TestResults_*.xml"
                $local:BuildJob.Retries = 2;
            }

            # Generate the script and other settings for Windows build jobs.
            if ($local:AgentTypes["Win64"] -contains $local:AgentType) {
                $local:BuildJob.Platform = "Win64"
                $local:BuildJob.CanWriteToSharedStorage = $true
                $local:PreGauntletCommand = ""

                # Generate the scripts for all Prepare-BuildGraph steps. These have to run outside of BuildGraph
                # logic because they might prepare the workspace before BuildGraph downloads artifacts on top
                # of them.
                $local:PreparationScripts = ""
                if ($null -ne $local:DistributionConfig.Prepare) {
                    foreach ($Prepare in $local:DistributionConfig.Prepare) {
                        if ($local:Prepare.Type -eq "Custom") {
                            if ($null -ne $local:Prepare.RunBefore -and $local:Prepare.RunBefore.Contains("BuildGraph")) {
                                $local:PreparationScripts += @"
powershell.exe -ExecutionPolicy Bypass `"`$((Get-Location).Path)\$($local:Prepare.ScriptPath.Replace("/", "\"))`"
if (`$LastExitCode -ne 0) { exit `$LastExitCode }

"@
                            }
                        }
                    }
                }

                # Adjust settings if we're running Gauntlet tests.
                if ($local:Node.Name.StartsWith("Gauntlet ") -and $local:DistributionConfig.FolderName -ne $null -and $local:DistributionConfig.FolderName -ne "") {
                    foreach ($local:Test in $local:DistributionConfig.Tests) {
                        if ($local:Test.Name -eq $local:Node.Name.Substring("Gauntlet ".Length)) {
                            $local:BuildJob.ResourceGroup = $local:Test.Gauntlet.ResourceGroup
                            $local:BuildJob.AgentOverrideTag = $local:Test.Gauntlet.GitLabOverrideTag
                            break
                        }
                    }
                    $local:BuildJob.CanWriteToSharedStorage = $false
                    $local:BuildJob.ArtifactPaths = @(
                        "$local:ShortHash/$($local:DistributionConfig.FolderName)/GauntletArtifacts/DeviceCache/**"
                        "$local:ShortHash/$($local:DistributionConfig.FolderName)/GauntletArtifacts/Logs/**"
                    )
                    # When we are going to run Gauntlet, we have to patch the Gauntlet binary. But we can't
                    # do that if we're already running the BuildGraph binary, because the DLLs will be locked.
                    # Do the patching process early, and don't do it inside Test_RunGauntletTestFromBuildGraph.ps1
                    $local:PreGauntletCommand = @"
& `"`$((Get-Location).Path)\$local:BuildScriptsLibFolderName\Patch_Gauntlet.ps1`" -Engine "$($local:Environment.Engine)" -Distribution "$local:Distribution" -TestName `"$($Node.Name.Substring("Gauntlet ".Length))`"
if (`$LastExitCode -ne 0) { exit `$LastExitCode }

if (Test-Path `"`$((Get-Location).Path)\$($local:DistributionConfig.FolderName)\GauntletArtifacts`") {
    Write-Output `"Cleaning up old Gauntlet artifacts...`"
    Remove-Item -Force -Recurse `"`$((Get-Location).Path)\$($local:DistributionConfig.FolderName)\GauntletArtifacts`"
}
    
"@
                }

                # Set up the Linux cross-compile root if that has been provided.
                if ($local:Environment.Windows.LinuxToolchainPath -ne $null -and
                    $local:Environment.Windows.LinuxToolchainPath.Trim() -ne "") {
                    $local:BuildJob.EnvironmentVariables = @{
                        "LINUX_MULTIARCH_ROOT" = $local:Environment.Windows.LinuxToolchainPath;
                    }
                }

                $local:RepositoryRoot = "`$((Get-Location).Path)"
                if (!$local:Environment.IsEngineBuild) {
                    if ($local:Environment.Windows.MapDriveForShorterPaths) {
                        $local:BuildJob.Script = @"
Get-ChildItem function:[d-z]: -n | ForEach-Object { subst `$_ /D | Out-Null }
`$local:MappedDrive = (Get-ChildItem function:[d-z]: -n | Where-Object { !(Test-Path -ErrorAction SilentlyContinue `$_) } | Select-Object -Last 1)
try {
    Write-Output "Mapping real working directory $($local:RepositoryRoot) to virtual drive `$MappedDrive..."
    subst `$MappedDrive "$($local:RepositoryRoot)"
    if (!(Test-Path `$MappedDrive)) {
        Write-Error "Unable to map real working directory to virtual drive!"
        exit 1
    }

    Push-Location `$MappedDrive
    try {
        # Clone the repository into the target folder.
        $local:GitCheckoutDecompressor
        Set-Content -Force -Path `"`$(`$MappedDrive)GitCheckout_Optimized.ps1`" -Value (Decompress-Data `"`$env:GIT_OPTIMIZED_CHECKOUT_SCRIPT`")
        & `"`$(`$MappedDrive)GitCheckout_Optimized.ps1`" -TargetDirectory `"`$MappedDrive\$local:ShortHash`" -ProjectFolderName `"$($local:DistributionConfig.FolderName)`" $local:GitCloneFlagsGlobal
        if (`$LastExitCode -ne 0) { exit `$LastExitCode }
        
        # Delete the existing artifacts folder if it exists; this makes retries work on GitLab.
        if (Test-Path "$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:Node.Name)") {
            Write-Host "Removing existing artifacts folder at $($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:Node.Name)..."
            Remove-Item -Force -Recurse "$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:Node.Name)"
        }

        # Clean up disk space.
        & `"`$MappedDrive\$local:ShortHash\$local:BuildScriptsLibFolderName\CleanupDiskUsage.ps1`" -RepositoryRoot "`$MappedDrive" -ShortHash "$local:ShortHash" -SharedBuildArtifacts "$($local:Environment.Windows.SharedStorageAbsolutePath)" -ArtifactsId "$local:SharedStorageFolderName"

        # Patch BuildGraph for the engine we are targeting.
        Set-Location `"`$MappedDrive\$local:ShortHash`"
        & `"`$MappedDrive\$local:ShortHash\$local:BuildScriptsLibFolderName\Patch_BuildGraph.ps1`" -Engine "$($local:Environment.Engine)"
        if (`$LastExitCode -ne 0) { exit `$LastExitCode }
        
        $local:PreparationScripts

        # Set up the BuildGraph project root (this is dynamic and can't be placed under variables:).
        # Also, remove any previous, local BuildGraph metadata.
        $($local:PreGauntletCommand)`$env:BUILD_GRAPH_PROJECT_ROOT = `"$($local:Environment.Windows.ProjectRoot)`"
        if (Test-Path `"$($local:Environment.Windows.ProjectRoot)\Engine\Saved\BuildGraph`") {
            Remove-Item -Force -Recurse `"$($local:Environment.Windows.ProjectRoot)\Engine\Saved\BuildGraph`"
        }
        
        # Execute BuildGraph!
        `$env:IsBuildMachine = "1"
        Write-Host "Executing BuildGraph..."
        `$EnginePath = (& `"`$MappedDrive\$local:ShortHash\$local:BuildScriptsLibFolderName\Internal_GetEnginePath.ps1`" -Engine `"$($local:Environment.Engine)`")
        & `"`$MappedDrive\$local:ShortHash\$local:BuildScriptsLibFolderName\Internal_RunUAT.ps1`" -UATEnginePath `"$EnginePath`" BuildGraph ``
            -Script=`"`$MappedDrive\$local:ShortHash\$local:BuildScriptsLibFolderName\$local:BuildGraphScriptName`" ``
            -SingleNode=`"$($local:Node.Name)`" ``
            -SharedStorageDir=`"$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\`" ``
            -NoP4 ``
            $(if ($local:BuildJob.CanWriteToSharedStorage) { "-WriteToSharedStorage" } else { "" }) ``
            "-set:EnginePath=``"`$EnginePath``"" ``
            $($local:Environment.Windows.BuildGraphSettings)
        Write-Host "RunUAT.ps1 exited with exit code `$LastExitCode"
        exit `$LastExitCode # Build exit
    } finally {
        Pop-Location
    }
} finally {
    Write-Output "Unmapping virtual drive `$MappedDrive..."
    subst `$MappedDrive /D
}
"@
                    }
                    else {
                        $local:BuildJob.Script = @"
# Clone the repository into the target folder.
$local:GitCheckoutDecompressor
Set-Content -Force -Path `"$($local:RepositoryRoot)GitCheckout_Optimized.ps1`" -Value (Decompress-Data `"`$env:GIT_OPTIMIZED_CHECKOUT_SCRIPT`")
& `"$($local:RepositoryRoot)GitCheckout_Optimized.ps1`" -TargetDirectory `"$local:RepositoryRoot\$local:ShortHash`" -ProjectFolderName `"$($local:DistributionConfig.FolderName)`" $local:GitCloneFlagsGlobal
if (`$LastExitCode -ne 0) { exit `$LastExitCode }
        
# Delete the existing artifacts folder if it exists; this makes retries work on GitLab.
if (Test-Path "$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:Node.Name)") {
    Write-Host "Removing existing artifacts folder at $($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:Node.Name)..."
    Remove-Item -Force -Recurse "$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:Node.Name)"
}

# Clean up disk space.
& `"$local:RepositoryRoot\$local:ShortHash\$local:BuildScriptsLibFolderName\CleanupDiskUsage.ps1`" -RepositoryRoot "$local:RepositoryRoot" -ShortHash "$local:ShortHash" -SharedBuildArtifacts "$($local:Environment.Windows.SharedStorageAbsolutePath)" -ArtifactsId "$local:SharedStorageFolderName"

# Patch BuildGraph for the engine we are targeting.
Set-Location `"$local:RepositoryRoot\$local:ShortHash`"
& `"$local:RepositoryRoot\$local:BuildScriptsLibFolderName\Patch_BuildGraph.ps1`" -Engine "$($local:Environment.Engine)"
if (`$LastExitCode -ne 0) { exit `$LastExitCode }

$local:PreparationScripts

# Set up the BuildGraph project root (this is dynamic and can't be placed under variables:).
# Also, remove any previous, local BuildGraph metadata.
$($local:PreGauntletCommand)`$env:BUILD_GRAPH_PROJECT_ROOT = `"$($local:Environment.Windows.ProjectRoot)`"
if (Test-Path `"$($local:Environment.Windows.ProjectRoot)\Engine\Saved\BuildGraph`") {
    Remove-Item -Force -Recurse `"$($local:Environment.Windows.ProjectRoot)\Engine\Saved\BuildGraph`"
}

# Execute BuildGraph!
`$env:IsBuildMachine = "1"
Write-Host "Executing BuildGraph..."
`$EnginePath = (& `"$local:RepositoryRoot\$local:BuildScriptsLibFolderName\Internal_GetEnginePath.ps1`" -Engine `"$($local:Environment.Engine)`")
& `"$local:RepositoryRoot\$local:BuildScriptsLibFolderName\Internal_RunUAT.ps1`" -UATEnginePath `"`$EnginePath`" BuildGraph ``
    -Script=`"$local:RepositoryRoot\$local:BuildScriptsLibFolderName\$local:BuildGraphScriptName`" ``
    -SingleNode=`"$($local:Node.Name)`" ``
    -SharedStorageDir=`"$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\`" ``
    -NoP4 ``
    $(if ($local:BuildJob.CanWriteToSharedStorage) { "-WriteToSharedStorage" } else { "" }) ``
    "-set:EnginePath=``"`$EnginePath``"" ``
    $($local:Environment.Windows.BuildGraphSettings)
Write-Host "RunUAT.ps1 exited with exit code `$LastExitCode"
exit `$LastExitCode # Build exit
"@
                    }
                }
                else {
                    $local:BuildJob.Script = @"
# Clone the repository into the target folder.
$local:GitCheckoutDecompressor
Set-Content -Force -Path `"$($local:RepositoryRoot)GitCheckout_Optimized.ps1`" -Value (Decompress-Data `"`$env:GIT_OPTIMIZED_CHECKOUT_SCRIPT`")
& `"$($local:RepositoryRoot)GitCheckout_Optimized.ps1`" -TargetDirectory `"$local:RepositoryRoot\$local:ShortHash`" $local:GitCloneFlagsGlobal
if (`$LastExitCode -ne 0) { exit `$LastExitCode }
        
# Delete the existing artifacts folder if it exists; this makes retries work on GitLab.
if (Test-Path "$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:Node.Name)") {
    Write-Host "Removing existing artifacts folder at $($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:Node.Name)..."
    Remove-Item -Force -Recurse "$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:Node.Name)"
}

# Clean up disk space.
& `"$local:RepositoryRoot\$local:ShortHash\$local:BuildScriptsLibFolderName\CleanupDiskUsage.ps1`" -RepositoryRoot "$local:RepositoryRoot" -ShortHash "$local:ShortHash" -SharedBuildArtifacts "$($local:Environment.Windows.SharedStorageAbsolutePath)" -ArtifactsId "$local:SharedStorageFolderName"

# We map the engine to a drive letter for the duration of the run, to avoid issues
# with long paths.
`$RealEnginePath = "$local:RepositoryRoot\$local:ShortHash\BuildScripts\Temp\UE"
if (!(Test-Path "`$RealEnginePath")) {
    New-Item -ItemType Directory "`$RealEnginePath"
}
ls function:[d-z]: -n | % { subst `$_ /D | Out-Null }
`$EnginePath = (ls function:[d-z]: -n | ?{ !(test-path `$_) } | select -Last 1)
try {
    Write-Output "Mapping real engine path `$RealEnginePath to virtual drive `$EnginePath..."
    subst `$EnginePath "`$RealEnginePath"
    if (!(Test-Path `$EnginePath)) {
        Write-Error "Unable to map real engine path to virtual drive!"
        exit 1
    }

    # Prepare the engine. We always do this (even on every step of GitLab)
    # because this is what clones the engine and patches it for the build.
    & "$local:RepositoryRoot\$local:ShortHash\$local:BuildScriptsLibFolderName\Prepare_Engine.ps1" -Distribution "$local:Distribution" -EnginePath "`$EnginePath"
    
    # Patch BuildGraph for the engine we are targeting.
    & `"$local:RepositoryRoot\$local:ShortHash\$local:BuildScriptsLibFolderName\Patch_BuildGraph.ps1`" -Engine "`$EnginePath"
    if (`$LastExitCode -ne 0) { exit `$LastExitCode }

    # Set up the BuildGraph project root (this is dynamic and can't be placed under variables:).
    # Also, remove any previous, local BuildGraph metadata.
    if (Test-Path `"`$EnginePath\Engine\Saved\BuildGraph`") {
        Remove-Item -Force -Recurse `"`$EnginePath\Engine\Saved\BuildGraph`"
    }

    # Execute BuildGraph!
    `$env:IsBuildMachine = "1"
    Write-Host "Executing BuildGraph..."
    & `"$local:RepositoryRoot\$local:ShortHash\$local:BuildScriptsLibFolderName\Internal_RunUAT.ps1`" -UATEnginePath `"`$EnginePath`" BuildGraph ``
        -Script=`"`$EnginePath\Engine\Build\InstalledEngineBuild.xml`" ``
        -SingleNode=`"$($local:Node.Name)`" ``
        -SharedStorageDir=`"$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\`" ``
        -NoP4 ``
        $(if ($local:BuildJob.CanWriteToSharedStorage) { "-WriteToSharedStorage" } else { "" }) ``
        $($local:Environment.Windows.BuildGraphSettings)
    Write-Host "RunUAT.ps1 exited with exit code `$LastExitCode"
    exit `$LastExitCode # Build exit
} finally {
    Write-Output "Unmapping virtual drive `$EnginePath..."
    subst `$EnginePath /D
}
"@
                }

                $local:BuildPipeline.Jobs += $local:BuildJob
            } 

            # Generate the script and other settings for macOS build jobs.
            elseif ($local:AgentTypes["Mac"] -contains $local:AgentType) {
                $local:BuildJob.Platform = "Mac"
                $local:RepositoryRoot = "`$(pwd)"
                $local:RepositoryRootPwsh = "`$((Get-Location).Path)"

                # Generate the scripts for all Prepare-BuildGraph steps. These have to run outside of BuildGraph
                # logic because they might prepare the workspace before BuildGraph downloads artifacts on top
                # of them.
                $local:PreparationScripts = ""
                if ($null -ne $local:DistributionConfig.Prepare) {
                    foreach ($Prepare in $local:DistributionConfig.Prepare) {
                        if ($local:Prepare.Type -eq "Custom") {
                            if ($null -ne $local:Prepare.RunBefore -and $local:Prepare.RunBefore.Contains("BuildGraph")) {
                                $local:PreparationScripts += @"
pwsh `"`$(pwd)/$($local:Prepare.ScriptPath.Replace("\", "/"))`"

"@
                            }
                        }
                    }
                }

                # If we're not using direct artifact transport on macOS, we need to generate build jobs on Windows to move files between shared storage and whatever artifact transport mechanism we're using.
                $local:ArtifactsDownloadScript = ""
                $local:ArtifactsUploadScript = ""
                $local:MacSharedStorageDirectory = ""
                if ($local:Environment.Mac.ArtifactTransport -eq [MacArtifactTransport]::Direct) {
                    $local:BuildJob.CanWriteToSharedStorage = $true
                    $local:MacSharedStorageDirectory = "$($local:Environment.Mac.SharedStorageAbsolutePath)$local:SharedStorageFolderName/"
                }
                else {
                    $local:MacSharedStorageDirectory = "$local:RepositoryRoot/RemoteArtifacts/"
                    $local:BuildJob.Name = "$($local:Node.Name) (Exec)";

                    # If we depend on artifacts from other build jobs, insert an (In) job that will transfer artifacts from shared storage.
                    if ($local:BuildJob.Needs.Length -gt 0) {
                        $local:BuildJobIn = @{
                            Name                    = "$($local:Node.Name) (In)";
                            Stage                   = $local:Group.Name;
                            Needs                   = $local:BuildJob.Needs;
                            Platform                = "Win64";
                            IsManual                = $false;
                            IsIntermediateJob       = $true;
                            CanWriteToSharedStorage = $false;
                            ArtifactPaths           = @();
                            ArtifactJUnitReportPath = $null;
                            ResourceGroup           = $null;
                            Retries                 = $null;
                            AgentOverrideTag        = $null;
                            EnvironmentVariables    = @{};
                            Script                  = "";
                            AfterScript             = "";
                        };
                        $local:BuildJob.Needs = @(
                            $local:BuildJobIn.Name
                        );
                        foreach ($local:DependsOn in $local:BuildJobIn.Needs) {
                            $local:BuildJobIn.Script += @"
Write-Host `"Copying $($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:DependsOn)\ -> .\$local:ShortHash\RemoteInputArtifacts\$($local:DependsOn)\`"
Copy-Item -Recurse -Force `"$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:DependsOn)\`" `".\$local:ShortHash\RemoteInputArtifacts\$($local:DependsOn)\`"

"@
                            $local:BuildJob.AfterScript += @"
rm -Rf `"$local:RepositoryRoot/$local:ShortHash/RemoteArtifacts/$($local:DependsOn)`"

"@
                        }
                        
                        # Add transport specific logic.
                        if ($local:Environment.Mac.ArtifactTransport -eq [MacArtifactTransport]::BuildServer) {
                            $local:BuildJobIn.ArtifactPaths = @(
                                "$ShortHash/RemoteInputArtifacts/"
                            )
                            $local:BuildJobIn.Script = @"
if (Test-Path `"$($local:RepositoryRootPwsh)\$local:ShortHash\RemoteInputArtifacts`") {
    Remove-Item -Recurse -Force `"$($local:RepositoryRootPwsh)\$local:ShortHash\RemoteInputArtifacts`"
}
New-Item -ItemType Directory `"$($local:RepositoryRootPwsh)\$ShortHash\RemoteInputArtifacts`"
$($local:BuildJobIn.Script)
"@
                        }
                        elseif ($local:Environment.Mac.ArtifactTransport -eq [MacArtifactTransport]::BackblazeB2) {
                            $local:BuildJobIn.Script = @"
# Clone the repository into the target folder.
$local:GitCheckoutDecompressor
Set-Content -Force -Path `"$($local:RepositoryRootPwsh)GitCheckout_Optimized.ps1`" -Value (Decompress-Data `"`$env:GIT_OPTIMIZED_CHECKOUT_SCRIPT`")
& `"$($local:RepositoryRootPwsh)GitCheckout_Optimized.ps1`" -TargetDirectory `"$local:RepositoryRootPwsh\$local:ShortHash`" $local:GitCloneFlagsGlobal
if (`$LastExitCode -ne 0) { exit `$LastExitCode }

# Remove input artifacts folder if it exists, since we're going to upload the whole thing.
if (Test-Path `"$($local:RepositoryRootPwsh)\$local:ShortHash\RemoteInputArtifacts`") {
    Remove-Item -Recurse -Force `"$($local:RepositoryRootPwsh)\$local:ShortHash\RemoteInputArtifacts`"
}

# Copy the artifacts that are needed.
$($local:BuildJobIn.Script)

# Now upload the RemoteInputArtifacts folder.
& `"$($local:RepositoryRootPwsh)\$local:ShortHash\$local:BuildScriptsLibFolderName\Internal_ArtifactTransport_UploadToB2.ps1`" -SharedStorageFolderName `"$local:SharedStorageFolderName`" -RelativePath `".\$local:ShortHash\RemoteInputArtifacts`"
"@
                            $local:ArtifactsDownloadScript = @"
pwsh `"$local:RepositoryRoot/$local:ShortHash/$($local:BuildScriptsLibFolderName.Replace("\", "/"))/Internal_ArtifactTransport_DownloadFromB2.ps1`" -SharedStorageFolderName `"$local:SharedStorageFolderName`" -RelativePath `"./$local:ShortHash/RemoteInputArtifacts`"
"@
                        }

                        $local:BuildPipeline.Jobs += $local:BuildJobIn
                    }

                    # We also need to generate the output job when we're not using direct transport on macOS. This is always generated, regardless
                    # of our dependencies or the dependencies of other nodes.
                    if ($true) {
                        $local:BuildJobOut = @{
                            Name                    = "$($local:Node.Name)";
                            Stage                   = $local:Group.Name;
                            Needs                   = @(
                                "$($local:Node.Name) (Exec)"
                            );
                            Platform                = "Win64";
                            IsManual                = $false;
                            IsIntermediateJob       = $true;
                            CanWriteToSharedStorage = $false;
                            ArtifactPaths           = @();
                            ArtifactJUnitReportPath = $null;
                            ResourceGroup           = $null;
                            Retries                 = $null;
                            AgentOverrideTag        = $null;
                            EnvironmentVariables    = @{};
                            Script                  = "";
                            AfterScript             = "";
                        };
                        $local:BuildJobOut.Script = @"
if (!(Test-Path `"$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:Node.Name)`")) {
    New-Item -ItemType Directory `"$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:Node.Name)`"
}
Copy-Item -Force `".\$local:ShortHash\RemoteArtifacts\$($local:Node.Name)\*`" `"$($local:Environment.Windows.SharedStorageAbsolutePath)$local:SharedStorageFolderName\$($local:Node.Name)\`"
"@

                        # Add transport specific logic.
                        if ($local:Environment.Mac.ArtifactTransport -eq [MacArtifactTransport]::BuildServer) {
                            # Note: this applies to the Exec job, not the output job.
                            $local:BuildJob.ArtifactPaths += "$ShortHash/RemoteArtifacts/"
                        }
                        elseif ($local:Environment.Mac.ArtifactTransport -eq [MacArtifactTransport]::BackblazeB2) {
                            $local:BuildJobOut.Script = @"
# Clone the repository into the target folder.
$local:GitCheckoutDecompressor
Set-Content -Force -Path `"$($local:RepositoryRootPwsh)GitCheckout_Optimized.ps1`" -Value (Decompress-Data `"`$env:GIT_OPTIMIZED_CHECKOUT_SCRIPT`")
& `"$($local:RepositoryRootPwsh)GitCheckout_Optimized.ps1`" -TargetDirectory `"$local:RepositoryRootPwsh\$local:ShortHash`" $local:GitCloneFlagsGlobal
if (`$LastExitCode -ne 0) { exit `$LastExitCode }

# Now download the RemoteArtifacts folder.
& `"$($local:RepositoryRootPwsh)\$local:ShortHash\$local:BuildScriptsLibFolderName\Internal_ArtifactTransport_DownloadFromB2.ps1`" -SharedStorageFolderName `"$local:SharedStorageFolderName`" -RelativePath `".\$local:ShortHash\RemoteArtifacts\$($local:Node.Name)`"

# Copy the artifacts that were emitted.
$($local:BuildJobOut.Script)
"@
                            $local:ArtifactsUploadScript = @"
pwsh `"$local:RepositoryRoot/$local:ShortHash/$($local:BuildScriptsLibFolderName.Replace("\", "/"))/Internal_ArtifactTransport_UploadToB2.ps1`" -SharedStorageFolderName `"$local:SharedStorageFolderName`" -RelativePath `"./$local:ShortHash/RemoteArtifacts/$($local:Node.Name)`"
"@
                        }

                        $local:BuildPipeline.Jobs += $local:BuildJobOut
                    }
                }

                $local:MacEngineValue = $local:Environment.Engine
                if ($local:Environment.Mac.MacEnginePathOverride -ne $null -and
                    $local:Environment.Mac.MacEnginePathOverride -ne "") {
                    $local:MacEngineValue = $local:Environment.Mac.MacEnginePathOverride
                }

                if (!$local:Environment.IsEngineBuild) {
                    $local:BuildJob.Script = @"
# Clone the repository into the target folder.
pwsh -Command `'$local:GitCheckoutDecompressor; Set-Content -Force -Path `"$($local:RepositoryRootPwsh)GitCheckout_Optimized.ps1`" -Value (Decompress-Data `"`$env:GIT_OPTIMIZED_CHECKOUT_SCRIPT`")`'
pwsh `"$($local:RepositoryRoot)GitCheckout_Optimized.ps1`" -TargetDirectory `"$local:RepositoryRoot/$local:ShortHash`" -ProjectFolderName `"$($local:DistributionConfig.FolderName)`" $local:GitCloneFlagsGlobal

# Download artifacts if required by the transport.
$($local:ArtifactsDownloadScript)

# Patch BuildGraph for the engine we are targeting.
pushd `"$local:RepositoryRoot/$local:ShortHash`"
pwsh `"$local:RepositoryRoot/$($local:BuildScriptsLibFolderName.Replace("\", "/"))/Patch_BuildGraph.ps1`" -Engine "$local:MacEngineValue"

$local:PreparationScripts

# Set up the remote artifacts directory properly.
if [ -e "$local:RepositoryRoot/RemoteArtifacts/" ]; then rm -Rf `"$local:RepositoryRoot/RemoteArtifacts`"; fi
if [ -e "$local:RepositoryRoot/RemoteInputArtifacts/" ]; then echo `"Moving remote input artifacts into place...`"; mv `"$local:RepositoryRoot/RemoteInputArtifacts`" `"$local:RepositoryRoot/RemoteArtifacts`"; fi
if [ -e "$local:RepositoryRoot/RemoteArtifacts/" ]; then echo `"Remote artifacts folder looks like:`"; ls -la "$local:RepositoryRoot/RemoteArtifacts/"; fi

# Set up the BuildGraph project root (this is dynamic and can't be placed under variables:).
# Also, remove any previous, local BuildGraph metadata.
export BUILD_GRAPH_PROJECT_ROOT=`"$($local:Environment.Mac.ProjectRoot)`"
if [ -e `"$($local:Environment.Mac.ProjectRoot)/Engine/Saved/BuildGraph`" ]; then rm -Rf `"$($local:Environment.Mac.ProjectRoot)/Engine/Saved/BuildGraph`"; fi

# Execute BuildGraph!
export IsBuildMachine=`"1`"
echo "Executing BuildGraph..."
ENGINE_PATH=`$(pwsh -File `"$local:RepositoryRoot/$($local:BuildScriptsLibFolderName.Replace("\", "/"))/Internal_GetEnginePath.ps1`" -Engine `"$local:MacEngineValue`")
pwsh `"$local:RepositoryRoot/$($local:BuildScriptsLibFolderName.Replace("\", "/"))/Internal_RunUAT.ps1`" -UATEnginePath `"`$ENGINE_PATH`" BuildGraph \
    -Script=`"$local:RepositoryRoot/$($BuildScriptsLibFolderName.Replace("\", "/"))/$local:BuildGraphScriptName`" \
    -SingleNode=`"$($local:Node.Name)`" \
    -SharedStorageDir=`"$local:MacSharedStorageDirectory`" \
    -NoP4 \
    $(if ($local:BuildJob.CanWriteToSharedStorage) { "-WriteToSharedStorage" } else { "" }) \
    -set:EnginePath="`$ENGINE_PATH" \
    $($local:Environment.Mac.BuildGraphSettings)

# Upload artifacts if required by the transport.
popd
$($local:ArtifactsUploadScript)
"@
                }
                else {
                    $local:BuildJob.Script = @"
# Clone the repository into the target folder.
pwsh -Command `'$local:GitCheckoutDecompressor; Set-Content -Force -Path `"$($local:RepositoryRootPwsh)GitCheckout_Optimized.ps1`" -Value (Decompress-Data `"`$env:GIT_OPTIMIZED_CHECKOUT_SCRIPT`")`'
pwsh `"$($local:RepositoryRoot)GitCheckout_Optimized.ps1`" -TargetDirectory `"$local:RepositoryRoot/$local:ShortHash`" $local:GitCloneFlagsGlobal

# Prepare the engine. We always do this (even on every step of GitLab)
# because this is what clones the engine and patches it for the build.
pushd `"$local:RepositoryRoot/$local:ShortHash`"
pwsh `"$RepositoryRoot/$($BuildScriptsLibFolderName.Replace("\", "/"))/Prepare_Engine.ps1`" -Distribution `"$Distribution`" -EnginePath `"$RepositoryRoot/$($BuildScriptsFolderName)/Temp/UE`"
popd

# Download artifacts if required by the transport.
$($local:ArtifactsDownloadScript)

# Patch BuildGraph for the engine we are targeting.
pushd `"$local:RepositoryRoot/$local:ShortHash`"
pwsh `"$local:RepositoryRoot/$($local:BuildScriptsLibFolderName.Replace("\", "/"))/Patch_BuildGraph.ps1`" -Engine `"$local:RepositoryRoot/$($local:BuildScriptsFolderName)/Temp/UE`"

# Set up the remote artifacts directory properly.
if [ -e "$local:RepositoryRoot/RemoteArtifacts/" ]; then rm -Rf `"$local:RepositoryRoot/RemoteArtifacts`"; fi
if [ -e "$local:RepositoryRoot/RemoteInputArtifacts/" ]; then echo `"Moving remote input artifacts into place...`"; mv `"$local:RepositoryRoot/RemoteInputArtifacts`" `"$local:RepositoryRoot/RemoteArtifacts`"; fi
if [ -e "$local:RepositoryRoot/RemoteArtifacts/" ]; then echo `"Remote artifacts folder looks like:`"; ls -la "$local:RepositoryRoot/RemoteArtifacts/"; fi

# Remove any previous, local BuildGraph metadata.
if [ -e `"$local:RepositoryRoot/$($local:BuildScriptsFolderName)/Temp/UE/Engine/Saved/BuildGraph`" ]; then rm -Rf `"$local:RepositoryRoot/$($local:BuildScriptsFolderName)/Temp/UE/Engine/Saved/BuildGraph`"; fi

# Execute BuildGraph!
export IsBuildMachine=`"1`"
echo "Executing BuildGraph..."
pwsh `"$local:RepositoryRoot/$($local:BuildScriptsLibFolderName.Replace("\", "/"))/Internal_RunUAT.ps1`" -UATEnginePath `"$local:RepositoryRoot/$($local:BuildScriptsFolderName)/Temp/UE`" BuildGraph \
    -Script=`"$local:RepositoryRoot/$($local:BuildScriptsFolderName)/Temp/UE/Engine/Build/InstalledEngineBuild.xml`" \
    -SingleNode=`"$($local:Node.Name)`" \
    -SharedStorageDir=`"$local:MacSharedStorageDirectory`" \
    -NoP4 \
    $(if ($local:BuildJob.CanWriteToSharedStorage) { "-WriteToSharedStorage" } else { "" }) \
    $($local:Environment.Mac.BuildGraphSettings)

# Upload artifacts if required by the transport.
popd
$($local:ArtifactsUploadScript)
"@
                }

                $local:BuildPipeline.Jobs += $local:BuildJob
            }
            else {
                Write-Warning "'$AgentType' is not a known agent type!"
                continue
            }
        }
    }
}

function Convert-BuildGraphToBuildPipeline(
    # The distribution that is being built.
    [Parameter(Mandatory = $true)][string] $Distribution,
    # The distribution configuration.
    [Parameter(Mandatory = $true)] $DistributionConfig,
    # The BuildGraph JSON data.
    [Parameter(Mandatory = $true)] $BuildGraph,
    # The name of the BuildGraph script underneath BuildScripts. This is only used for non-engine builds.
    [string] $BuildGraphScriptName,
    # The environment settings for the build.
    [Parameter(Mandatory = $true)][BuildGraphEnvironment] $Environment,
    # Is this building or targeting Unreal Engine 5?
    [Parameter(Mandatory = $true)][bool] $IsUnrealEngine5
) {
    # Parameter validation.
    if ($local:Environment.Windows -eq $null -or $local:Environment.Mac -eq $null) {
        Write-Error "Missing Windows or macOS environments from build environment."
        return $null
    }
    if (!$local:Environment.IsEngineBuild) {
        if ($local:Environment.Engine -eq $null) {
            Write-Error "Missing 'Engine' value from build environment (and we're not building the engine)."
            return $null
        }
        if ($local:BuildGraphScriptName -eq $null) {
            Write-Error "Missing -BuildGraphScriptName parameter (and we're not building the engine)."
            return $null
        }
        if ($local:Environment.Windows.ProjectRoot -eq $null) {
            Write-Error "Missing 'Windows.ProjectRoot' value from build environment (and we're not building the engine)."
            return $null
        }
        if ($local:Environment.Mac.ProjectRoot -eq $null) {
            Write-Error "Missing 'Mac.ProjectRoot' value from build environment (and we're not building the engine)."
            return $null
        }
    }

    # Define some constants (so they're easy to change if we need to later).
    $local:BuildScriptsFolderName = "BuildScripts"
    $local:BuildScriptsLibFolderName = "BuildScripts\Lib"

    # Encode the Git checkout script, since we need to embed it inside the CI/CD environment variables.
    $local:GitCheckoutScriptText = [System.Text.Encoding]::ASCII.GetBytes((Get-Content -Raw -Path "$PSScriptRoot\GitCheckout_Optimized.ps1"))
    $local:MS = New-Object IO.MemoryStream
    $local:CS = New-Object System.IO.Compression.GZipStream($local:MS, [IO.Compression.CompressionMode]"Compress")
    $local:CS.Write($local:GitCheckoutScriptText, 0, $local:GitCheckoutScriptText.Length)
    $local:CS.Close()
    $local:GitCheckoutScriptEncoded = [Convert]::ToBase64String($local:MS.ToArray())
    $local:MS.Close()
    $local:CS = $null
    $local:MS = $null

    # Compute the short hash for storing the build in.
    $local:ShortHash = "W" + (Get-StringHash -InputString "$local:Distribution-$($local:Environment.Engine)" -Length 4)

    # Compute the shared storage folder name.
    $local:SharedStorageFolderName = (Get-StringHash -InputString "$($local:Environment.PipelineId)-$local:Distribution-$($local:Environment.Engine)" -Length 8)

    # Create our return value, which will contain the global environment variables, stages and build jobs.
    $local:BuildPipeline = @{
        GlobalEnvironmentVariables = @{};
        Stages                     = @();
        Jobs                       = @();
    }
    
    # Compute the environment variables for the pipeline.
    $local:BuildPipeline.GlobalEnvironmentVariables = @{
        GIT_STRATEGY                  = "none";
        GIT_OPTIMIZED_CHECKOUT_SCRIPT = $local:GitCheckoutScriptEncoded;
    }
    if (Get-IsPlugin) {
        $local:BuildPipeline.GlobalEnvironmentVariables["BUILDING_FOR_REDISTRIBUTION"] = "true"
    }
    if ($local:DistributionConfig.EnvironmentVariables -ne $null) {
        foreach ($local:EnvKey in $local:DistributionConfig.EnvironmentVariables | Get-Member -MemberType NoteProperty | Select -ExpandProperty Name) {
            $local:BuildPipeline.GlobalEnvironmentVariables[$local:EnvKey] = ($local:DistributionConfig.EnvironmentVariables.$local:EnvKey).ToString()
        }
    }

    # Compute the agent types that we match on.
    $local:AgentTypes = @{
        "Win64" = @("Win64");
        "Mac"   = @("Mac");
    }
    if ($local:Environment.IsEngineBuild) {
        $local:AgentTypes["Win64"] = @("Win64_Licensee", "Win64", "HoloLens")
        $local:AgentTypes["Mac"] = @("Mac_Licensee", "Mac")
    }

    # This is a super hack for engine builds, where we don't want the Editor Win64 node to block
    # preparation of Editor Mac targets. This involves moving "Update Version Files" into it's own
    # group at the start of the build.
    if ($local:Environment.IsEngineBuild) {
        $local:NewGroup = @{
            Name          = "Update Version Files";
            "Agent Types" = @(
                "Win64_Licensee"
            );
            Nodes         = @();
        }
        for ($local:i = 0; $local:i -lt $local:BuildGraph.Groups.Length; $local:i++) {
            if ($local:BuildGraph.Groups[$i].Name -eq "Editor Win64") {
                foreach ($local:Node in $local:BuildGraph.Groups[$i].Nodes) {
                    if ($local:Node.Name -eq "Update Version Files") {
                        $local:NewGroup.Nodes += $local:Node
                        $local:BuildGraph.Groups[$local:i].Nodes = `
                            $local:BuildGraph.Groups[$local:i].Nodes | `
                            Where-Object { `
                                $_.Name -ne "Update Version Files";
                        };
                        break
                    }
                }
                break
            }
        }
        $local:BuildGraph.Groups += $local:NewGroup
    }

    # When we a building an engine and have external macOS agents, we need to copy not only
    # the dependencies of the macOS jobs, but also the dependencies of their dependencies. This isn't
    # an issue for project and plugin builds, since our BuildGraph XML already includes dependencies
    # of dependencies for these jobs, but the BuildGraph XML shipped in the engine does not.
    $local:NodeToFullDependenciesArray = @{}
    if ($local:Environment.IsEngineBuild) {
        $local:NodeToDependenciesArray = @{}
        foreach ($local:Group in $local:BuildGraph.Groups) {
            foreach ($local:Node in $local:Group.Nodes) {
                $local:Needs = $local:Node.DependsOn.Split(";")
                if ($local:Needs.Length -eq 1 -and $local:Needs[0] -eq "") {
                    $local:Needs = @()
                }
                $local:NodeToDependenciesArray[$local:Node.Name] = $local:Needs
            }
        }
        function Get-NodeDependencies([string] $local:Node) {
            $local:Deps = @()
            foreach ($local:Dep in $NodeToDependenciesArray[$local:Node]) {
                if (!$local:Deps.Contains($local:Dep)) {
                    $local:Deps += $local:Dep
                    foreach ($local:DepDep in (Get-NodeDependencies $local:Dep)) {
                        if (!$local:Deps.Contains($local:DepDep)) {
                            $local:Deps += $local:DepDep
                        }
                    }
                }
            }
            return $local:Deps
        }
        foreach ($local:Node in $local:NodeToDependenciesArray.Keys) {
            $local:NodeToFullDependenciesArray[$Node] = Get-NodeDependencies $local:Node
        }
    }

    # Compute the stages. This is done differently for custom Unreal Engine builds vs project or plugin builds.
    if ($local:Environment.IsEngineBuild) {
        # For engine builds, we really have to force the Mac/IOS targets to be prioritized over Windows. Re-order
        # the stages to make sure they run in the right order.
        $local:NodeToStageMap = @{}
        $local:StageToDepMap = @{}
        $local:GroupNameToGroupMap = @{}
        foreach ($Group in $local:BuildGraph.Groups) {
            $local:GroupNameToGroupMap[$Group.Name] = $Group
            foreach ($Node in $Group.Nodes) {
                $local:NodeToStageMap[$Node.Name] = $Group.Name
            }
        }
        foreach ($Group in $local:BuildGraph.Groups) {
            $local:StageDeps = @()
            foreach ($Node in $Group.Nodes) {
                $local:NeedsArray = $local:NodeToFullDependenciesArray[$Node.Name]
                if ($local:NeedsArray -is [string]) {
                    $local:NeedsArray = @($local:NeedsArray)
                }
                foreach ($Need in $local:NeedsArray) {
                    $local:StageDep = $local:NodeToStageMap[$Need]
                    if (!$local:StageDeps.Contains($local:StageDep)) {
                        $local:StageDeps += $local:StageDep 
                    }
                }
            }
            $local:StageToDepMap[$Group.Name] = $local:StageDeps
        }
        $local:StagesToAdd = @()
        foreach ($Group in $local:BuildGraph.Groups) {
            if ($Group.Name.Contains("Mac") -or $Group.Name.Contains("IOS")) {
                $local:StagesToAdd += $Group.Name
            }
        }
        foreach ($Group in $local:BuildGraph.Groups) {
            if (!($Group.Name.Contains("Mac") -or $Group.Name.Contains("IOS"))) {
                $local:StagesToAdd += $Group.Name
            }
        }
        $global:OrderedPriorityStages = @()
        function Process-PrioritizedGroup($GroupName, $StageToDepMap, $Seen) {
            $local:NeedsArray = $local:StageToDepMap[$local:GroupName]
            $local:Seen += $GroupName
            foreach ($Need in $local:NeedsArray) {
                if ($local:Seen.Contains($local:Need)) {
                    continue
                }
                if (!$global:OrderedPriorityStages.Contains($Need)) {
                    Process-PrioritizedGroup $local:Need $local:StageToDepMap $local:Seen
                    $local:Seen += $local:Need
                }
            }
            $global:OrderedPriorityStages += $GroupName
        }
        foreach ($GroupName in $local:StagesToAdd) {
            Process-PrioritizedGroup $local:GroupName $local:StageToDepMap @()
        }
        $local:Emitted = @()
        foreach ($GroupName in $global:OrderedPriorityStages) {
            if ($local:Emitted.Contains($local:GroupName)) {
                continue
            }
            $local:Emitted += $local:GroupName
            $local:Group = $local:GroupNameToGroupMap[$local:GroupName]
            $local:BuildPipeline.Stages += $Group.Name
        }
    }
    else {
        # For project and plugin builds, we've already arranged the BuildGraph scripts to run macOS as
        # early as possible.
        foreach ($Group in $local:BuildGraph.Groups) {
            $local:BuildPipeline.Stages += $Group.Name
        }
    }

    # Generate Mac jobs first so that the "dependency in" jobs that need to run on Windows
    # (which are usually quick on Windows) run first. That way, macOS can get started doin
    # the actual build work while Windows moves onto doing it's actual compiling.
    # todo: Why does this have to be backwards to get jobs in the right order???
    Internal-Generate-BuildJobs `
        -BuildPipeline $local:BuildPipeline `
        -Distribution $local:Distribution `
        -DistributionConfig $local:DistributionConfig `
        -BuildGraph $local:BuildGraph `
        -BuildGraphScriptName $local:BuildGraphScriptName `
        -Environment $local:Environment `
        -NodeToFullDependenciesArray $local:NodeToFullDependenciesArray `
        -AgentTypes $local:AgentTypes `
        -ShortHash $local:ShortHash `
        -SharedStorageFolderName $local:SharedStorageFolderName `
        -BuildScriptsFolderName $local:BuildScriptsFolderName `
        -BuildScriptsLibFolderName $local:BuildScriptsLibFolderName `
        -TargetAgentType "Win64" `
        -IsUnrealEngine5 $local:IsUnrealEngine5
    Internal-Generate-BuildJobs `
        -BuildPipeline $local:BuildPipeline `
        -Distribution $local:Distribution `
        -DistributionConfig $local:DistributionConfig `
        -BuildGraph $local:BuildGraph `
        -BuildGraphScriptName $local:BuildGraphScriptName `
        -Environment $local:Environment `
        -NodeToFullDependenciesArray $local:NodeToFullDependenciesArray `
        -AgentTypes $local:AgentTypes `
        -ShortHash $local:ShortHash `
        -SharedStorageFolderName $local:SharedStorageFolderName `
        -BuildScriptsFolderName $local:BuildScriptsFolderName `
        -BuildScriptsLibFolderName $local:BuildScriptsLibFolderName `
        -TargetAgentType "Mac" `
        -IsUnrealEngine5 $local:IsUnrealEngine5

    # Return the generated pipeline.
    return $local:BuildPipeline
}

function Dump-BuildPipeline($BuildPipeline) {
    Write-Host "--- Generated pipeline ---"
    Write-Host
    Write-Host "Global environment variables:"
    foreach ($Key in $BuildPipeline.GlobalEnvironmentVariables.Keys) {
        Write-Host " - $Key = $($BuildPipeline.GlobalEnvironmentVariables.Item($Key))"
    }
    Write-Host
    Write-Host "Stages:"
    foreach ($Stage in $BuildPipeline.Stages) {
        Write-Host " - $Stage"
    }
    Write-Host
    Write-Host "Jobs:"
    Write-Host
    foreach ($Job in $BuildPipeline.Jobs) {
        Write-Host "-- $($Job.Name) --"
        Write-Host
        Write-Host "Stage: $($Job.Stage)"
        Write-Host "Platform: $($Job.Platform)"
        Write-Host "Manual? $(if ($Job.IsManual) { "true" } else { "false" })"
        Write-Host "Can write to shared storage? $(if ($Job.CanWriteToSharedStorage) { "true" } else { "false" })"
        Write-Host "Needs:"
        foreach ($Need in $Job.Needs) {
            Write-Host " - $Need"
        }
        Write-Host "Artifact paths:"
        foreach ($Path in $Job.ArtifactPaths) {
            Write-Host " - $Path"
        }
        Write-Host "Artifact JUnit report path: $($Job.ArtifactJUnitReportPath)"
        Write-Host "Resource group: $($Job.ResourceGroup)"
        Write-Host "Retries: $($Job.Retries)"
        Write-Host "Agent override tag: $($Job.AgentOverrideTag)"
        Write-Host "Environment variables:"
        foreach ($Key in $Job.EnvironmentVariables.Keys) {
            Write-Host " - $Key = $($Job.EnvironmentVariables.Item($Key))"
        }
        Write-Host 
        Write-Host "# Script"
        Write-Host $Job.Script
        Write-Host 
        Write-Host "# After Script"
        Write-Host $Job.AfterScript
        Write-Host
    }
}