param(
    [Parameter(Mandatory=$true)][string] $ZipPath,
    [Parameter(Mandatory=$true)][string] $BucketName,
    [Parameter(Mandatory=$true)][string] $FolderEnvVar,
    [switch][bool] $IsForMarketplace
)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Internal_SetupEnvironment.ps1

$ZipFile = Get-Item $ZipPath

$FolderPrefixEnvVar = (Get-Item "Env:\$FolderEnvVar" -ErrorAction SilentlyContinue)
if ($FolderPrefixEnvVar -eq $null) {
    Write-Error "Backblaze keys not available - not running upload step."
    exit 1
}
$FolderPrefix = ($FolderPrefixEnvVar).Value
if ($env:DL_BACKBLAZE_B2_KEY_ID -eq $null -or $env:DL_BACKBLAZE_B2_APPLICATION_KEY -eq $null -or $FolderPrefix -eq $null) {
    Write-Error "Backblaze keys not available - not running upload step."
    exit 1
}
$env:B2_ACCOUNT_INFO = Join-Path -Path (Resolve-Path -Path ".") -ChildPath ".b2.db"
try {
    b2 authorize-account "$env:DL_BACKBLAZE_B2_KEY_ID" "$env:DL_BACKBLAZE_B2_APPLICATION_KEY"
    if ($LASTEXITCODE -ne 0) {
        exit $LASTEXITCODE
    }
    b2 upload-file "$BucketName" "$ZipPath" "$FolderPrefix/$($ZipFile.Name)"
    if ($LASTEXITCODE -ne 0) {
        exit $LASTEXITCODE
    }
} finally {
    if (Test-Path $env:B2_ACCOUNT_INFO) {
        Write-Output "Cleaning up B2 authentication file..."
        Remove-Item -Force -Path $env:B2_ACCOUNT_INFO
    }
}

Write-Output "Uploaded as: $ProjectName-$Distribution-$VersionName.zip"
exit 0