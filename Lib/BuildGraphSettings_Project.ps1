param()

function Get-BuildGraphSettingsForProject(
    [string] $ProjectName,
    [string] $FolderName,
    $DistributionConfig,
    [string] $EnginePath, 
    [bool] $StrictIncludes,
    [bool] $ExecuteBuild,
    [bool] $ExecuteTests,
    [bool] $ExecuteDeployment,
    [string] $MacPlatforms = "Mac;IOS"
) {
    # Determine if the engine is Unreal Engine 5.
    $local:IsUnrealEngine5 = $false
    if ($global:IsMacOS -or $global:IsLinux) {
        $local:RunUAT = Get-Content -Raw -Path "$EnginePath/Engine/Build/BatchFiles/RunUAT.sh"
        if ($local:RunUAT.Contains("Unreal Engine AutomationTool setup script")) {
            $local:IsUnrealEngine5 = $true
        }
    } else {
        $local:RunUAT = Get-Content -Raw -Path "$EnginePath\Engine\Build\BatchFiles\RunUAT.bat"
        if ($local:RunUAT.Contains("SET_TURNKEY_VARIABLES")) {
            $local:IsUnrealEngine5 = $true
        }
    }

    # Determine build matrix.
    $local:EditorConfig = Compute-BuildGraphInputArgs $local:DistributionConfig "Editor" $local:IsUnrealEngine5
    $local:GameConfig = Compute-BuildGraphInputArgs $local:DistributionConfig "Game" $local:IsUnrealEngine5
    $local:ClientConfig = Compute-BuildGraphInputArgs $local:DistributionConfig "Client" $local:IsUnrealEngine5
    $local:ServerConfig = Compute-BuildGraphInputArgs $local:DistributionConfig "Server" $local:IsUnrealEngine5

    # Compute prepare scripts.
    $local:PrepareCustomCompileScripts = @()
    if ($local:DistributionConfig.Prepare -ne $null) {
        foreach ($local:Prepare in $local:DistributionConfig.Prepare) {
            if ($local:Prepare.Type -eq "Custom" -and $local:Prepare.RunBefore -ne $null) {
                if ($local:Prepare.RunBefore.Contains("Compile")) {
                    $local:PrepareCustomCompileScripts += $local:Prepare.ScriptPath
                }
            }
        }
    }
    $local:PrepareCustomCompileScripts = $local:PrepareCustomCompileScripts -join ";"

    # Compute custom tests.
    $local:CustomTests = @()
    if ($local:DistributionConfig.Tests -ne $null) {
        foreach ($local:Test in $local:DistributionConfig.Tests) {
            if ($local:Test.Type -eq "Custom" -and $local:Test.Custom -ne $null) {
                $local:CustomTests += "$($local:Test.Name)~$($local:Test.Custom.ScriptPath)"
            }
        }
    }

    # Determine Gauntlet tasks.
    $local:GauntletTests = @()
    if ($local:DistributionConfig.Tests -ne $null) {
        foreach ($Test in $local:DistributionConfig.Tests) {
            if ($Test.Type -eq "Gauntlet") {
                $Requires = @()
                foreach ($Require in $Test.Requires) {
                    foreach ($Platform in $Require.Platforms) {
                        $Requires += "#$($Require.Type)Staged_$($Require.Target)_$($Platform)_$($Require.Configuration)"
                    }
                }
                $local:GauntletTests += "$($Test.Name)~$($Requires -join ";")"
            }
        }
    }

    # Compute deployment tasks.
    $local:DeploymentSteam = @()
    if ($local:ExecuteDeployment -and ($null -ne $local:DistributionConfig.Deployment)) {
        foreach ($local:Deploy in $local:DistributionConfig.Deployment) {
            $local:ManualDeploy = "false";
            if ($local:Deploy.Manual) {
                $local:ManualDeploy = "true";
            }

            if ($local:Deploy.Type -eq "Steam") {
                $local:DeploymentSteam += "$($local:Deploy.Name);$($local:ManualDeploy);$($local:Deploy.Package.Type);$($local:Deploy.Package.Target);$($local:Deploy.Package.Platform);$($local:Deploy.Package.Configuration);$($local:Deploy.Steam.AppID);$($local:Deploy.Steam.DepotID);$($local:Deploy.Steam.Channel)"
            }
        }
    }

    # Compute final settings for BuildGraph.
    return @{
        # Environment options
        BuildScriptsPath = "__REPOSITORY_ROOT__/BuildScripts";
        BuildScriptsLibPath = "__REPOSITORY_ROOT__/BuildScripts/Lib";
        TempPath = "__REPOSITORY_ROOT__/BuildScripts/Temp";
        ProjectRoot = "__REPOSITORY_ROOT__/$($local:FolderName)";
        RepositoryRoot = "__REPOSITORY_ROOT__";

        # General options
        UProjectPath = "__REPOSITORY_ROOT__/$($local:FolderName)/$($local:ProjectName).uproject";
        Distribution = $local:DistributionConfig.Name;
        IsUnrealEngine5 = if ($local:IsUnrealEngine5) { "true" } else { "false" };

        # Prepare options
        PrepareCustomCompileScripts = $local:PrepareCustomCompileScripts;

        # Build options
        ExecuteBuild = if ($local:ExecuteBuild) { "true" } else { "false" };
        EditorTarget = $local:EditorConfig.Targets.Split(";")[0];
        GameTargets = $local:GameConfig.Targets;
        ClientTargets = $local:ClientConfig.Targets;
        ServerTargets = $local:ServerConfig.Targets;
        GameTargetPlatforms = $local:GameConfig.TargetPlatforms;
        ClientTargetPlatforms = $local:ClientConfig.TargetPlatforms;
        ServerTargetPlatforms = $local:ServerConfig.TargetPlatforms;
        GameConfigurations = $local:GameConfig.Configurations;
        ClientConfigurations = $local:ClientConfig.Configurations;
        ServerConfigurations = $local:ServerConfig.Configurations;
        MacPlatforms = $MacPlatforms;
        StrictIncludes = if ($local:StrictIncludes) { "true" } else { "false" };

        # Stage options
        StageDirectory = "__REPOSITORY_ROOT__/$($local:FolderName)/Saved/StagedBuilds";

        # Test options
        ExecuteTests = if ($local:ExecuteTests) { "true" } else { "false" };
        GauntletTests = ($local:GauntletTests -join "+");
        CustomTests = ($local:CustomTests -join "+");

        # Deploy options
        DeploymentSteam = ($local:DeploymentSteam -join "+");
    }
}