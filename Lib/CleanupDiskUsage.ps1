param(
    [Parameter(Mandatory=$true)][string] $RepositoryRoot,
    [Parameter(Mandatory=$true)][string] $ShortHash,
    [Parameter(Mandatory=$false)][string] $SharedBuildArtifacts,
    [Parameter(Mandatory=$false)][string] $ArtifactsId
)

if ($global:IsMacOS -or $global:IsLinux) {
    # Currently don't support automatic clean up on these platforms.
    exit 0
}

#
# This script evaluates disk usage and automatically cleans up
# old build folders to ensure there's enough disk space for
# the build to run.
#

$DateTimeCleanupThreshold = (Get-Date).Add([timespan]::FromHours(-2))

Write-Host "[Space cleanup] Short hash: $ShortHash"
Write-Host "[Space cleanup] Repository root: $RepositoryRoot"
Write-Host "[Space cleanup] Resolved repository root: $((Resolve-Path $RepositoryRoot).Path)"
Write-Host "[Space cleanup] Repository root qualifier: $((Split-Path -Path (Resolve-Path $RepositoryRoot) -Qualifier))"
Write-Host "[Space cleanup] Repository root drive letter: $((Split-Path -Path (Resolve-Path $RepositoryRoot) -Qualifier).Substring(0, 1))"

$RepositoryDrive = (Get-PSDrive (Split-Path -Path (Resolve-Path $RepositoryRoot) -Qualifier).Substring(0, 1))
$RepositoryUsed = [double]$RepositoryDrive.Used
$RepositoryFree = [double]$RepositoryDrive.Free
$RepositoryPercentageFree = ($RepositoryFree / $RepositoryUsed * 100)
if ($RepositoryPercentageFree -lt 20) {
    # Less than 20% disk space left, remove any W.... directories that aren't ours.
    Write-Host "[Space cleanup] Less than 20% space left on build drive, removing old variants..."
    foreach ($Child in Get-ChildItem -Path $RepositoryRoot -Directory) {
        if ($Child.Name.StartsWith("W") -and $Child.Name.Length -le 5) {
            if ($Child.Name -ne $ShortHash) {
                if ($Child.LastWriteTime -lt $DateTimeCleanupThreshold) {
                    Write-Host "[Space cleanup] Deleting $($Child.Name)..."
                    Remove-Item -Force -Recurse $Child.FullName
                } else {
                    Write-Host "[Space cleanup] Skipping $($Child.Name) because it was last written too recently..."
                }
            }
        }
    }
}

if ($null -ne $SharedBuildArtifacts -and $SharedBuildArtifacts -ne "") {
    Write-Host "[Space cleanup] Artifacts ID: $ArtifactsId"
    Write-Host "[Space cleanup] Artifacts folder: $SharedBuildArtifacts"
    Write-Host "[Space cleanup] Resolved artifacts folder: $((Resolve-Path $SharedBuildArtifacts).Path)"
    Write-Host "[Space cleanup] Artifacts folder qualifier: $((Split-Path -Path (Resolve-Path $SharedBuildArtifacts) -Qualifier))"
    Write-Host "[Space cleanup] Artifacts folder drive letter: $((Split-Path -Path (Resolve-Path $SharedBuildArtifacts) -Qualifier).Substring(0, 1))"

    $ArtifactsDrive = (Get-PSDrive (Split-Path -Path (Resolve-Path $SharedBuildArtifacts) -Qualifier).Substring(0, 1))
    $ArtifactsUsed = [double]$ArtifactsDrive.Used
    $ArtifactsFree = [double]$ArtifactsDrive.Free
    $ArtifactsFree = ($ArtifactsFree / $ArtifactsUsed * 100)
    if ($RepositoryPercentageFree -lt 20) {
        # Less than 20% disk space left, remove any directories that aren't ours.
        Write-Host "[Space cleanup] Less than 20% space left on artifacts drive, removing old artifacts..."
        foreach ($Child in Get-ChildItem -Path $SharedBuildArtifacts -Directory) {
            if ($Child.Name -ne $ArtifactsId) {
                if ($Child.LastWriteTime -lt $DateTimeCleanupThreshold) {
                    Write-Host "[Space cleanup] Deleting $($Child.Name)..."
                    Remove-Item -Force -Recurse $Child.FullName
                } else {
                    Write-Host "[Space cleanup] Skipping $($Child.Name) because it was last written too recently..."
                }
            }
        }
    }
}

# Do a second, final pass of the build folder. If we're still less than 20%, bypass the date time threshold.
$RepositoryUsed = [double]$RepositoryDrive.Used
$RepositoryFree = [double]$RepositoryDrive.Free
$RepositoryPercentageFree = ($RepositoryFree / $RepositoryUsed * 100)
if ($RepositoryPercentageFree -lt 20) {
    # Less than 20% disk space left, remove any W.... directories that aren't ours.
    Write-Host "[Space cleanup] Still less than 20% space left on build drive! Bypassing date-time threshold to remove as much as possible..."
    foreach ($Child in Get-ChildItem -Path $RepositoryRoot -Directory) {
        if ($Child.Name.StartsWith("W") -and $Child.Name.Length -le 5) {
            if ($Child.Name -ne $ShortHash) {
                Write-Host "[Space cleanup] Deleting $($Child.Name)..."
                Remove-Item -Force -Recurse $Child.FullName
            }
        }
    }
}

