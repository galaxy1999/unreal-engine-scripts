param(
    # The distribution name, as it appears in BuildConfig.json.
    [Parameter(Mandatory = $true)][string] $Distribution,
    # The path the engine will be cloned to.
    [Parameter(Mandatory = $true)][string] $EnginePath
)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Internal_SetupEnvironment.ps1

$DistributionConfig = Get-Distribution $Distribution

# Clone the engine.
& "$PSScriptRoot\GitCheckout_Optimized.ps1" `
    -TargetDirectory $EnginePath `
    -SourceUrlOverride $DistributionConfig.Source.Repository `
    -RefOverride $DistributionConfig.Source.Ref `
    -IsEngineBuild

# Copy all of the console platforms, and do it in parallel.
if (!$global:IsMacOS -and !$global:IsLinux) {
    if ($null -ne $DistributionConfig.Build.ConsoleDirectories) {
        Start-Section "console-copy" "Copying console implementations to engine..."
        $CopyJobs = @()
        foreach ($ConsoleDirectory in $DistributionConfig.Build.ConsoleDirectories) {
            $CopyJobs += Start-Job -ArgumentList @($ConsoleDirectory, $EnginePath) -ScriptBlock {
                param([string] $SourcePath, [string] $DestinationPath)

                Write-Output "Robocopy '$SourcePath' -> '$DestinationPath': Started..."
                $Stopwatch = [System.Diagnostics.StopWatch]::StartNew()
                robocopy "$SourcePath" "$DestinationPath" /E /NS /NC /NFL /NDL /NP /NJH /NJS 2> $null | ForEach-Object {
                    if ($_ -ne $null -and $_.Trim() -ne "") {
                        Write-Output $_.Trim()
                    }
                }
                $Stopwatch.Stop()
                if ($LastExitCode -ge 8) {
                    Write-Error "Robocopy '$SourcePath' -> '$DestinationPath': Failed in $([int][System.Math]::Ceiling($Stopwatch.Elapsed.TotalSeconds)) secs"
                }
                else {
                    Write-Output "Robocopy '$SourcePath' -> '$DestinationPath': Success in $([int][System.Math]::Ceiling($Stopwatch.Elapsed.TotalSeconds)) secs"
                }
            }
        }
        Receive-Job -Job $CopyJobs -Wait -AutoRemoveJob
        Stop-Section "console-copy"
    }
}

# Copy all of the patches, and do it in parallel.
if ($null -ne $DistributionConfig.Build.PatchDirectories) {
    Start-Section "patch-copy" "Copying patches to engine..."
    $CopyJobs = @()
    foreach ($PatchRelativeDirectory in $DistributionConfig.Build.PatchDirectories) {
        $PatchDirectory = (Join-Path $ProjectRoot $PatchRelativeDirectory)
        if (Test-Path $PatchDirectory) {
            $CopyJobs += Start-Job -ArgumentList @($PatchDirectory, $EnginePath) -ScriptBlock {
                param([string] $SourcePath, [string] $DestinationPath)

                $CommandName = "Robocopy"
                if (!$global:IsMacOS -and !$global:IsLinux) {
                    $CommandName = "Rsync"
                }
                Write-Output "$CommandName '$SourcePath' -> '$DestinationPath': Started..."
                $Stopwatch = [System.Diagnostics.StopWatch]::StartNew()
                if (!$global:IsMacOS -and !$global:IsLinux) {
                    robocopy "$SourcePath" "$DestinationPath" /E /NS /NC /NFL /NDL /NP /NJH /NJS 2> $null | ForEach-Object {
                        if ($_ -ne $null -and $_.Trim() -ne "") {
                            Write-Output $_.Trim()
                        }
                    }
                }
                else {
                    rsync -a -q "$($SourcePath.Replace("\", "/"))" "$DestinationPath"
                }
                $Stopwatch.Stop()
                if ($LastExitCode -ge 8) {
                    Write-Error "$CommandName '$SourcePath' -> '$DestinationPath': Failed in $([int][System.Math]::Ceiling($Stopwatch.Elapsed.TotalSeconds)) secs"
                }
                else {
                    Write-Output "$CommandName '$SourcePath' -> '$DestinationPath': Success in $([int][System.Math]::Ceiling($Stopwatch.Elapsed.TotalSeconds)) secs"
                }
            }
        }
    }
    Receive-Job -Job $CopyJobs -Wait -AutoRemoveJob
    Stop-Section "patch-copy"
}

# If we have confidential platforms, patch the rsync rules to ensure confidential platforms don't get copied to remote macOS build machines.
if (Test-Path "$EnginePath\Engine\Platforms") {
    Start-Section "discover-conplats" "Discovering confidential platforms..."
    $ConfidentialPlatformNames = @()
    foreach ($DataDrivenPlatformInfo in (Get-ChildItem -Path "$EnginePath\Engine\Platforms" -Filter DataDrivenPlatformInfo.ini -Recurse)) {
        $PlatformInfo = Get-Content -Path $DataDrivenPlatformInfo.FullName -Raw
        if (!$PlatformInfo.Contains("bIsConfidential=true")) {
            continue
        }
        $ConfidentialPlatformNames += $DataDrivenPlatformInfo.Directory.Parent.Name
    }
    Stop-Section "discover-conplats"
    Start-Section "patch-rsync" "Patching rsync files to exclude confidential platforms..."
    $RsyncEntries = @()
    $RsyncEntries += "# RSYNC PATCH START <<"
    foreach ($PlatformName in $ConfidentialPlatformNames) {
        $RsyncEntries += "- /**/$PlatformName/"
        $RsyncEntries += "- /**/${PlatformName}Extras/"
    }
    $RsyncEntries += "# >>"
    $RsyncEntries = $RsyncEntries -join "`r`n"
    foreach ($RsyncFile in (Get-ChildItem -Path "$EnginePath\Engine\Build\Rsync" -Filter *.txt)) {
        if ($RsyncFile.Name -eq "RsyncEngineScripts.txt") {
            continue
        }
        $RsyncContent = Get-Content -Raw -Path $RsyncFile.FullName
        if (!$RsyncContent.Contains("# RSYNC PATCH START <<")) {
            $RsyncContent = "# RSYNC PATCH START <<`r`n# >>`r`n$RsyncContent"
        }
        $RsyncContentNew = ($RsyncContent -replace "(?ms)# RSYNC PATCH START <<.*?# >>", $RsyncEntries)
        if ($RsyncContent -ne $RsyncContentNew) {
            Set-RetryableContent -Path $RsyncFile.FullName -Value $RsyncContentNew
        }
        Write-Output "patch $($RsyncFile.Name) #1: ok"
    }
    Stop-Section "patch-rsync"
}

# Detect if this is Unreal Engine 5.
$IsUnrealEngine5 = $false
if ((Get-Content -Raw "$EnginePath\Engine\Source\Programs\AutomationTool\BuildGraph\BuildGraph.Automation.csproj").Contains("netcoreapp")) {
    $IsUnrealEngine5 = $true
}

# Patch all of the remaining content.
$GauntletTargetDeviceMacPatchDataFileName = "Gauntlet.TargetDeviceMac.patchdata426"
$GauntletTargetDeviceMacConstructor = "TargetDeviceMac(InRef, InCachePath, InParam)"
if ((Get-Content -Path "$EnginePath\Engine\Source\Programs\AutomationTool\Gauntlet\Platform\Mac\Gauntlet.TargetDeviceMac.cs" -Raw).Contains("IsOSOutOfDate()")) {
    $GauntletTargetDeviceMacPatchDataFileName = "Gauntlet.TargetDeviceMac.patchdata427"
    $GauntletTargetDeviceMacConstructor = "TargetDeviceMac(InRef, InCachePath)"
}
$UnrealPrefix = "UE4"
if ($IsUnrealEngine5) {
    $UnrealPrefix = "Unreal"
}
$SimplePatches = @(
    @{
        SectionName        = "patch-rsync2";
        SectionDescription = "Patching rsync files to fixup macOS issues...";
        Files              = @(
            @{
                RelativePath = "Engine\Build\Rsync\RsyncEngine.txt";
                Patches      = @(
                    @{
                        CheckNotContains = "/Binaries/Mac/UnrealCEFSubProcess.app/";
                        Find             = "+ /Binaries/Mac/DsymExporter";
                        Replace          = @"
+ /Binaries/Mac/DsymExporter
+ /Binaries/Mac/UnrealCEFSubProcess.app/
+ /Binaries/Mac/UnrealCEFSubProcess.app/**
+ /Binaries/Mac/UnrealSync.app/
+ /Binaries/Mac/UnrealSync.app/**
+ /Binaries/Mac/$($UnrealPrefix)EditorServices.app/
+ /Binaries/Mac/$($UnrealPrefix)EditorServices.app/**
+ /Binaries/Mac/BootstrapPackagedGame.app/
+ /Binaries/Mac/BootstrapPackagedGame.app/**
"@;
                    },
                    @{
                        CheckNotContains = "+ /Source/Programs/CrashReportClient/";
                        Find             = "+ /Source/Programs/UnrealLightmass/";
                        Replace          = @"
+ /Source/Programs/CrashReportClient/
+ /Source/Programs/ShaderCompileWorker/
+ /Source/Programs/UnrealCEFSubProcess/
+ /Source/Programs/UnrealInsights/
+ /Source/Programs/UnrealFrontend/
+ /Source/Programs/UnrealLightmass/
+ /Source/Programs/UnrealMultiUserServer/
+ /Source/Programs/UnrealRecoverySvc/
+ /Source/Programs/BuildPatchTool/
+ /Source/Programs/Mac/
"@;
                    },
                    @{
                        CheckNotContains = "+ /Source/ThirdParty/Intel/ISPC/bin/Mac/**";
                        Find             = "+ /Source/ThirdParty/Eigen/**";
                        Replace          = @"
+ /Source/ThirdParty/Eigen/**
+ /Source/ThirdParty/Intel/ISPC/bin/Mac/**
"@;
                    }
                );
            },
            @{
                RelativePath = "Engine\Build\Rsync\RsyncEngineInstalled.txt";
                Patches      = @(
                    @{
                        CheckNotContains = "+ /Binaries/Mac/*.target";
                        Find             = "+ /Binaries/TVOS/*.target";
                        Replace          = @"
+ /Binaries/TVOS/*.target
+ /Binaries/Mac/
+ /Binaries/Mac/*.target
"@;
                    },
                    @{
                        CheckNotContains = "+ /Intermediate/Build/Mac/";
                        Find             = "+ /Intermediate/Build/TVOS/**";
                        Replace          = @"
+ /Intermediate/Build/TVOS/**
+ /Intermediate/Build/Mac/
+ /Intermediate/Build/Mac/**
"@;
                    },
                    @{
                        CheckNotContains = "+ /Plugins/**/Intermediate/Build/Mac/";
                        Find             = "+ /Plugins/**/Intermediate/Build/TVOS/**";
                        Replace          = @"
+ /Plugins/**/Intermediate/Build/TVOS/**
+ /Plugins/**/Intermediate/Build/Mac/ 
+ /Plugins/**/Intermediate/Build/Mac/**
"@;
                    }
                );
            },
            @{
                RelativePath = "Engine\Build\Rsync\RsyncProject.txt";
                Patches      = @(
                    @{
                        CheckNotContains = "+ /Intermediate/Mac/";
                        Find             = "+ /Intermediate/TVOS/**";
                        Replace          = @"
+ /Intermediate/TVOS/**
+ /Intermediate/Mac/
+ /Intermediate/Mac/**
"@;
                    },
                    @{
                        CheckNotContains = "+ /Build/Mac/";
                        Find             = "+ /Build/TVOS/**";
                        Replace          = @"
+ /Build/TVOS/**
+ /Build/Mac/
+ /Build/Mac/**
"@;
                    }
                );
            }
        );
    },
    @{
        SectionName        = "patch-android";
        SectionDescription = "Patching Android's doesFileExistInternal function...";
        Files              = @(
            @{
                RelativePath = "Engine\Build\Android\Java\src\com\google\android\vending\expansion\downloader\Helpers.java";
                Patches      = @(
                    @{
                        CheckContains = "`t";
                        Find          = "`t";
                        Replace       = "    ";
                    },
                    @{
                        CheckNotContains = "// Redpoint: Patched for Gauntlet";
                        Find             = @"
        if (fileForNewFile.exists()) {
            // ignore actual file size if requested filesize is 0 (special case allow any)
            if ((fileSize==0) || (fileForNewFile.length() == fileSize)) {
                return true;
            }
            if (deleteFileOnMismatch) {
                // delete the file --- we won't be able to resume
                // because we cannot confirm the integrity of the file
                fileForNewFile.delete();
            }
        }
"@;
                        Replace          = @"
        if (fileForNewFile.exists()) {
            // Redpoint: Patched for Gauntlet
            return true;
        }
"@;
                    }
                );
            }
        );
    },
    @{
        SectionName        = "patch-build-on-mac";
        SectionDescription = "Patching InstalledEngineBuild/Filters.xml to build macOS from Windows...";
        Files              = @(
            @{
                RelativePath = "Engine\Build\InstalledEngineBuild.xml";
                Patches      = @(
                    @{
                        CheckNotContains = @"
<Compile Target="UnrealInsights" Configuration="Development" Platform="Mac" Tag="#Build Tools Mac" If="'`$(HostPlatform)' != 'Win64'"/> <!-- Redpoint -->
"@;
                        Find             = @"
<Compile Target="UnrealInsights" Configuration="Development" Platform="Mac" Tag="#Build Tools Mac"/>
"@;
                        Replace          = @"
<Compile Target="UnrealInsights" Configuration="Development" Platform="Mac" Tag="#Build Tools Mac" If="'`$(HostPlatform)' != 'Win64'"/> <!-- Redpoint -->
"@;
                    },
                    @{
                        CheckNotContains = @"
<Property Name="DDCPlatformsWin64" Value="`$(DDCPlatformsWin64)+Mac" If="'`$(WithMac)' == true"/> <!-- Redpoint -->
"@;
                        Find             = @"
<Property Name="DDCPlatformsWin64" Value="`$(DDCPlatformsWin64)+Android_ATC" If="'`$(WithAndroid)' == true"/>
"@;
                        Replace          = @"
<Property Name="DDCPlatformsWin64" Value="`$(DDCPlatformsWin64)+Android_ATC" If="'`$(WithAndroid)' == true"/>
<Property Name="DDCPlatformsWin64" Value="`$(DDCPlatformsWin64)+Mac" If="'`$(WithMac)' == true"/> <!-- Redpoint -->
"@;
                    },
                    @{
                        CheckNotContains = @"
<Property Name="InstalledRequirements" Value="`$(InstalledRequirements);#$($UnrealPrefix)Game Mac;#$($UnrealPrefix)Game Mac Unstripped;#$($UnrealPrefix)Game Mac Stripped;#$($UnrealPrefix)Game Mac Unsigned;#$($UnrealPrefix)Game Mac Signed;#Build Tools Mac" If="'`$(WithMac)' == true"/> <!-- Redpoint -->
"@;
                        Find             = @"
<Property Name="InstalledRequirements" Value="`$(InstalledRequirements);#$($UnrealPrefix)Game Win32;#$($UnrealPrefix)Game Win32 Unstripped;#$($UnrealPrefix)Game Win32 Stripped;#$($UnrealPrefix)Game Win32 Unsigned;#$($UnrealPrefix)Game Win32 Signed;#Build Tools Win32" If="'`$(WithWin32)' == true"/>
"@;
                        Replace          = @"
<Property Name="InstalledRequirements" Value="`$(InstalledRequirements);#$($UnrealPrefix)Game Win32;#$($UnrealPrefix)Game Win32 Unstripped;#$($UnrealPrefix)Game Win32 Stripped;#$($UnrealPrefix)Game Win32 Unsigned;#$($UnrealPrefix)Game Win32 Signed;#Build Tools Win32" If="'`$(WithWin32)' == true"/>
<Property Name="InstalledRequirements" Value="`$(InstalledRequirements);#$($UnrealPrefix)Game Mac;#$($UnrealPrefix)Game Mac Unstripped;#$($UnrealPrefix)Game Mac Stripped;#$($UnrealPrefix)Game Mac Unsigned;#$($UnrealPrefix)Game Mac Signed;#Build Tools Mac" If="'`$(WithMac)' == true"/> <!-- Redpoint -->
"@;
                    },
                    @{
                        CheckNotContains = @"
`t`t`t<Do If="'`$(WithMac)' == true">
`t`t`t`t<Tag Files="#$($UnrealPrefix)Game Mac;#Build Tools Mac" Except="#$($UnrealPrefix)Game Mac Unstripped;#$($UnrealPrefix)Game Mac Unsigned" With="#Installed Win64"/>
`t`t`t`t<Tag Files="#$($UnrealPrefix)Game Mac Unstripped;#$($UnrealPrefix)Game Mac Unsigned" With="#Saved Output"/>
`t`t`t`t<Property Name="Platforms" Value="`$(Platforms)Mac;"/>
`t`t`t`t<Property Name="CopyInstalledFilter" Value="`$(CopyInstalledFilter);`$(CopyMacFilterWin64)"/>
`t`t`t`t<Property Name="CopyInstalledExceptions" Value="`$(CopyInstalledExceptions);`$(CopyMacExceptions)"/>
`t`t`t</Do> <!-- Redpoint -->
"@;
                        Find             = @"
`t`t`t<Do If="'`$(WithWin32)' == true">
`t`t`t`t<Tag Files="#$($UnrealPrefix)Game Win32;#Build Tools Win32" Except="#$($UnrealPrefix)Game Win32 Unstripped;#$($UnrealPrefix)Game Win32 Unsigned" With="#Installed Win64"/>
`t`t`t`t<Tag Files="#$($UnrealPrefix)Game Win32 Stripped;#$($UnrealPrefix)Game Win32 Signed" With="#Saved Output"/>
`t`t`t`t<Property Name="Platforms" Value="`$(Platforms)Win32;"/>
`t`t`t`t<Property Name="CopyInstalledFilter" Value="`$(CopyInstalledFilter);`$(CopyWin32Filter)"/>
`t`t`t`t<Property Name="CopyInstalledExceptions" Value="`$(CopyInstalledExceptions);`$(CopyWin32Exceptions)"/>
`t`t`t</Do>
"@
                        Replace          = @"
`t`t`t<Do If="'`$(WithWin32)' == true">
`t`t`t`t<Tag Files="#$($UnrealPrefix)Game Win32;#Build Tools Win32" Except="#$($UnrealPrefix)Game Win32 Unstripped;#$($UnrealPrefix)Game Win32 Unsigned" With="#Installed Win64"/>
`t`t`t`t<Tag Files="#$($UnrealPrefix)Game Win32 Stripped;#$($UnrealPrefix)Game Win32 Signed" With="#Saved Output"/>
`t`t`t`t<Property Name="Platforms" Value="`$(Platforms)Win32;"/>
`t`t`t`t<Property Name="CopyInstalledFilter" Value="`$(CopyInstalledFilter);`$(CopyWin32Filter)"/>
`t`t`t`t<Property Name="CopyInstalledExceptions" Value="`$(CopyInstalledExceptions);`$(CopyWin32Exceptions)"/>
`t`t`t</Do>
`t`t`t<Do If="'`$(WithMac)' == true">
`t`t`t`t<Tag Files="#$($UnrealPrefix)Game Mac;#Build Tools Mac" Except="#$($UnrealPrefix)Game Mac Unstripped;#$($UnrealPrefix)Game Mac Unsigned" With="#Installed Win64"/>
`t`t`t`t<Tag Files="#$($UnrealPrefix)Game Mac Unstripped;#$($UnrealPrefix)Game Mac Unsigned" With="#Saved Output"/>
`t`t`t`t<Property Name="Platforms" Value="`$(Platforms)Mac;"/>
`t`t`t`t<Property Name="CopyInstalledFilter" Value="`$(CopyInstalledFilter);`$(CopyMacFilterWin64)"/>
`t`t`t`t<Property Name="CopyInstalledExceptions" Value="`$(CopyInstalledExceptions);`$(CopyMacExceptions)"/>
`t`t`t</Do> <!-- Redpoint -->
"@
                    }
                );
            },
            @{
                RelativePath = "Engine\Build\InstalledEngineFilters.xml";
                Patches      = @(
                    @{
                        CheckNotContains = @"
`t<Property Name="CopyMacFilterWin64" Value="`$(MacBuildFiles);`$(CopyMacFilter)">
`t`tEngine/Binaries/DotNET/IOS/openssl.exe
`t`tEngine/Binaries/Mac/DsymExporter*
`t`tEngine/Binaries/ThirdParty/ICU/icu4c-53_1/Mac/...
`t`tEngine/Binaries/ThirdParty/IOS/*
`t`tEngine/Build/BatchFiles/MakeAndInstallSSHKey.bat
`t`tEngine/Build/Rsync/...
`t`tEngine/Extras/ThirdPartyNotUE/DeltaCopy/...
`t`tEngine/Extras/ThirdPartyNotUE/ios-deploy/...
`t</Property> <!-- Redpoint -->
"@;
                        Find             = @"
`t<Property Name="CopyMacExceptions">
"@;
                        Replace          = @"
`t<Property Name="CopyMacFilterWin64" Value="`$(MacBuildFiles);`$(CopyMacFilter)">
`t`tEngine/Binaries/DotNET/IOS/openssl.exe
`t`tEngine/Binaries/Mac/DsymExporter*
`t`tEngine/Binaries/ThirdParty/ICU/icu4c-53_1/Mac/...
`t`tEngine/Binaries/ThirdParty/IOS/*
`t`tEngine/Build/BatchFiles/MakeAndInstallSSHKey.bat
`t`tEngine/Build/Rsync/...
`t`tEngine/Extras/ThirdPartyNotUE/DeltaCopy/...
`t`tEngine/Extras/ThirdPartyNotUE/ios-deploy/...
`t</Property> <!-- Redpoint -->
`t<Property Name="CopyMacExceptions">
"@;
                    }
                );
            }
        );
    },
    @{
        SectionName        = "patch-build-on-mac-cs";
        SectionDescription = "Patching MacPlatform.Automation.cs to build macOS from Windows...";
        Files              = @(
            @{
                RelativePath = "Engine\Source\Programs\AutomationTool\Mac\MacPlatform.Automation.cs";
                Patches      = @(
                    @{
                        CheckNotContains = "if (HostPlatform.Current.HostEditorPlatform != UnrealTargetPlatform.Mac) /* Redpoint */";
                        Find             = @"
`tpublic override void PreBuildAgenda($($UnrealPrefix)Build Build, $($UnrealPrefix)Build.BuildAgenda Agenda, ProjectParams Params)
`t{
`t`tbase.PreBuildAgenda(Build, Agenda, Params);
"@;
                        Replace          = @"
`tpublic override void PreBuildAgenda($($UnrealPrefix)Build Build, $($UnrealPrefix)Build.BuildAgenda Agenda, ProjectParams Params)
`t{
`t`tbase.PreBuildAgenda(Build, Agenda, Params);
`t`tif (HostPlatform.Current.HostEditorPlatform != UnrealTargetPlatform.Mac) /* Redpoint */
`t`t{
`t`t`t// We can not evaluate this when building for Mac remotely from Windows.
`t`t`treturn;
`t`t}
"@;
                    }
                );
            }
        );
    },
    @{
        SectionName        = "patch-ios-ver";
        SectionDescription = "Patching BaseEngine.ini to support iOS 13.0 and SIWA...";
        Files              = @(
            @{
                RelativePath = "Engine\Config\BaseEngine.ini";
                Patches      = @(
                    @{
                        CheckContains = "MinimumiOSVersion=IOS_12";
                        Find          = "MinimumiOSVersion=IOS_12";
                        Replace       = @"
MinimumiOSVersion=IOS_13
bEnableSignInWithAppleSupport=True
"@;
                    }
                );
            }
        )
    },
    @{
        SectionName        = "patch-google-oss";
        SectionDescription = "Patching OnlineSubsystemGoogle.uplugin so that it is available on Android...";
        Files              = @(
            @{
                RelativePath = "Engine\Plugins\Online\OnlineSubsystemGoogle\OnlineSubsystemGoogle.uplugin";
                Patches      = @(
                    @{
                        CheckNotContains = "Android";
                        Find             = "`"Win64`",`"Win32`"";
                        Replace          = "`"Win64`",`"Win32`",`"Android`"";
                    }
                );
            }
        );
    },
    @{
        SectionName        = "patch-gauntlet-mac";
        SectionDescription = "Patching Gauntlet to support running tests on macOS from Windows...";
        Files              = @(
            @{
                RelativePath = "Engine\Source\Programs\AutomationTool\Gauntlet\Framework\Devices\Gauntlet.DevicePool.cs";
                Patches      = @(
                    @{
                        CheckNotContains = "if (IsDesktop && !(Def.Platform == UnrealTargetPlatform.Mac && BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Win64))";
                        Find             = "if (IsDesktop)";
                        Replace          = "if (IsDesktop && !(Def.Platform == UnrealTargetPlatform.Mac && BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Win64))";
                    }
                );
            },
            @{
                RelativePath = "Engine\Source\Programs\AutomationTool\Gauntlet\Platform\Mac\Gauntlet.TargetDeviceMac.cs";
                Patches      = @(
                    @{
                        UnrealEngine5    = $false;
                        CheckNotContains = @"
using System.Threading;
using System.Text; // Redpoint
"@;
                        Find             = @"
using Tools.DotNETCommon;
"@;
                        Replace          = @"
using Tools.DotNETCommon;
using System.Threading;
using System.Text; // Redpoint
"@;
                    },
                    @{
                        UnrealEngine5    = $true;
                        CheckNotContains = @"
using System.Threading;
using System.Text; // Redpoint
"@;
                        Find             = @"
using EpicGames.Core;
"@;
                        Replace          = @"
using EpicGames.Core;
using System.Threading;
using System.Text; // Redpoint
"@;
                    },
                    @{
                        CheckNotContains                = @"
if (BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Mac) // Redpoint
"@;
                        ExpectNotApplicableToAllEngines = $true
                        Find                            = @"
`t`tpublic ITargetDevice CreateDevice(string InRef, string InParam)
`t`t{
`t`t`treturn new TargetDeviceMac(InRef, InParam);
`t`t}
"@;
                        Replace                         = @"
`t`tpublic ITargetDevice CreateDevice(string InRef, string InParam)
`t`t{
`t`t`tif (BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Mac) // Redpoint
`t`t`t{
`t`t`t`treturn new TargetDeviceMac(InRef, InParam);
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`treturn new TargetDeviceRemoteMac(InRef, InParam);
`t`t`t}
`t`t}
"@;
                    },
                    @{
                        CheckNotContains                = @"
if (BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Mac) // Redpoint
"@;
                        ExpectNotApplicableToAllEngines = $true
                        Find                            = @"
`t`tpublic ITargetDevice CreateDevice(string InRef, string InCachePath, string InParam = null)
`t`t{
`t`t`treturn new TargetDeviceMac(InRef, InCachePath);
`t`t}
"@;
                        Replace                         = @"
`t`tpublic ITargetDevice CreateDevice(string InRef, string InCachePath, string InParam)
`t`t{
`t`t`tif (BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Mac) // Redpoint
`t`t`t{
`t`t`t`treturn new TargetDeviceMac(InRef, InCachePath);
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`treturn new TargetDeviceRemoteMac(InRef, InParam);
`t`t`t}
`t`t}
"@;
                    },
                    @{
                        CheckNotContains = "#region Remote Mac";
                        Find             = "public class MacDeviceFactory : IDeviceFactory";
                        Replace          = @"
$(Get-Content -Path "$PSScriptRoot\$GauntletTargetDeviceMacPatchDataFileName" -Raw)

`tpublic class MacDeviceFactory : IDeviceFactory
"@;
                    }
                );
            }
        );
    },
    @{
        Section            = "patch-gauntlet-ios";
        SectionDescription = "Patching Gauntlet to support running tests on iOS from Windows...";
        Files              = @(
            @{
                RelativePath = "Engine\Source\Programs\AutomationTool\Gauntlet\Platform\IOS\Gauntlet.IOSBuildSource.cs";
                Patches      = @(
                    @{
                        UnrealEngine5    = $false;
                        CheckNotContains = @"
using Tools.DotNETCommon;
using System.Text;
using System.Diagnostics; // Redpoint
"@;
                        Find             = @"
using System.Linq;
"@;
                        Replace          = @"
using System.Linq;
using Tools.DotNETCommon;
using System.Text;
using System.Diagnostics; // Redpoint
"@;
                    },
                    @{
                        UnrealEngine5    = $true;
                        CheckNotContains = @"
using EpicGames.Core;
using System.Text;
using System.Diagnostics; // Redpoint
"@;
                        Find             = @"
using System.Linq;
"@;
                        Replace          = @"
using System.Linq;
using EpicGames.Core;
using System.Text;
using System.Diagnostics; // Redpoint
"@;
                    },
                    @{
                        CheckNotContains = @"
public IOSBuild(UnrealTargetConfiguration InConfig, string InPackageName, string InIPAPath, Dictionary<string, string> InFilesToInstall, BuildFlags InFlags, RemoteMac InRemoteMac) /* Redpoint */
"@;
                        Find             = @"
public IOSBuild(UnrealTargetConfiguration InConfig, string InPackageName, string InIPAPath, Dictionary<string, string> InFilesToInstall, BuildFlags InFlags)
"@;
                        Replace          = @"
internal RemoteMac RemoteMac;

`t`tpublic IOSBuild(UnrealTargetConfiguration InConfig, string InPackageName, string InIPAPath, Dictionary<string, string> InFilesToInstall, BuildFlags InFlags, RemoteMac InRemoteMac) /* Redpoint */
"@;
                    },
                    @{
                        CheckNotContains = "RemoteMac = InRemoteMac; /* Redpoint */";
                        Find             = @"
`t`t`tFlags = InFlags;
"@;
                        Replace          = @"
`t`t`tFlags = InFlags;
`t`t`tRemoteMac = InRemoteMac; /* Redpoint */
"@; ;
                    },
                    @{
                        CheckNotContains = "internal static IProcessResult ExecuteCommand(RemoteMac RemoteMac, String Command, String Arguments) /* Redpoint */";
                        Find             = @"
`t`tinternal static IProcessResult ExecuteCommand(String Command, String Arguments)
`t`t{
`t`t`tCommandUtils.ERunOptions RunOptions = CommandUtils.ERunOptions.AppMustExist;

`t`t`tif (Log.IsVeryVerbose)
`t`t`t{
`t`t`t`tRunOptions |= CommandUtils.ERunOptions.AllowSpew;
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tRunOptions |= CommandUtils.ERunOptions.NoLoggingOfRunCommand;
`t`t`t}

`t`t`tLog.Verbose("Executing '{0} {1}'", Command, Arguments);

`t`t`tIProcessResult Result = CommandUtils.Run(Command, Arguments, Options: RunOptions);

`t`t`treturn Result;
`t`t}
"@;
                        Replace          = @"
`t`tinternal class CapturedProcessResult : IProcessResult /* Redpoint */
`t`t{
`t`t`tpublic int ExitCode { get; set; }

`t`t`tpublic string Output { get; set; }

`t`t`tpublic Process ProcessObject { get; set; }

`t`t`tpublic bool HasExited => true;

`t`t`tpublic void DisposeProcess()
`t`t`t{
`t`t`t}

`t`t`tpublic string GetProcessName()
`t`t`t{
`t`t`t`tthrow new NotImplementedException();
`t`t`t}

`t`t`tpublic void OnProcessExited()
`t`t`t{
`t`t`t}

`t`t`tpublic void StdErr(object sender, DataReceivedEventArgs e)
`t`t`t{
`t`t`t}

`t`t`tpublic void StdOut(object sender, DataReceivedEventArgs e)
`t`t`t{
`t`t`t}

`t`t`tpublic void StopProcess(bool KillDescendants = true)
`t`t`t{
`t`t`t}

`t`t`tpublic void WaitForExit()
`t`t`t{
`t`t`t}
`t`t}

`t`tinternal static IProcessResult ExecuteCommand(RemoteMac RemoteMac, String Command, String Arguments) /* Redpoint */
`t`t{
`t`t`tCommandUtils.ERunOptions RunOptions = CommandUtils.ERunOptions.AppMustExist;

`t`t`tif (Log.IsVeryVerbose)
`t`t`t{
`t`t`t`tRunOptions |= CommandUtils.ERunOptions.AllowSpew;
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tRunOptions |= CommandUtils.ERunOptions.NoLoggingOfRunCommand;
`t`t`t}

`t`t`tif (BuildHostPlatform.Current.Platform != UnrealTargetPlatform.Mac) /* Redpoint */
`t`t`t{
`t`t`t`tLog.Info("[Remote] Executing '{0} {1}'", Command, Arguments);
`t`t`t`tStringBuilder sb;
`t`t`t`tvar exitCode = RemoteMac.ExecuteAndCaptureOutput(Command + " " + Arguments, out sb);
`t`t`t`treturn new CapturedProcessResult()
`t`t`t`t{
`t`t`t`t`tExitCode = exitCode,
`t`t`t`t`tOutput = sb.ToString(),
`t`t`t`t`tProcessObject = null,
`t`t`t`t};
`t`t`t}

`t`t`tLog.Verbose("Executing '{0} {1}'", Command, Arguments);

`t`t`tIProcessResult Result = CommandUtils.Run(Command, Arguments, Options: RunOptions);

`t`t`treturn Result;
`t`t}
"@;
                    },
                    @{
                        CheckNotContains = "internal static bool ExecuteIPAZipCommand(RemoteMac RemoteMac, String Arguments, out String Output, String ShouldExist = `"`") /* Redpoint */";
                        Find             = "internal static bool ExecuteIPAZipCommand(String Arguments, out String Output, String ShouldExist = `"`")";
                        Replace          = "internal static bool ExecuteIPAZipCommand(RemoteMac RemoteMac, String Arguments, out String Output, String ShouldExist = `"`") /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "IProcessResult Result = ExecuteCommand(RemoteMac, `"unzip`", Arguments); /* Redpoint */";
                        Find             = "IProcessResult Result = ExecuteCommand(`"unzip`", Arguments);";
                        Replace          = "IProcessResult Result = ExecuteCommand(RemoteMac, `"unzip`", Arguments); /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "internal static bool ExecuteIPADittoCommand(RemoteMac RemoteMac, String Arguments, out String Output, String ShouldExist = `"`") /* Redpoint */";
                        Find             = "internal static bool ExecuteIPADittoCommand(String Arguments, out String Output, String ShouldExist = `"`")";
                        Replace          = "internal static bool ExecuteIPADittoCommand(RemoteMac RemoteMac, String Arguments, out String Output, String ShouldExist = `"`") /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "IProcessResult Result = ExecuteCommand(RemoteMac, `"ditto`", Arguments); /* Redpoint */";
                        Find             = "IProcessResult Result = ExecuteCommand(`"ditto`", Arguments);";
                        Replace          = "IProcessResult Result = ExecuteCommand(RemoteMac, `"ditto`", Arguments); /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "private static string GetBundleIdentifier(RemoteMac RemoteMac, string SourceIPA) /* Redpoint */";
                        Find             = @"
`t`tprivate static string GetBundleIdentifier(string SourceIPA)
"@;
                        Replace          = @"
`t`tinternal static string ConvertPotentialRemotePath(RemoteMac RemoteMac, string Path) /* Redpoint */
`t`t{
`t`t`tif (RemoteMac == null)
`t`t`t{
`t`t`t`treturn Path;
`t`t`t}

`t`t`treturn RemoteMac.GetRemotePath(Path);
`t`t}


`t`tprivate static string GetBundleIdentifier(RemoteMac RemoteMac, string SourceIPA) /* Redpoint */
"@;
                    },
                    @{
                        CheckNotContains = "if (!ExecuteIPAZipCommand(RemoteMac, String.Format(`"-Z1 {0}`", ConvertPotentialRemotePath(RemoteMac, SourceIPA)), out Output)) /* Redpoint */";
                        Find             = "if (!ExecuteIPAZipCommand(String.Format(`"-Z1 {0}`", SourceIPA), out Output))";
                        Replace          = "if (!ExecuteIPAZipCommand(RemoteMac, String.Format(`"-Z1 {0}`", ConvertPotentialRemotePath(RemoteMac, SourceIPA)), out Output)) /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "if (!ExecuteIPAZipCommand(RemoteMac, String.Format(`"-p '{0}' '{1}'`", ConvertPotentialRemotePath(RemoteMac, SourceIPA), PList), out Output)) /* Redpoint */";
                        Find             = "if (!ExecuteIPAZipCommand(String.Format(`"-p '{0}' '{1}'`", SourceIPA, PList), out Output))";
                        Replace          = "if (!ExecuteIPAZipCommand(RemoteMac, String.Format(`"-p '{0}' '{1}'`", ConvertPotentialRemotePath(RemoteMac, SourceIPA), PList), out Output)) /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "public static IEnumerable<IOSBuild> CreateFromPath(RemoteMac RemoteMac, string InProjectName, string InPath) /* Redpoint */";
                        Find             = "public static IEnumerable<IOSBuild> CreateFromPath(string InProjectName, string InPath)";
                        Replace          = "public static IEnumerable<IOSBuild> CreateFromPath(RemoteMac RemoteMac, string InProjectName, string InPath) /* Redpoint */";
                    },
                    @{
                        CheckNotContains = @"
`t`t`t`tif (RemoteMac != null) /* Redpoint */
`t`t`t`t{
`t`t`t`t`tLog.Info("[Remote] Uploading IPA to Mac...");
`t`t`t`t`tRemoteMac.UploadFile(new FileReference(Fi.FullName));
`t`t`t`t}
`t`t`t`tstring PackageName = GetBundleIdentifier(RemoteMac, SourceIPAPath);
"@;
                        Find             = @"
`t`t`t`tstring PackageName = GetBundleIdentifier(SourceIPAPath);
"@;
                        Replace          = @"
`t`t`t`tif (RemoteMac != null) /* Redpoint */
`t`t`t`t{
`t`t`t`t`tLog.Info("[Remote] Uploading IPA to Mac...");
`t`t`t`t`tRemoteMac.UploadFile(new FileReference(Fi.FullName));
`t`t`t`t}
`t`t`t`tstring PackageName = GetBundleIdentifier(RemoteMac, SourceIPAPath);
"@;
                    },
                    @{
                        CheckNotContains = "IOSBuild NewBuild = new IOSBuild(UnrealConfig, PackageName, SourceIPAPath, FilesToInstall, Flags, RemoteMac); /* Redpoint */";
                        Find             = "IOSBuild NewBuild = new IOSBuild(UnrealConfig, PackageName, SourceIPAPath, FilesToInstall, Flags);";
                        Replace          = "IOSBuild NewBuild = new IOSBuild(UnrealConfig, PackageName, SourceIPAPath, FilesToInstall, Flags, RemoteMac); /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "public IOSBuildSource() /* Redpoint */";
                        Find             = @"
`t`tpublic IOSBuildSource()
"@;
                        Replace          = @"
`t`tpublic static RemoteMac RemoteMac { get; set; }
`t`tpublic IOSBuildSource() /* Redpoint */
"@;
                    },
                    @{
                        CheckNotContains = @"
`t`tpublic List<IBuild> GetBuildsAtPath(string InProjectName, string InPath, int MaxRecursion = 3) /* Redpoint */
"@;
                        Find             = @"
`t`tpublic List<IBuild> GetBuildsAtPath(string InProjectName, string InPath, int MaxRecursion = 3)
`t`t{
`t`t`t// We only want iOS builds on Mac host
`t`t`tif (BuildHostPlatform.Current.Platform != UnrealTargetPlatform.Mac)
`t`t`t{
`t`t`t`treturn new List<IBuild>();
`t`t`t}
"@;
                        Replace          = @"
`t`tpublic List<IBuild> GetBuildsAtPath(string InProjectName, string InPath, int MaxRecursion = 3) /* Redpoint */
`t`t{
`t`t`tif (BuildHostPlatform.Current.Platform != UnrealTargetPlatform.Mac && RemoteMac == null)
`t`t`t{
`t`t`t`tvar CurrentDir = new DirectoryReference(InPath);
`t`t`t`tFileReference ProjectFile = null;
`t`t`t`twhile (CurrentDir != null)
`t`t`t`t{
`t`t`t`t`tif (File.Exists(Path.Combine(CurrentDir.FullName, InProjectName + ".uproject")))
`t`t`t`t`t{
`t`t`t`t`t`tProjectFile = new FileReference(Path.Combine(CurrentDir.FullName, InProjectName + ".uproject"));
`t`t`t`t`t`tbreak;
`t`t`t`t`t}
`t`t`t`t`tCurrentDir = CurrentDir.ParentDirectory;
`t`t`t`t}

`t`t`t`tif (ProjectFile == null)
`t`t`t`t{
`t`t`t`t`tLog.Warning("No IOS builds will be returned on non-Mac platform, as the appropriate .uproject file could not be located.");
`t`t`t`t`treturn new List<IBuild>();
`t`t`t`t}

`t`t`t`tRemoteMac = new RemoteMac(ProjectFile);
`t`t`t}
"@;
                    },
                    @{
                        CheckNotContains = "IEnumerable<IOSBuild> FoundBuilds = IOSBuild.CreateFromPath(RemoteMac, InProjectName, Di.FullName);";
                        Find             = "IEnumerable<IOSBuild> FoundBuilds = IOSBuild.CreateFromPath(InProjectName, Di.FullName);";
                        Replace          = "IEnumerable<IOSBuild> FoundBuilds = IOSBuild.CreateFromPath(RemoteMac, InProjectName, Di.FullName);";
                    }
                );
            },
            @{
                RelativePath = "Engine\Source\Programs\AutomationTool\Gauntlet\Platform\IOS\Gauntlet.TargetDeviceIOS.cs";
                Patches      = @(
                    @{
                        CheckNotContains = "class IOSWindowsAppInstance : IAppInstance /* Redpoint */";
                        Find             = @"
`tclass IOSAppInstall : IAppInstall
"@;
                        Replace          = @"
`tclass IOSWindowsAppInstance : IAppInstance /* Redpoint */
`t{
`t`tprotected IOSAppInstall Install;
`t`tpublic IOSWindowsAppInstance(IOSAppInstall InInstall, IProcessResult InAppProcess, IProcessResult InSysLogProcess, string InCommandLine)
`t`t{
`t`t`tInstall = InInstall;
`t`t`tthis.CommandLine = InCommandLine;
`t`t`tthis.AppProcessResult = InAppProcess;
`t`t`tthis.SysLogProcessResult = InSysLogProcess;
`t`t}

`t`tpublic string ArtifactPath
`t`t{
`t`t`tget
`t`t`t{
`t`t`t`tif (bHaveSavedArtifacts == false)
`t`t`t`t{
`t`t`t`t`tif (HasExited)
`t`t`t`t`t{
`t`t`t`t`t`tSaveArtifacts();
`t`t`t`t`t`tbHaveSavedArtifacts = true;
`t`t`t`t`t}
`t`t`t`t}

`t`t`t`treturn Install.IOSDevice.LocalCachePath + "/" + Install.IOSDevice.DeviceArtifactPath;
`t`t`t}
`t`t}

`t`tpublic ITargetDevice Device
`t`t{
`t`t`tget
`t`t`t{
`t`t`t`treturn Install.Device;
`t`t`t}
`t`t}

`t`tprotected void SaveArtifacts()
`t`t{
`t`t`tif (BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Mac)
`t`t`t{
`t`t`t`tTargetDeviceIOS Device = Install.IOSDevice;

`t`t`t`t// copy remote artifacts to local		
`t`t`t`tstring CommandLine = String.Format("--bundle_id {0} --download={1} --to {2}", Install.PackageName, Device.DeviceArtifactPath, Device.LocalCachePath);

`t`t`t`tIProcessResult DownloadCmd = Device.ExecuteIOSDeployCommand(CommandLine, 120);

`t`t`t`tif (DownloadCmd.ExitCode != 0)
`t`t`t`t{
`t`t`t`t`tLog.Warning("Failed to retrieve artifacts. {0}", DownloadCmd.Output);
`t`t`t`t}
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tLog.Warning("Unable to save artifacts for iOS when running from Windows");
`t`t`t}
`t`t}

`t`tpublic IProcessResult AppProcessResult { get; private set; }
`t`tpublic IProcessResult SysLogProcessResult { get; private set; }

`t`tpublic bool HasExited { get { return AppProcessResult.HasExited; } }

`t`tpublic bool WasKilled { get; protected set; }

`t`tpublic int ExitCode { get { return AppProcessResult.ExitCode; } }

`t`tpublic string CommandLine { get; private set; }

`t`tprivate string CachedStdOut;

`t`tprivate string GetStdOutFromSysLogProcess()
`t`t{
`t`t`tstring RawStdOut = SysLogProcessResult.Output;
`t`t`treturn string.Join("\n", RawStdOut.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries)
`t`t`t`t.Select(line =>
`t`t`t`t{
`t`t`t`t`tvar idx = line.IndexOf("[$($UnrealPrefix)] ");
`t`t`t`t`tif (idx == -1)
`t`t`t`t`t{
`t`t`t`t`t`treturn string.Empty;
`t`t`t`t`t}
`t`t`t`t`treturn line.Substring(idx + "[$($UnrealPrefix)] ".Length);
`t`t`t`t})
`t`t`t`t.ToArray());
`t`t}

`t`tpublic string StdOut
`t`t{
`t`t`tget
`t`t`t{
`t`t`t`tif (HasExited)
`t`t`t`t{
`t`t`t`t`treturn CachedStdOut;
`t`t`t`t}
`t`t`t`tCachedStdOut = GetStdOutFromSysLogProcess();
`t`t`t`treturn CachedStdOut;
`t`t`t}
`t`t}

`t`tpublic int WaitForExit()
`t`t{
`t`t`tif (!HasExited)
`t`t`t{
`t`t`t`tAppProcessResult.WaitForExit();
`t`t`t`tCachedStdOut = GetStdOutFromSysLogProcess();
`t`t`t`tSysLogProcessResult.StopProcess();
`t`t`t}

`t`t`treturn ExitCode;
`t`t}

`t`tpublic void Kill()
`t`t{
`t`t`tif (!HasExited)
`t`t`t{
`t`t`t`tWasKilled = true;
`t`t`t`tCachedStdOut = GetStdOutFromSysLogProcess();
`t`t`t`tSysLogProcessResult.StopProcess();
`t`t`t`tAppProcessResult.StopProcess();
`t`t`t}
`t`t}


`t`tinternal bool bHaveSavedArtifacts;
`t}

`tclass IOSAppInstall : IAppInstall
"@;
                    },
                    @{
                        CheckNotContains = "if (BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Mac) /* Redpoint 1 */";
                        Find             = @"
`t`t`tstring CommandLine = IOSApp.CommandLine.Replace("\"", "\\\"");
"@;
                        Replace          = @"
`t`t`tif (BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Mac) /* Redpoint 1 */
`t`t`t{
`t`t`tstring CommandLine = IOSApp.CommandLine.Replace("\"", "\\\"");
"@;
                    },
                    @{
                        CheckNotContains = "else /* Redpoint 2 */";
                        Find             = @"
`t`t`treturn new IOSAppInstance(IOSApp, Result, IOSApp.CommandLine);
"@;
                        Replace          = @"
`t`t`treturn new IOSAppInstance(IOSApp, Result, IOSApp.CommandLine);
`t`t`t}
`t`t`telse /* Redpoint 2 */
`t`t`t{
`t`t`t`tIProcessResult SysLogResult = ExecuteIOSToolsForWindowsCommand("idevicesyslog", "-q --match \"[$($UnrealPrefix)]\" --no-colors -K", 0);

`t`t`t`t// Wait for syslog to start.
`t`t`t`tThread.Sleep(2500);

`t`t`t`tif (SysLogResult.HasExited)
`t`t`t`t{
`t`t`t`t`tLog.Warning("idevicesyslog exited early: " + SysLogResult.Output);
`t`t`t`t`tthrow new DeviceException("Failed to launch on {0}. {1}", Name, SysLogResult.Output);
`t`t`t`t}

`t`t`t`tIProcessResult AppResult = ExecuteIOSToolsForWindowsCommand("idevicedebug", "run " + IOSApp.PackageName + " " + IOSApp.CommandLine, 0);

`t`t`t`t// Wait for app to start.
`t`t`t`tThread.Sleep(5000);

`t`t`t`tif (AppResult.HasExited)
`t`t`t`t{
`t`t`t`t`tLog.Warning("idevicedebug exited early: " + AppResult.Output);
`t`t`t`t`tthrow new DeviceException("Failed to launch on {0}. {1}", Name, AppResult.Output);
`t`t`t`t}

`t`t`t`treturn new IOSWindowsAppInstance(IOSApp, AppResult, SysLogResult, IOSApp.CommandLine);
`t`t`t}
"@;
                    },
                    @{
                        CheckNotContains = "void ResignApplication(UnrealAppConfig AppConfig) /* Redpoint 3 */";
                        Find             = @"
`t`tvoid ResignApplication(UnrealAppConfig AppConfig)
`t`t{
"@;
                        Replace          = @"
`t`tvoid ResignApplication(UnrealAppConfig AppConfig) /* Redpoint 3 */
`t`t{
`t`t`tif (BuildHostPlatform.Current.Platform != UnrealTargetPlatform.Mac)
`t`t`t{
`t`t`t`treturn;
`t`t`t}
"@;
                    },
                    @{
                        CheckNotContains = "var Result = IOSBuild.ExecuteCommand(null, `"codesign`", SignArgs); /* Redpoint */";
                        Find             = "var Result = IOSBuild.ExecuteCommand(`"codesign`", SignArgs);";
                        Replace          = "var Result = IOSBuild.ExecuteCommand(null, `"codesign`", SignArgs); /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "if (BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Mac) /* Redpoint 4 */";
                        Find             = @"
`t`t`tlock(IPALock)
`t`t`t{
`t`t`t`tLog.Info("Installing using IPA {0}", Build.SourceIPAPath);
"@;
                        Replace          = @"
`t`t`tif (BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Mac) /* Redpoint 4 */
`t`t`t{
`t`t`tlock(IPALock)
`t`t`t{
`t`t`t`tLog.Info("Installing using IPA {0}", Build.SourceIPAPath);
"@;
                    },
                    @{
                        CheckNotContains = "else /* Redpoint 5 */";
                        Find             = @"
`t`t`tIOSAppInstall IOSApp = new IOSAppInstall(AppConfig.Name, this, Build.PackageName, AppConfig.CommandLine);`t
`t`t`treturn IOSApp;
"@;
                        Replace          = @"
`t`t`t}
`t`t`telse /* Redpoint 5 */
`t`t`t{
`t`t`t`tlock (Globals.MainLock)
`t`t`t`t{
`t`t`t`t`t// Check if the app is already installed.
`t`t`t`t`tLog.Info("Checking if app is already installed using bundle ID " + Build.PackageName);
`t`t`t`t`tIProcessResult ListApps = ExecuteIOSToolsForWindowsCommand("ideviceinstaller", "-l", 20 * 60);
`t`t`t`t`tListApps.WaitForExit();
`t`t`t`t`tif (ListApps.Output.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Any(x => x.StartsWith(Build.PackageName + ",")))
`t`t`t`t`t{
`t`t`t`t`t`t// We can do an upgrade, which will prevent any previously granted permissions from being reset.
`t`t`t`t`t`tLog.Info("Upgrading IPA to iOS device using path " + Build.SourceIPAPath);
`t`t`t`t`t`tExecuteIOSToolsForWindowsCommand("ideviceinstaller", "--upgrade \"" + Build.SourceIPAPath + "\"", 20 * 60);
`t`t`t`t`t}
`t`t`t`t`telse
`t`t`t`t`t{
`t`t`t`t`t`t// Uninstall the bundle.
`t`t`t`t`t`tLog.Info("Uninstalling app from iOS device using bundle ID " + Build.PackageName);
`t`t`t`t`t`tExecuteIOSToolsForWindowsCommand("ideviceinstaller", "--uninstall " + Build.PackageName, 20 * 60);

`t`t`t`t`t`t// Now install the app from the IPA.
`t`t`t`t`t`tLog.Info("Installing IPA to iOS device using path " + Build.SourceIPAPath);
`t`t`t`t`t`tExecuteIOSToolsForWindowsCommand("ideviceinstaller", "--install \"" + Build.SourceIPAPath + "\"", 20 * 60);
`t`t`t`t`t}
`t`t`t`t}
`t`t`t}

`t`t`tIOSAppInstall IOSApp = new IOSAppInstall(AppConfig.Name, this, Build.PackageName, AppConfig.CommandLine);`t
`t`t`treturn IOSApp;
"@;
                    },
                    @{
                        CheckNotContains = "public bool Reboot() /* Redpoint 6 */";
                        Find             = @"
`t`tpublic bool Reboot() 
`t`t{
"@;
                        Replace          = @"
`t`tpublic bool Reboot() /* Redpoint 6 */
`t`t{
`t`t`tif (BuildHostPlatform.Current.Platform != UnrealTargetPlatform.Mac)
`t`t`t{
`t`t`t`treturn false;
`t`t`t}
"@;
                    },
                    @{
                        CheckNotContains = @"
var Result = IOSBuild.ExecuteCommand(null, Cmd, string.Format("restart -u {0}", DeviceName)); /* Redpoint 7 */
"@;
                        Find             = @"
var Result = IOSBuild.ExecuteCommand(Cmd, string.Format("restart -u {0}", DeviceName));
"@;
                        Replace          = @"
var Result = IOSBuild.ExecuteCommand(null, Cmd, string.Format("restart -u {0}", DeviceName)); /* Redpoint 7 */
"@;
                    },
                    @{
                        CheckNotContains = @"
Result = IOSBuild.ExecuteCommand(null, Cmd, string.Format("diagnostics WiFi -u {0}", DeviceName)); /* Redpoint 8 */
"@;
                        Find             = @"
Result = IOSBuild.ExecuteCommand(Cmd, string.Format("diagnostics WiFi -u {0}", DeviceName));
"@;
                        Replace          = @"
Result = IOSBuild.ExecuteCommand(null, Cmd, string.Format("diagnostics WiFi -u {0}", DeviceName)); /* Redpoint 8 */
"@;
                    },
                    @{
                        CheckNotContains = "List<string> GetConnectedDeviceUUID() /* Redpoint 9 */";
                        Find             = @"
`t`tList<string> GetConnectedDeviceUUID()
`t`t{
`t`t`tvar Result = ExecuteIOSDeployCommand("--detect", 60, true, false);

`t`t`tif (Result.ExitCode != 0)
`t`t`t{
`t`t`t`treturn new List<string>();
`t`t`t}

`t`t`tMatchCollection DeviceMatches = Regex.Matches(Result.Output, @"(.?)Found\ ([a-z0-9]{40}|[A-Z0-9]{8}-[A-Z0-9]{16})");

`t`t`treturn DeviceMatches.Cast<Match>().Select<Match, string>(
`t`t`t`tM => M.Groups[2].ToString()
`t`t`t).ToList();
`t`t}
"@;
                        Replace          = @"
`t`tList<string> GetConnectedDeviceUUID() /* Redpoint 9 */
`t`t{
`t`t`tif (BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Mac)
`t`t`t{
`t`t`t`tvar Result = ExecuteIOSDeployCommand("--detect", 60, true, false);

`t`t`t`tif (Result.ExitCode != 0)
`t`t`t`t{
`t`t`t`t`treturn new List<string>();
`t`t`t`t}

`t`t`t`tMatchCollection DeviceMatches = Regex.Matches(Result.Output, @"(.?)Found\ ([a-z0-9]{40}|[A-Z0-9]{8}-[A-Z0-9]{16})");

`t`t`t`treturn DeviceMatches.Cast<Match>().Select<Match, string>(
`t`t`t`t`tM => M.Groups[2].ToString()
`t`t`t`t).ToList();
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tvar Result = ExecuteIOSToolsForWindowsCommand("ideviceinfo", "-k UniqueDeviceID", 60, true, false);

`t`t`t`tif (Result.ExitCode != 0)
`t`t`t`t{
`t`t`t`t`treturn new List<string>();
`t`t`t`t}

`t`t`t`treturn Result.Output.Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
`t`t`t}
`t`t}
"@;
                    },
                    @{
                        CheckNotContains = @"
IOSBuild.ExecuteCommand(null, "killall", "ios-deploy"); /* Redpoint 10 */
"@;
                        Find             = @"
`t`t`tIOSBuild.ExecuteCommand("killall", "ios-deploy");
`t`t`tThread.Sleep(2500);
`t`t`tIOSBuild.ExecuteCommand("killall", "lldb");			
`t`t`tThread.Sleep(2500);
"@;
                        Replace          = @"
`t`t`tif (BuildHostPlatform.Current.Platform == UnrealTargetPlatform.Mac)
`t`t`t{
`t`t`t`tIOSBuild.ExecuteCommand(null, "killall", "ios-deploy"); /* Redpoint 10 */
`t`t`t`tThread.Sleep(2500);
`t`t`t`tIOSBuild.ExecuteCommand(null, "killall", "lldb");
`t`t`t`tThread.Sleep(2500);
`t`t`t}
"@;
                    },
                    @{
                        CheckNotContains = @"
if (!IOSBuild.ExecuteIPADittoCommand(Build.RemoteMac, String.Format("-x -k {0} {1}", IOSBuild.ConvertPotentialRemotePath(Build.RemoteMac, Build.SourceIPAPath), IOSBuild.ConvertPotentialRemotePath(Build.RemoteMac, GauntletAppCache)), out Output, PayloadDir)) /* Redpoint 11 */
"@;
                        Find             = @"
if (!IOSBuild.ExecuteIPADittoCommand(String.Format("-x -k {0} {1}", Build.SourceIPAPath, GauntletAppCache), out Output, PayloadDir))
"@;
                        Replace          = @"
if (!IOSBuild.ExecuteIPADittoCommand(Build.RemoteMac, String.Format("-x -k {0} {1}", IOSBuild.ConvertPotentialRemotePath(Build.RemoteMac, Build.SourceIPAPath), IOSBuild.ConvertPotentialRemotePath(Build.RemoteMac, GauntletAppCache)), out Output, PayloadDir)) /* Redpoint 11 */
"@;
                    },
                    @{
                        CheckNotContains = @"
if (!IOSBuild.ExecuteIPAZipCommand(Build.RemoteMac, String.Format("{0} -d {1}", SymbolsZipFile, SymbolsDir), out Output, SymbolsDir)) /* Redpoint 12 */
"@;
                        Find             = @"
if (!IOSBuild.ExecuteIPAZipCommand(String.Format("{0} -d {1}", SymbolsZipFile, SymbolsDir), out Output, SymbolsDir))
"@;
                        Replace          = @"
if (!IOSBuild.ExecuteIPAZipCommand(Build.RemoteMac, String.Format("{0} -d {1}", SymbolsZipFile, SymbolsDir), out Output, SymbolsDir)) /* Redpoint 12 */
"@;
                    },
                    @{
                        CheckNotContains = @"
if (BuildHostPlatform.Current.Platform != UnrealTargetPlatform.Mac) /* Redpoint 13 */
"@;
                        Find             = @"
`t`tpublic IProcessResult ExecuteIOSDeployCommand(String CommandLine, int WaitTime = 60, bool WarnOnTimeout = true, bool UseDeviceID = true)
`t`t{
"@;
                        Replace          = @"
`t`tpublic IProcessResult ExecuteIOSDeployCommand(String CommandLine, int WaitTime = 60, bool WarnOnTimeout = true, bool UseDeviceID = true)
`t`t{
`t`t`tif (BuildHostPlatform.Current.Platform != UnrealTargetPlatform.Mac) /* Redpoint 13 */
`t`t`t{
`t`t`t`tthrow new InvalidOperationException("ExecuteIOSDeployCommand called from non-Mac machine with command line: " + CommandLine);
`t`t`t}
"@;
                    },
                    @{
                        CheckNotContains = @"
public IProcessResult ExecuteIOSToolsForWindowsCommand(String Executable, String CommandLine, int WaitTime = 60, bool WarnOnTimeout = true, bool UseDeviceID = true) /* Redpoint 14 */
"@;
                        Find             = @"
`t}


`t/// <summary>
`t/// Helper class to parses LLDB crash threads and generate Unreal compatible log callstack
`t/// </summary>
`tstatic class LLDBCrashParser
"@;
                        Replace          = @"
`t`tpublic IProcessResult ExecuteIOSToolsForWindowsCommand(String Executable, String CommandLine, int WaitTime = 60, bool WarnOnTimeout = true, bool UseDeviceID = true) /* Redpoint 14 */
`t`t{
`t`t`tif (BuildHostPlatform.Current.Platform != UnrealTargetPlatform.Win64)
`t`t`t{
`t`t`t`tthrow new InvalidOperationException("ExecuteIOSToolsForWindowsCommand called from non-Windows machine with command line: " + CommandLine);
`t`t`t}

`t`t`tif (UseDeviceID && !IsDefaultDevice)
`t`t`t{
`t`t`t`tCommandLine = String.Format("--udid {0} {1}", DeviceName, CommandLine);
`t`t`t}

`t`t`tString IOSToolPath = Path.Combine(Environment.GetEnvironmentVariable("PROGRAMDATA"), "iOS-Tools-For-Windows", Executable + ".exe");

`t`t`tif (!File.Exists(IOSToolPath))
`t`t`t{
`t`t`t`tthrow new AutomationException("Unable to run iOS Tool at {0}. Check that iOS Tools for Windows is installed in the correct location.", IOSToolPath);
`t`t`t}

`t`t`tCommandUtils.ERunOptions RunOptions = CommandUtils.ERunOptions.NoWaitForExit;

`t`t`tif (Log.IsVeryVerbose)
`t`t`t{
`t`t`t`tRunOptions |= CommandUtils.ERunOptions.AllowSpew;
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tRunOptions |= CommandUtils.ERunOptions.NoLoggingOfRunCommand;
`t`t`t}

`t`t`tLog.Verbose("{0} executing '{1}'", Executable, CommandLine);

`t`t`tIProcessResult Result = CommandUtils.Run(IOSToolPath, CommandLine, Options: RunOptions);

`t`t`tif (WaitTime > 0)
`t`t`t{
`t`t`t`tDateTime StartTime = DateTime.Now;

`t`t`t`tResult.ProcessObject.WaitForExit(WaitTime * 1000);

`t`t`t`tif (Result.HasExited == false)
`t`t`t`t{
`t`t`t`t`tif ((DateTime.Now - StartTime).TotalSeconds >= WaitTime)
`t`t`t`t`t{
`t`t`t`t`t`tstring Message = String.Format("iOS Tools timeout after {0} secs: {1}, killing process", WaitTime, CommandLine);

`t`t`t`t`t`tif (WarnOnTimeout)
`t`t`t`t`t`t{
`t`t`t`t`t`t`tLog.Warning(Message);
`t`t`t`t`t`t}
`t`t`t`t`t`telse
`t`t`t`t`t`t{
`t`t`t`t`t`t`tLog.Info(Message);
`t`t`t`t`t`t}

`t`t`t`t`t`tResult.ProcessObject.Kill();
`t`t`t`t`t`t// wait up to 15 seconds for process exit
`t`t`t`t`t`tResult.ProcessObject.WaitForExit(15000);
`t`t`t`t`t}
`t`t`t`t}
`t`t`t}

`t`t`treturn Result;

`t`t}
`t}


`t/// <summary>
`t/// Helper class to parses LLDB crash threads and generate Unreal compatible log callstack
`t/// </summary>
`tstatic class LLDBCrashParser
"@;
                    }
                );
            }
        )
    },
    @{
        SectionName        = "patch-ubt";
        SectionDescription = "Patching UnrealBuildTool to support iOS Gauntlet from Windows...";
        Files              = @(
            @{
                RelativePath = "Engine\Source\Programs\UnrealBuildTool\Configuration\TargetDescriptor.cs";
                Patches      = @(
                    @{
                        CheckNotContains = "public class TargetDescriptor /* Redpoint */";
                        Find             = "class TargetDescriptor";
                        Replace          = "public class TargetDescriptor /* Redpoint */";
                    }
                )
            },
            @{
                RelativePath = "Engine\Source\Programs\UnrealBuildTool\System\HotReload.cs";
                Patches      = @(
                    @{
                        CheckNotContains = "public enum HotReloadMode /* Redpoint */";
                        Find             = "enum HotReloadMode";
                        Replace          = "public enum HotReloadMode /* Redpoint */";
                    }
                )
            },
            @{
                RelativePath = "Engine\Source\Programs\UnrealBuildTool\ToolChain\RemoteMac.cs";
                Patches      = @(
                    @{
                        CheckNotContains = "public class RemoteMac /* Redpoint */";
                        Find             = "class RemoteMac";
                        Replace          = "public class RemoteMac /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "public string RemoteBaseDir; /* Redpoint */";
                        Find             = "private string RemoteBaseDir;";
                        Replace          = "public string RemoteBaseDir; /* Redpoint */";
                    },
                    @{
                        CheckNotContains                = "public RemoteMac(FileReference ProjectFile, string InOverrideServerName = null, string InOverrideUserName = null) /* Redpoint */";
                        IsUnrealEngine5                 = $false;
                        ExpectNotApplicableToAllEngines = $true;
                        Find                            = @"
`t`tpublic RemoteMac(FileReference ProjectFile)
`t`t{
`t`t`tthis.RsyncExe = FileReference.Combine(UnrealBuildTool.EngineDirectory, "Extras", "ThirdPartyNotUE", "DeltaCopy", "Binaries", "Rsync.exe");
`t`t`tthis.SshExe = FileReference.Combine(UnrealBuildTool.EngineDirectory, "Extras", "ThirdPartyNotUE", "DeltaCopy", "Binaries", "Ssh.exe");
"@;
                        Replace                         = @"
`t`tpublic RemoteMac(FileReference ProjectFile, string InOverrideServerName = null, string InOverrideUserName = null) /* Redpoint */
`t`t{
`t`t`tstring RsyncExePath = Environment.GetEnvironmentVariable("UNREAL_ENGINE_RSYNC_PATH");
`t`t`tif (!string.IsNullOrWhiteSpace(RsyncExePath))
`t`t`t{
`t`t`t`tthis.RsyncExe = new FileReference(RsyncExePath);
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tthis.RsyncExe = FileReference.Combine(UnrealBuildTool.EngineDirectory, "Extras", "ThirdPartyNotUE", "DeltaCopy", "Binaries", "Rsync.exe");
`t`t`t}
`t`t`tstring SshExePath = Environment.GetEnvironmentVariable("UNREAL_ENGINE_SSH_PATH");
`t`t`tif (!string.IsNullOrWhiteSpace(SshExePath))
`t`t`t{
`t`t`t`tthis.SshExe = new FileReference(SshExePath);
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tthis.SshExe = FileReference.Combine(UnrealBuildTool.EngineDirectory, "Extras", "ThirdPartyNotUE", "DeltaCopy", "Binaries", "Ssh.exe");
`t`t`t}
"@;
                    },
                    @{
                        CheckNotContains                = "public RemoteMac(FileReference ProjectFile, string InOverrideServerName = null, string InOverrideUserName = null) /* Redpoint */";
                        IsUnrealEngine5                 = $false;
                        ExpectNotApplicableToAllEngines = $true;
                        Find                            = @"
`t`tpublic RemoteMac(FileReference ProjectFile)
`t`t{
`t`t`tthis.RsyncExe = FileReference.Combine(UnrealBuildTool.EngineDirectory, "Extras", "ThirdPartyNotUE", "cwrsync", "bin", "rsync.exe");
`t`t`tthis.SshExe = FileReference.Combine(UnrealBuildTool.EngineDirectory, "Extras", "ThirdPartyNotUE", "cwrsync", "bin", "ssh.exe");
"@;
                        Replace                         = @"
`t`tpublic RemoteMac(FileReference ProjectFile, string InOverrideServerName = null, string InOverrideUserName = null) /* Redpoint */
`t`t{
`t`t`tstring RsyncExePath = Environment.GetEnvironmentVariable("UNREAL_ENGINE_RSYNC_PATH");
`t`t`tif (!string.IsNullOrWhiteSpace(RsyncExePath))
`t`t`t{
`t`t`t`tthis.RsyncExe = new FileReference(RsyncExePath);
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tthis.RsyncExe = FileReference.Combine(UnrealBuildTool.EngineDirectory, "Extras", "ThirdPartyNotUE", "cwrsync", "bin", "rsync.exe");
`t`t`t}
`t`t`tstring SshExePath = Environment.GetEnvironmentVariable("UNREAL_ENGINE_SSH_PATH");
`t`t`tif (!string.IsNullOrWhiteSpace(SshExePath))
`t`t`t{
`t`t`t`tthis.SshExe = new FileReference(SshExePath);
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tthis.SshExe = FileReference.Combine(UnrealBuildTool.EngineDirectory, "Extras", "ThirdPartyNotUE", "cwrsync", "bin", "ssh.exe");
`t`t`t}
"@;
                    },
                    @{
                        CheckNotContains                = "public RemoteMac(FileReference ProjectFile, string InOverrideServerName = null, string InOverrideUserName = null) /* Redpoint */";
                        IsUnrealEngine5                 = $false;
                        ExpectNotApplicableToAllEngines = $true;
                        Find                            = @"
`t`tpublic RemoteMac(FileReference ProjectFile)
`t`t{
`t`t`tthis.RsyncExe = FileReference.Combine(Unreal.EngineDirectory, "Extras", "ThirdPartyNotUE", "cwrsync", "bin", "rsync.exe");
`t`t`tthis.SshExe = FileReference.Combine(Unreal.EngineDirectory, "Extras", "ThirdPartyNotUE", "cwrsync", "bin", "ssh.exe");
"@;
                        Replace                         = @"
`t`tpublic RemoteMac(FileReference ProjectFile, string InOverrideServerName = null, string InOverrideUserName = null) /* Redpoint */
`t`t{
`t`t`tstring RsyncExePath = Environment.GetEnvironmentVariable("UNREAL_ENGINE_RSYNC_PATH");
`t`t`tif (!string.IsNullOrWhiteSpace(RsyncExePath))
`t`t`t{
`t`t`t`tthis.RsyncExe = new FileReference(RsyncExePath);
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tthis.RsyncExe = FileReference.Combine(Unreal.EngineDirectory, "Extras", "ThirdPartyNotUE", "cwrsync", "bin", "rsync.exe");
`t`t`t}
`t`t`tstring SshExePath = Environment.GetEnvironmentVariable("UNREAL_ENGINE_SSH_PATH");
`t`t`tif (!string.IsNullOrWhiteSpace(SshExePath))
`t`t`t{
`t`t`t`tthis.SshExe = new FileReference(SshExePath);
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tthis.SshExe = FileReference.Combine(Unreal.EngineDirectory, "Extras", "ThirdPartyNotUE", "cwrsync", "bin", "ssh.exe");
`t`t`t}
"@;
                    },
                    @{
                        CheckNotContains = "public RemoteMac(FileReference? ProjectFile, string? InOverrideServerName = null, string? InOverrideUserName = null) /* Redpoint */";
                        IsUnrealEngine5  = $true;
                        Find             = @"
`t`tpublic RemoteMac(FileReference? ProjectFile)
`t`t{
`t`t`tthis.RsyncExe = FileReference.Combine(Unreal.EngineDirectory, "Extras", "ThirdPartyNotUE", "cwrsync", "bin", "rsync.exe");
`t`t`tthis.SshExe = FileReference.Combine(Unreal.EngineDirectory, "Extras", "ThirdPartyNotUE", "cwrsync", "bin", "ssh.exe");
"@;
                        Replace          = @"
`t`tpublic RemoteMac(FileReference? ProjectFile, string? InOverrideServerName = null, string? InOverrideUserName = null) /* Redpoint */
`t`t{
`t`t`tstring RsyncExePath = Environment.GetEnvironmentVariable("UNREAL_ENGINE_RSYNC_PATH");
`t`t`tif (!string.IsNullOrWhiteSpace(RsyncExePath))
`t`t`t{
`t`t`t`tthis.RsyncExe = new FileReference(RsyncExePath);
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tthis.RsyncExe = FileReference.Combine(Unreal.EngineDirectory, "Extras", "ThirdPartyNotUE", "cwrsync", "bin", "rsync.exe");
`t`t`t}
`t`t`tstring SshExePath = Environment.GetEnvironmentVariable("UNREAL_ENGINE_SSH_PATH");
`t`t`tif (!string.IsNullOrWhiteSpace(SshExePath))
`t`t`t{
`t`t`t`tthis.SshExe = new FileReference(SshExePath);
`t`t`t}
`t`t`telse
`t`t`t{
`t`t`t`tthis.SshExe = FileReference.Combine(Unreal.EngineDirectory, "Extras", "ThirdPartyNotUE", "cwrsync", "bin", "ssh.exe");
`t`t`t}
"@;
                    },
                    @{
                        CheckNotContains = "if (InOverrideServerName != null) /* Redpoint */";
                        Find             = @"
`t`t`tConfigHierarchy Ini = ConfigCache.ReadHierarchy(ConfigHierarchyType.Engine, EngineIniPath, UnrealTargetPlatform.IOS);
"@;
                        Replace          = @"
`t`t`tConfigHierarchy Ini = ConfigCache.ReadHierarchy(ConfigHierarchyType.Engine, EngineIniPath, UnrealTargetPlatform.IOS);

`t`t`tif (InOverrideServerName != null) /* Redpoint */
`t`t`t{
`t`t`t`tServerName = InOverrideServerName;
`t`t`t}
`t`t`tif (InOverrideUserName != null)
`t`t`t{
`t`t`t`tUserName = InOverrideUserName;
`t`t`t}
"@;
                    },
                    @{
                        CheckNotContains = "BasicRsyncArguments.Add(`"--no-p`"); /* Redpoint */";
                        Find             = @"
`t`t`tBasicRsyncArguments.Add("--chmod=ugo=rwx");
"@;
                        Replace          = @"
`t`t`tBasicRsyncArguments.Add("--no-p"); /* Redpoint */
`t`t`tBasicRsyncArguments.Add("--no-g");
`t`t`tBasicRsyncArguments.Add("--chmod=ugo=rwx");
"@;
                    },
                    @{
                        CheckNotContains = "public string GetRemotePath(string LocalPath) /* Redpoint */";
                        Find             = "private string GetRemotePath(string LocalPath)";
                        Replace          = "public string GetRemotePath(string LocalPath) /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "public static string EscapeShellArgument(string Argument) /* Redpoint */";
                        Find             = "private static string EscapeShellArgument(string Argument)";
                        Replace          = "public static string EscapeShellArgument(string Argument) /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "public void UploadFile(FileReference LocalFile) /* Redpoint */";
                        Find             = "void UploadFile(FileReference LocalFile)";
                        Replace          = "public void UploadFile(FileReference LocalFile) /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "public void UploadDirectory(DirectoryReference LocalDirectory) /* Redpoint */";
                        Find             = "void UploadDirectory(DirectoryReference LocalDirectory)";
                        Replace          = "public void UploadDirectory(DirectoryReference LocalDirectory) /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "public void DownloadDirectory(DirectoryReference LocalDirectory) /* Redpoint */";
                        Find             = "private void DownloadDirectory(DirectoryReference LocalDirectory)";
                        Replace          = "public void DownloadDirectory(DirectoryReference LocalDirectory) /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "public string ConvertRemotePathsToLocal(string Text) /* Redpoint */";
                        Find             = "private string ConvertRemotePathsToLocal(string Text)";
                        Replace          = "public string ConvertRemotePathsToLocal(string Text) /* Redpoint */";
                    },
                    @{
                        CheckNotContains = "public int ExecuteAndCaptureOutput(string Command, out StringBuilder Output) /* Redpoint */";
                        Find             = @"
`t`tprotected int ExecuteAndCaptureOutput(string Command, out StringBuilder Output)
`t`t{
`t`t`tStringBuilder FullCommand = new StringBuilder();
`t`t`tforeach(string CommonSshArgument in CommonSshArguments)
`t`t`t{
`t`t`t`tFullCommand.AppendFormat("{0} ", CommonSshArgument);
`t`t`t}
`t`t`tFullCommand.Append(Command.Replace("\"", "\\\""));

`t`t`tusing(Process SSHProcess = new Process())
`t`t`t{
`t`t`t`tOutput = new StringBuilder();

`t`t`t`tStringBuilder OutputLocal = Output;
`t`t`t`tDataReceivedEventHandler OutputHandler = (E, Args) => { if(Args.Data != null){ OutputLocal.Append(Args.Data); } };

`t`t`t`tSSHProcess.StartInfo.FileName = SshExe.FullName;
`t`t`t`tSSHProcess.StartInfo.WorkingDirectory = SshExe.Directory.FullName;
`t`t`t`tSSHProcess.StartInfo.Arguments = FullCommand.ToString();
`t`t`t`tSSHProcess.OutputDataReceived += OutputHandler;
`t`t`t`tSSHProcess.ErrorDataReceived += OutputHandler;

`t`t`t`tLog.TraceLog("[SSH] {0} {1}", Utils.MakePathSafeToUseWithCommandLine(SSHProcess.StartInfo.FileName), SSHProcess.StartInfo.Arguments);
`t`t`t`treturn Utils.RunLocalProcess(SSHProcess);
`t`t`t}
`t`t}
"@;
                        Replace          = @"
`t`tpublic int ExecuteAndCaptureOutput(string Command, out StringBuilder Output) /* Redpoint */
`t`t{
`t`t`tStringBuilder FullCommand = new StringBuilder();
`t`t`tforeach (string CommonSshArgument in CommonSshArguments)
`t`t`t{
`t`t`t`tFullCommand.AppendFormat("{0} ", CommonSshArgument);
`t`t`t}
`t`t`tFullCommand.Append(Command.Replace("\"", "\\\""));

`t`t`tusing (Process SSHProcess = new Process())
`t`t`t{
`t`t`t`tOutput = new StringBuilder();

`t`t`t`tStringBuilder OutputLocal = Output;
`t`t`t`tDataReceivedEventHandler OutputHandler = (E, Args) => { if (Args.Data != null) { OutputLocal.AppendLine(Args.Data); } };

`t`t`t`tSSHProcess.StartInfo.FileName = SshExe.FullName;
`t`t`t`tSSHProcess.StartInfo.WorkingDirectory = SshExe.Directory.FullName;
`t`t`t`tSSHProcess.StartInfo.Arguments = FullCommand.ToString();
`t`t`t`tSSHProcess.OutputDataReceived += OutputHandler;
`t`t`t`tSSHProcess.ErrorDataReceived += OutputHandler;

`t`t`t`tLog.TraceLog("[SSH] {0} {1}", Utils.MakePathSafeToUseWithCommandLine(SSHProcess.StartInfo.FileName), SSHProcess.StartInfo.Arguments);
`t`t`t`treturn Utils.RunLocalProcess(SSHProcess);
`t`t`t}
`t`t}

`t`t/// <summary>
`t`t/// Execute a remote command, capturing the output text
`t`t/// </summary>
`t`t/// <param name="Command">Command to be executed</param>
`t`t/// <param name="StdOutLineEmitted">Called for each line emitted</param>
`t`t/// <returns></returns>
`t`tpublic int ExecuteAndCaptureOutput(string Command, Action<string> StdOutLineEmitted)
`t`t{
`t`t`tStringBuilder FullCommand = new StringBuilder();
`t`t`tforeach (string CommonSshArgument in CommonSshArguments)
`t`t`t{
`t`t`t`tFullCommand.AppendFormat("{0} ", CommonSshArgument);
`t`t`t}
`t`t`tFullCommand.Append(Command.Replace("\"", "\\\""));

`t`t`tusing (Process SSHProcess = new Process())
`t`t`t{
`t`t`t`tDataReceivedEventHandler OutputHandler = (E, Args) => { if (Args.Data != null) { StdOutLineEmitted(Args.Data); } };

`t`t`t`tSSHProcess.StartInfo.FileName = SshExe.FullName;
`t`t`t`tSSHProcess.StartInfo.WorkingDirectory = SshExe.Directory.FullName;
`t`t`t`tSSHProcess.StartInfo.Arguments = FullCommand.ToString();
`t`t`t`tSSHProcess.OutputDataReceived += OutputHandler;
`t`t`t`tSSHProcess.ErrorDataReceived += OutputHandler;

`t`t`t`tLog.TraceLog("[SSH] {0} {1}", Utils.MakePathSafeToUseWithCommandLine(SSHProcess.StartInfo.FileName), SSHProcess.StartInfo.Arguments);
`t`t`t`treturn Utils.RunLocalProcess(SSHProcess);
`t`t`t}
`t`t}
"@;
                    }
                )
            }
        )
    },
    @{
        SectionName        = "patch-gauntlet-android";
        SectionDescription = "Patching Gauntlet to fix Android deployment issues on modern devices...";
        Files              = @(
            @{
                RelativePath = "Engine\Source\Programs\AutomationTool\Gauntlet\Platform\Android\Gauntlet.AndroidBuildSource.cs";
                Patches      = @(
                    @{
                        CheckNotContains = "Dictionary<string, string> FilesToInstall = OBBMatches.Cast<Match>().ToDictionary(M => Path.Combine(AbsPath, M.Groups[1].ToString()), M => M.Groups[2].ToString().Replace(`"%STORAGE%/obb/`", `"%STORAGE%/Android/obb/`"));";
                        Find             = "Dictionary<string, string> FilesToInstall = OBBMatches.Cast<Match>().ToDictionary(M => Path.Combine(AbsPath, M.Groups[1].ToString()), M => M.Groups[2].ToString());";
                        Replace          = "Dictionary<string, string> FilesToInstall = OBBMatches.Cast<Match>().ToDictionary(M => Path.Combine(AbsPath, M.Groups[1].ToString()), M => M.Groups[2].ToString().Replace(`"%STORAGE%/obb/`", `"%STORAGE%/Android/obb/`"));";
                    }
                );
            },
            @{
                RelativePath = "Engine\Source\Programs\AutomationTool\Gauntlet\Platform\Android\Gauntlet.TargetDeviceAndroid.cs";
                Patches      = @(
                    @{
                        CheckNotContains = "string OBBRemoteDestination = string.Format(`"{0}/Android/obb/{1}`", StorageLocation, Build.AndroidPackageName);";
                        Find             = "string OBBRemoteDestination = string.Format(`"{0}/obb/{1}`", StorageLocation, Build.AndroidPackageName);";
                        Replace          = "string OBBRemoteDestination = string.Format(`"{0}/Android/obb/{1}`", StorageLocation, Build.AndroidPackageName);";
                    }
                );
            }
        );
    },
    @{
        SectionName        = "patch-mac-toolchain";
        SectionDescription = "Patching MacToolChain.cs to fix a crash that's present in 4.27...";
        Files              = @(
            @{
                RelativePath = "Engine\Source\Programs\UnrealBuildTool\Platform\Mac\MacToolChain.cs";
                Patches      = @(
                    @{
                        CheckNotContains = "Log.TraceWarning(`"Unexpected third party dylib location when generating RPATH entries: {0}. Skipping.`", LibraryFullPath);";
                        Find             = "Log.TraceWarning(`"Unexpected third party dylib location when generating RPATH entries: {1}. Skipping.`", LibraryFullPath);";
                        Replace          = "Log.TraceWarning(`"Unexpected third party dylib location when generating RPATH entries: {0}. Skipping.`", LibraryFullPath);";
                    }
                );
            }
        );
    }

    # todo: Determine if we should port the ProjectParams fixes for AutomationTool over. These are highly invasive patches (even more so than iOS Gauntlet support on Windows), and are only relevant if you're trying to use BuildGraph to build multiple, specific, confidential platforms at once on the same machine (not through GitLab).
)
foreach ($Section in $SimplePatches) {
    Start-Section $Section.SectionName $Section.SectionDescription
    foreach ($File in $Section.Files) {
        $FullPath = (Join-Path $EnginePath $File.RelativePath)
        $BaseName = [System.IO.Path]::GetFileName($FullPath)
        if (!(Test-Path $FullPath)) {
            Write-Warning "patch ${BaseName}: file does not exist"
            continue
        }
        $i = 1
        foreach ($Patch in $File.Patches) {
            if ($IsUnrealEngine5) {
                if ($null -ne $Patch.CheckContains) {
                    $Patch.CheckContains = $Patch.CheckContains.Replace("`r`n", "`n")
                }
                if ($null -ne $Patch.CheckNotContains) {
                    $Patch.CheckNotContains = $Patch.CheckNotContains.Replace("`r`n", "`n")
                }
                if ($null -ne $Patch.Find) {
                    $Patch.Find = $Patch.Find.Replace("`r`n", "`n")
                }
                if ($null -ne $Patch.Replace) {
                    $Patch.Replace = $Patch.Replace.Replace("`r`n", "`n")
                }
            }
            $Content = Get-Content -Path $FullPath -Raw
            if ($IsUnrealEngine5) {
                $Content = $Content.Replace("`r`n", "`n")
            }
            if ($null -ne $Patch.UnrealEngine5) {
                if ($Patch.UnrealEngine5 -and !$IsUnrealEngine5) {
                    Write-Output "patch ${BaseName} #${i}: does not apply to Unreal Engine 4"
                    continue;
                }
                if (!$Patch.UnrealEngine5 -and $IsUnrealEngine5) {
                    Write-Output "patch ${BaseName} #${i}: does not apply to Unreal Engine 5"
                    continue;
                }
            }
            if ($null -ne $Patch.CheckContains) {
                if ($Content.Contains($Patch.CheckContains)) {
                    $NewContent = $Content.Replace($Patch.Find, $Patch.Replace)
                    if ($NewContent -eq $Content) {
                        if ($Patch.ExpectNotApplicableToAllEngines -eq $true) {
                            Write-Output "patch ${BaseName} #${i}: not applicable to this engine version"
                        }
                        else {
                            Write-Warning "patch ${BaseName} #${i}: replace failed, patch does not apply"
                        }
                        $i = $i + 1
                        continue
                    }
                    if ($NewContent.Contains($Patch.CheckContains)) {
                        Write-Error "patch ${BaseName} #${i}: malformed 'contains check' still applies after patch: $($Patch.CheckContains)"
                        $i = $i + 1
                        continue
                    }
                    Set-RetryableContent -Path $FullPath -Value $NewContent
                    Write-Output "patch ${BaseName} #${i}: ok"
                }
                else {
                    Write-Output "patch ${BaseName} #${i}: ok"
                }
            }
            elseif ($null -ne $Patch.CheckNotContains) {
                if (!$Content.Contains($Patch.CheckNotContains)) {
                    $NewContent = $Content.Replace($Patch.Find, $Patch.Replace)
                    if ($NewContent -eq $Content) {
                        if ($Patch.ExpectNotApplicableToAllEngines -eq $true) {
                            Write-Output "patch ${BaseName} #${i}: not applicable to this engine version"
                        }
                        else {
                            Write-Warning "patch ${BaseName} #${i}: replace failed, patch does not apply"
                        }
                        $i = $i + 1
                        continue
                    }
                    if (!$NewContent.Contains($Patch.CheckNotContains)) {
                        Write-Error "patch ${BaseName} #${i}: malformed 'not contains check' still applies after patch: $($Patch.CheckNotContains)"
                        $i = $i + 1
                        continue
                    }
                    Set-RetryableContent -Path $FullPath -Value $NewContent
                    Write-Output "patch ${BaseName} #${i}: ok"
                }
                else {
                    Write-Output "patch ${BaseName} #${i}: ok"
                }
            }
            $i = $i + 1
        }
    }
    Stop-Section $Section.SectionName
}

# Download all of the binary files where needed.
$GitCommit = Get-Content -Raw -Path "$EnginePath\.gitcheckout"
$DepCommit = ""
if (Test-Path "$EnginePath\.depcheckout") {
    $DepCommit = Get-Content -Raw -Path "$EnginePath\.depcheckout"
}
if ($GitCommit.Trim() -ne $DepCommit.Trim()) {
    Start-Section "git-dependencies" "Updating binary dependencies..."
    $CacheFolder = ""
    if ($global:IsMacOS) {
        $CacheFolder = "/Users/Shared/UEDepsCache"
    }
    elseif ($global:IsLinux) {
        $CacheFolder = "/var/cache/UEDepsCache"
    }
    else {
        $CacheFolder = "$env:SYSTEMDRIVE\UEDepsCache"
    }
    if (!(Test-Path "$CacheFolder")) {
        New-Item -ItemType Directory -Path "$CacheFolder" | Out-Null
    }
    if ($global:IsMacOS) {
        & "$EnginePath/Engine/Build/BatchFiles/Mac/GitDependencies.sh" "--cache=$CacheFolder" --cache-days=30
    }
    elseif ($global:IsLinux) {
        & "$EnginePath/Engine/Build/BatchFiles/Linux/GitDependencies.sh" "--cache=$CacheFolder" --cache-days=30
    }
    else {
        & "$EnginePath\Engine\Binaries\DotNET\GitDependencies.exe" "--cache=$CacheFolder" --cache-days=30
    }
    Stop-Section "git-dependencies"
    if ($LastExitCode -ne 0) {
        exit $LastExitCode
    }
    else {
        Set-Content -Path "$EnginePath\.depcheckout" -Value $GitCommit
    }
}

Write-Output ">> Engine has been prepared for build!"