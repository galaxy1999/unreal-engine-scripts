param(
    # The engine you are targeting. Can be a version number or an absolute path.
    [Parameter(Mandatory = $true)][string] $Engine,
    # The path to the engine for builds that run on macOS machines. Not required
    # unless you're building either Mac or IOS platforms.
    [Parameter(Mandatory = $false)][string] $MacEnginePath,
    # The distribution name, as it appears in BuildConfig.json.
    [Parameter(Mandatory = $true)][string] $Distribution,
    # The path to output the .gitlab-ci.yml file to.
    [Parameter(Mandatory = $true)][string] $GitLabYamlPath,
    # The prefix for the GitLab agent tag for running builds. If this is "abc", then jobs will be defined
    # to use the tags abc-windows and abc-mac.
    [Parameter(Mandatory = $true)][string] $GitLabAgentTagPrefix,
    # The absolute path to shared storage on Windows. Must start with a drive letter (like X:\). Must have a trailing path.
    [Parameter(Mandatory = $true)][string] $WindowsSharedStorageAbsolutePath,
    # The absolute path to shared storage on macOS. If present, must have a trailing path. If not specified, the build server will be used as the artifact transport.
    [Parameter(Mandatory = $false)][string] $MacSharedStorageAbsolutePath,
    # The path to the Linux toolchain on Windows.
    [string] $WindowsLinuxToolchainPath,
    # If true, executes the build phase.
    [switch][bool] $ExecuteBuild,
    # If true, executes the test phase.
    [switch][bool] $ExecuteTests,
    # If true, executes the deployment phase.
    [switch][bool] $ExecuteDeployment,
    # If true, the working directory will be mapped as a drive on Windows to reduce the length of file paths. Engine builds
    # always map drives, but you can optionally enable this for project or plugin builds.
    [switch][bool] $MapDriveForShorterPaths
)

. $PSScriptRoot\Internal_SetupEnvironment.ps1
. $PSScriptRoot\Internal_Convert_BuildGraphToBuildPipeline.ps1
. $PSScriptRoot\Internal_Convert_BuildPipelineToGitLab.ps1

$BuildConfig = Get-BuildConfig
$EnginePath = Resolve-EnginePath $Engine
$DistributionConfig = Get-Distribution $Distribution

# Set up common variables.
$PluginName = $BuildConfig.PluginName

# Determine if the engine is Unreal Engine 5.
$local:IsUnrealEngine5 = $false
if ($global:IsMacOS -or $global:IsLinux) {
    $local:RunUAT = Get-Content -Raw -Path "$EnginePath/Engine/Build/BatchFiles/RunUAT.sh"
    if ($local:RunUAT.Contains("Unreal Engine AutomationTool setup script")) {
        $local:IsUnrealEngine5 = $true
    }
}
else {
    $local:RunUAT = Get-Content -Raw -Path "$EnginePath\Engine\Build\BatchFiles\RunUAT.bat"
    if ($local:RunUAT.Contains("SET_TURNKEY_VARIABLES")) {
        $local:IsUnrealEngine5 = $true
    }
}

# Compute BuildGraph settings.
. $PSScriptRoot\BuildGraphSettings_Plugin.ps1
$Settings = Get-BuildGraphSettingsForPlugin $PluginName $DistributionConfig $EnginePath $false $false $ExecuteBuild $ExecuteTests $ExecuteDeployment $BuildConfig.Copyright

# Generate .gitlab-ci.yml from BuildGraph!
$BuildGraphArgsArray = @()
$BuildGraphArgsArrayWindows = @()
$BuildGraphArgsArrayMac = @()
$Settings.Keys | ForEach-Object {
    $BuildGraphArgsArray += "-set:$_=`"$($Settings.Item($_).Replace("__REPOSITORY_ROOT__", "$ProjectRoot").Replace("/", "\").TrimEnd("\"))`""

    $local:MacArg = "-set__$_=`"$($Settings.Item($_).Replace("__REPOSITORY_ROOT__", "`$(pwd)").Replace("\", "/").TrimEnd("/"))`""
    $local:MacArg = $local:MacArg.Replace("\", "\\\").Replace("`"", "\`"")
    $local:MacArg = "`"$local:MacArg`""
    $BuildGraphArgsArrayMac += $local:MacArg

    $local:WindowsArg = "-set:$_=`"$($Settings.Item($_).Replace("__REPOSITORY_ROOT__", "`$((Get-Location).Path)").Replace("/", "\").TrimEnd("\"))`""
    $local:WindowsArg = $local:WindowsArg.Replace("``", "````").Replace("`"", "```"")
    $local:WindowsArg = "`"$local:WindowsArg`""
    $BuildGraphArgsArrayWindows += $local:WindowsArg
}
$BuildGraphArgsStringWindows = $BuildGraphArgsArrayWindows -join " "
$BuildGraphArgsStringMac = $BuildGraphArgsArrayMac -join " "
$env:uebp_LOCAL_ROOT = $EnginePath
$env:IsBuildMachine = "1"
& "$PSScriptRoot\Internal_RunUAT.ps1" -UATEnginePath "$EnginePath" BuildGraph `
    -Script="$PSScriptRoot\BuildGraph_Plugin.xml" `
    -Target="End" `
    "-set__EnginePath=`"$EnginePath`"" `
    $BuildGraphArgsArray `
    -Export="$PSScriptRoot\..\Temp\BuildGraphExport.json" `
    -noP4
$env:IsBuildMachine = "0"
if ($LastExitCode -eq 0) {
    $local:BuildGraphEnvironment = [BuildGraphEnvironment]@{
        Engine        = $Engine;
        IsEngineBuild = $false;
        PipelineId    = $env:CI_PIPELINE_ID;
        Windows       = [WindowsEnvironment]@{
            ProjectRoot               = ($Settings.Item("ProjectRoot").Replace("__REPOSITORY_ROOT__", "`$((Get-Location).Path)").Replace("/", "\"));
            BuildGraphSettings        = $BuildGraphArgsStringWindows;
            SharedStorageAbsolutePath = $WindowsSharedStorageAbsolutePath;
            LinuxToolchainPath        = $WindowsLinuxToolchainPath;
            MapDriveForShorterPaths   = $MapDriveForShorterPaths;
        };
        Mac           = if ($null -eq $MacSharedStorageAbsolutePath -or "" -eq $MacSharedStorageAbsolutePath) {
            [MacEnvironment]@{
                ProjectRoot               = ($Settings.Item("ProjectRoot").Replace("__REPOSITORY_ROOT__", "`$(pwd)").Replace("\", "/"));
                BuildGraphSettings        = $BuildGraphArgsStringMac;
                MacEnginePathOverride     = $MacEnginePath;
                SharedStorageAbsolutePath = $null;
                ArtifactTransport         = [MacArtifactTransport]::BuildServer;
            };
        }
        else {
            [MacEnvironment]@{
                ProjectRoot               = ($Settings.Item("ProjectRoot").Replace("__REPOSITORY_ROOT__", "`$(pwd)").Replace("\", "/"));
                BuildGraphSettings        = $BuildGraphArgsStringMac;
                MacEnginePathOverride     = $MacEnginePath;
                SharedStorageAbsolutePath = $MacSharedStorageAbsolutePath;
                ArtifactTransport         = [MacArtifactTransport]::Direct;
            };
        }
    }
    $local:BuildPipeline = Convert-BuildGraphToBuildPipeline `
        -Distribution $Distribution `
        -DistributionConfig $DistributionConfig `
        -BuildGraph (Get-Content -Raw -Path "$PSScriptRoot\..\Temp\BuildGraphExport.json" | ConvertFrom-Json) `
        -BuildGraphScriptName "BuildGraph_Plugin.xml" `
        -Environment $local:BuildGraphEnvironment `
        -IsUnrealEngine5 $local:IsUnrealEngine5

    Dump-BuildPipeline $local:BuildPipeline

    $local:GitLabYaml = Convert-BuildPipelineToGitLab `
        -BuildPipeline $local:BuildPipeline `
        -AgentTagPrefix $local:GitLabAgentTagPrefix
    Set-Content -Path $local:GitLabYamlPath -Value $local:GitLabYaml
    Write-Host $local:GitLabYaml
}

exit $LastExitCode