##### COMMON SETUP #####

$global:ErrorActionPreference = "Stop"

if ($null -eq (Get-Variable -Scope Global Redpoint_PublicScriptWasCalled -ErrorAction SilentlyContinue)) {
    Write-Error "This script can't be called publicly. Use one of the scripts directly under BuildScripts instead of the Lib folder."
    exit 1
}

if ($null -eq (Get-Variable -Scope Global Redpoint_HasScriptEnvironmentSetup -ErrorAction SilentlyContinue)) {
    $global:ProjectRoot = (Resolve-Path -Path $PSScriptRoot\..\..).Path
    $global:Redpoint_HasScriptEnvironmentSetup = $true
    $global:RelativePSScriptRoot = $PSScriptRoot.Substring($global:ProjectRoot.Length + 1)
}

##### COMMON FUNCTIONS #####

<#
    .Description
    Get-BuildConfig returns the build configuration for this repository, caching it.
#>
function Get-BuildConfig() {
    if (!(Test-Path "$ProjectRoot\BuildConfig.json")) {
        Write-Error "Please create BuildConfig.json in your project root."
        return $null
    }
    $local:BaseConfig = (Get-Content -Raw "$ProjectRoot\BuildConfig.json" | ConvertFrom-Json)
    if ($null -eq $local:BaseConfig.Distributions) {
        Add-Member -InputObject $local:BaseConfig -NotePropertyName Distributions -NotePropertyValue @()
    }
    if ($null -ne $local:BaseConfig.Include) {
        foreach ($Include in $local:BaseConfig.Include) {
            $local:PartConfig = (Get-Content -Raw "$ProjectRoot\$local:Include" | ConvertFrom-Json)
            foreach ($Subdist in $local:PartConfig.Distributions) {
                $local:BaseConfig.Distributions += $local:Subdist
            }
        }
    }
    return $local:BaseConfig
}

<#
    .Description
    Get-IsProject returns if this repository is configured as an Unreal Engine 
    project (instead of a plugin).
#>
function Get-IsProject() {
    return (Get-BuildConfig).Type -eq "Project"
}

<#
    .Description
    Get-IsEngine returns if this repository is configured to build
    Unreal Engine itself.
#>
function Get-IsEngine() {
    return (Get-BuildConfig).Type -eq "Engine"
}

<#
    .Description
    Get-IsPlugin returns if this repository is configured to build an 
    Unreal Engine plugin.
#>
function Get-IsPlugin() {
    return !((Get-IsProject) -or (Get-IsEngine))
}

<#
    .Description
    Get-Distribution returns the distribution config for the specified distribution name.
#>
function Get-Distribution([string] $Distribution) {
    $local:BuildConfig = Get-BuildConfig
    $local:DistributionConfig = $null
    foreach ($local:CandidateDistribution in $local:BuildConfig.Distributions) {
        if ($local:CandidateDistribution.Name -eq $local:Distribution) {
            $local:DistributionConfig = $local:CandidateDistribution
            break
        }
    }
    if ($local:DistributionConfig -eq $null) {
        Write-Error "Distribution '$Distribution' does not appear in BuildConfig.json"
        return $null
    }
    return $local:DistributionConfig
}

<#
    .Description
    Resolve-EngineToPath resolves the specified engine input to an absolute path to an 
    engine. Handles version numbers, registered engine IDs and absolute paths.
#>
function Resolve-EnginePath([string] $Engine) {
    # Try to get the engine from the registry.
    if (Test-Path "HKLM:\SOFTWARE\EpicGames\Unreal Engine\$Engine") {
        return (Get-ItemProperty -Path "HKLM:\SOFTWARE\EpicGames\Unreal Engine\$Engine" -Name "InstalledDirectory").InstalledDirectory;
    }

    # If the engine matches a version regex [45]\.[0-9]+(EA)?, check the Program Files folder.
    if ($Engine -match "[45]\.[0-9]+(EA)?") {
        if (Test-Path "$env:PROGRAMFILES\Epic Games\UE_$Engine") {
            return "$env:PROGRAMFILES\Epic Games\UE_$Engine"
        }
    }

    # If the engine path ends in a \, remove it because it creates problems when the
    # path is passed in on the command line (usually escaping a quote " ...)
    if ($Engine.EndsWith("\")) {
        $Engine = $Engine.Substring(0, $Engine.Length - 1)
    }

    # If this is an absolute path to an engine, use that.
    if ([System.IO.Path]::IsPathRooted("$Engine")) {
        if (Test-Path "$Engine") {
            return $Engine
        }
    }

    # Otherwise, we couldn't locate the engine.
    Write-Error "Unable to locate engine description by `"$Engine`" (checked registry, Program Files and absolute path)."
    return $null
}

<#
    .Description
    Compute-BuildGraphInputArgs determines the target, platform and configuration strings to pass
    to BuildGraph, based on the distribution's build settings.
#>
function Compute-BuildGraphInputArgs($DistributionConfig, [string] $Name, [bool] $IsUnrealEngine5) {
    if ($local:DistributionConfig.Build -eq $null -or
        $local:DistributionConfig.Build.$local:Name -eq $null) {
        return @{
            Targets = "";
            TargetPlatforms = "";
            Configurations = "";
        }
    }
    $local:Targets = @("UE4" + $local:Name)
    $local:Platforms = @()
    $local:Configurations = @("Development", "Shipping")
    if ($local:DistributionConfig.Build.$local:Name.Target -ne $null) {
        $local:Targets = @($local:DistributionConfig.Build.$local:Name.Target)
    }
    if ($local:DistributionConfig.Build.$local:Name.Targets -ne $null) {
        $local:Targets = $local:DistributionConfig.Build.$local:Name.Targets
    }
    if ($local:DistributionConfig.Build.$local:Name.Platforms -ne $null) {
        $local:Platforms = $local:DistributionConfig.Build.$local:Name.Platforms
    }
    if ($local:DistributionConfig.Build.$local:Name.Configurations -ne $null) {
        $local:Configurations = $local:DistributionConfig.Build.$local:Name.Configurations
    }
    $local:AdjustedTargets = @()
    foreach ($Target in $local:Targets) {
        if ($local:IsUnrealEngine5 -and $Target.StartsWith("UE4")) {
            $local:AdjustedTargets += ("Unreal" + $Target.Substring(3))
        } else {
            $local:AdjustedTargets += $Target
        }
    }
    return @{
        Targets = ($local:AdjustedTargets -join ";");
        TargetPlatforms = ($local:Platforms -join ";");
        Configurations = ($local:Configurations -join ";");
    }
}

<#
    .Description
    Compute-GauntletTests determines the Gauntlet test string to pass to BuildGraph based on the
    distribution's test settings.
#>
function Compute-GauntletTestString($DistributionConfig) {
    $local:GauntletTests = @()
    if ($local:DistributionConfig.Tests -ne $null) {
        foreach ($local:Test in $local:DistributionConfig.Tests) {
            if ($local:Test.Type -eq "Gauntlet") {
                $local:Requires = @()
                foreach ($local:Require in $local:Test.Requires) {
                    foreach ($local:Platform in $local:Require.Platforms) {
                        $local:Requires += "#$($local:Require.Type)Staged_$($local:Require.Target)_$($local:Platform)_$($local:Require.Configuration)"
                    }
                }
                $local:GauntletTests += "$($local:Test.Name)~$($local:Requires -join ";")"
            }
        }
    }
    return $local:GauntletTests
}

<#
    .Description
    Get-FilterInclude reads the filter rules from the file specified in the distribution
    configuration and returns the include rules as a string.
#>
function Get-FilterInclude($DistributionConfig) {
    if ($DistributionConfig -eq $null -or
        $DistributionConfig.Package -eq $null -or
        $DistributionConfig.Package.Filter -eq $null) {
        return ""
    }
    $FilterRules = @()
    $RawFilterRules = Get-Content -Path "$global:ProjectRoot\$($DistributionConfig.Package.Filter)"
    foreach ($RawFilterRule in $RawFilterRules) {
        if ($RawFilterRule -eq "[FilterPlugin]") {
            continue
        } elseif ($RawFilterRule.StartsWith(";")) {
            continue
        } elseif ($RawFilterRule.StartsWith("-")) {
            continue
        } elseif ($RawFilterRule.Trim() -eq "") {
            continue
        } else {
            $FilterRules += $RawFilterRule
        }
    }
    return $FilterRules -join ";"
}

<#
    .Description
    Get-FilterExclude reads the filter rules from the file specified in the distribution
    configuration and returns the include rules as a string.
#>
function Get-FilterExclude($DistributionConfig) {
    if ($DistributionConfig -eq $null -or
        $DistributionConfig.Package -eq $null -or
        $DistributionConfig.Package.Filter -eq $null) {
        return ""
    }
    $FilterRules = @()
    $RawFilterRules = Get-Content -Path "$global:ProjectRoot\$($DistributionConfig.Package.Filter)"
    foreach ($RawFilterRule in $RawFilterRules) {
        if ($RawFilterRule -eq "[FilterPlugin]") {
            continue
        } elseif ($RawFilterRule.StartsWith(";")) {
            continue
        } elseif ($RawFilterRule.Trim() -eq "") {
            continue
        } elseif (!$RawFilterRule.StartsWith("-")) {
            continue
        } else {
            $FilterRules += $RawFilterRule.Substring(1)
        }
    }
    return $FilterRules -join ";"
}

<#
    .Description
    Get-UnrealEngineVersion returns the Unreal Engine version information for a given engine
    path, if possible.
#>
function Get-UnrealEngineVersion([string] $EnginePath) {
    if (!(Test-Path "$EnginePath\Engine\Build\Build.version")) {
        return $null
    }
    $Json = (Get-Content -Raw "$EnginePath\Engine\Build\Build.version" | ConvertFrom-Json)
    if ($Json.MajorVersion -eq $null -or $Json.MinorVersion -eq $null) {
        return $null
    }
    return $Json
}

<#
    .Description
    Compute-VersionNameAndNumber computes the version name and number to use for a plugin.
#>
function Compute-VersionNameAndNumber([string] $EnginePath) {
    $local:VersionNumber = "10000"
    $local:VersionName = "Unversioned"
    if ($env:CI_COMMIT_SHORT_SHA -ne $null -and $env:CI_COMMIT_SHORT_SHA.Length -ge 8) {
        $local:CurrentTime = Get-Date
        $local:UnixTimestamp = [Math]::Floor((New-TimeSpan -Start (Get-Date "01/01/1970") -End $local:CurrentTime).TotalSeconds)
        $local:VersionDateTime = $local:CurrentTime.ToString("yyyy.MM.dd")
        if ($env:OVERRIDE_DATE_VERSION -ne $null -and $env:OVERRIDE_DATE_VERSION -ne "") {
            $local:VersionDateTime = $env:OVERRIDE_DATE_VERSION
        }

        $local:EngineInfo = Get-UnrealEngineVersion $local:EnginePath
        if ($local:EngineInfo -eq $null) {
            Write-Error "Specified engine does not have a build.version file, but having version information is required in order to build and package plugins."
            return $null
        }
    
        $local:VersionNumber = "$($local:UnixTimestamp)$($local:EngineInfo.MinorVersion)"
        $local:VersionName = "$($local:VersionDateTime)-$($local:EngineInfo.MajorVersion).$($local:EngineInfo.MinorVersion)-$($env:CI_COMMIT_SHORT_SHA.Substring(0, 8))"
    
        $env:BUILD_VERSION_NAME = $local:VersionName
    
        Write-Host "Building as versioned package: $env:BUILD_VERSION_NAME"
        return @{
            VersionName = $VersionName;
            VersionNumber = $VersionNumber;
        }
    } else {
        Write-Host "Building as unversioned package"
        return @{
            VersionName = "Unversioned";
            VersionNumber = "10000";
        }
    }
}

<#
    .Description
    Get-StringHash computes a hash for a given string. Optionally truncates it to the specified length.

    Any trailing . characters are removed, as folder names with a trailing dot are treated differently
    on Windows and macOS.
#>
function Get-StringHash([string] $InputString, $Length) {
    $Sha = [System.Security.Cryptography.SHA256CryptoServiceProvider]::new()
    $InputBytes = $Sha.ComputeHash([System.Text.Encoding]::ASCII.GetBytes($InputString))
    $Alphabet = "0123456789abcdefghijklmnopqrstuvwxyz.-_"
    $Dividend = [System.Numerics.BigInteger]::new($InputBytes)
    $Builder = [System.Text.StringBuilder]::new()
    while ($Dividend -ne 0) {
        $Remainder = [System.Numerics.BigInteger]::new(0)
        $Dividend = [System.Numerics.BigInteger]::DivRem($Dividend, $Alphabet.Length, [ref] $Remainder)
        $Builder.Insert(0, $Alphabet[[System.Math]::Abs([int]$Remainder)]) | Out-Null
    }
    if ($null -eq $Length) {
        return $Builder.ToString().TrimEnd(".")
    } else {
        return $Builder.ToString().Substring(0, $Length).TrimEnd(".")
    }
}

<#
    .Description
    Get-EnginePrefix returns either "UE4" or "UE5" depending on the engine version.
#>
function Get-EnginePrefix([string] $EnginePath) {
    $EngineVersion = Get-UnrealEngineVersion $EnginePath
    if ($EngineVersion.MajorVersion -eq 5) {
        return "Unreal"
    } else {
        return "UE4"
    }
}

<#
    .Description
    Get-MSBuildFromRegistry is internally used by Get-MSBuildPath.
#>
function Get-MSBuildFromRegistry($Path, $Key, $Suffix) {
    $PathsToTry = @(
        "HKCU:\SOFTWARE\$Path",
        "HKLM:\SOFTWARE\$Path",
        "HKCU:\SOFTWARE\Wow6432Node\$Path",
        "HKLM:\SOFTWARE\Wow6432Node\$Path"
    )
    foreach ($PathToTry in $PathsToTry) {
        if (Test-Path "$PathToTry") {
            $Property = Get-ItemProperty -Path "$PathToTry" -Name $Key -ErrorAction SilentlyContinue
            if ($Property -ne $null) {
                $PropertyValue = $Property."$Key"
                if ($PropertyValue -ne $null) {
                    if (Test-Path "$PropertyValue$Suffix") {
                        return "$PropertyValue$Suffix"
                    }
                }
            }
        }
    }
    return $null
}

<#
    .Description
    Get-MSBuildPath finds the path to MSBuild, replicating the logic that Unreal Engine
    ships with inside GetMSBuildPath.bat. The difference is that this can handle paths
    with spaces more safely, and doesn't require wrapping things with a batch script.
#>
function Get-MSBuildPath() {
    if ($global:IsMacOS) {
        if (Test-Path "/Library/Frameworks/Mono.framework/Versions/Current/Commands/msbuild") {
            return "/Library/Frameworks/Mono.framework/Versions/Current/Commands/msbuild"
        }
        if (Test-Path "/Library/Frameworks/Mono.framework/Versions/Current/Commands/xbuild") {
            return "/Library/Frameworks/Mono.framework/Versions/Current/Commands/xbuild"
        }
        return $null
    }
    if (Test-Path "${env:ProgramFiles(x86)}\Microsoft Visual Studio\Installer\vswhere.exe") {
        $VsInstallPath = (& "C:\Program Files (x86)\Microsoft Visual Studio\Installer\vswhere.exe" -prerelease -latest -products * -requires Microsoft.Component.MSBuild -property installationPath)
        $VsInstallPath = $VsInstallPath.Trim()
        if (Test-Path "$VsInstallPath\MSBuild\Current\Bin\MSBuild.exe") {
            return "$VsInstallPath\MSBuild\Current\Bin\MSBuild.exe"
        }
        if (Test-Path "$VsInstallPath\MSBuild\15.0\Bin\MSBuild.exe") {
            return "$VsInstallPath\MSBuild\15.0\Bin\MSBuild.exe"
        }
    }
    $local:ProposedPath = (Get-MSBuildFromRegistry "Microsoft\VisualStudio\SxS\VS7" "15.0" "MSBuild\15.0\bin\MSBuild.exe")
    if ($local:ProposedPath -ne $null) {
        return $local:ProposedPath
    }
    if (Test-Path "${env:ProgramFiles(x86)}\MSBuild\14.0\bin\MSBuild.exe") {
        return "${env:ProgramFiles(x86)}\MSBuild\14.0\bin\MSBuild.exe"
    }
    $local:ProposedPath = (Get-MSBuildFromRegistry "Microsoft\MSBuild\ToolsVersions\14.0" "MSBuildToolsPath" "MSBuild.exe")
    if ($local:ProposedPath -ne $null) {
        return $local:ProposedPath
    }
    $local:ProposedPath = (Get-MSBuildFromRegistry "Microsoft\MSBuild\ToolsVersions\12.0" "MSBuildToolsPath" "MSBuild.exe")
    if ($local:ProposedPath -ne $null) {
        return $local:ProposedPath
    }
    return $null
}

<#
    .Description
    Get-ClangFormat finds the path to clang-format.exe
#>
function Get-ClangFormat() {
    if (Test-Path "${env:ProgramFiles(x86)}\Microsoft Visual Studio\Installer\vswhere.exe") {
        $VsInstallPath = (& "C:\Program Files (x86)\Microsoft Visual Studio\Installer\vswhere.exe" -prerelease -latest -products * -property installationPath)
        $VsInstallPath = $VsInstallPath.Trim()
        if (Test-Path "$VsInstallPath\VC\Tools\Llvm\bin\clang-format.exe") {
            return "$VsInstallPath\VC\Tools\Llvm\bin\clang-format.exe"
        }
        if (Test-Path "$VsInstallPath\VC\Tools\Llvm\x64\bin\clang-format.exe") {
            return "$VsInstallPath\VC\Tools\Llvm\x64\bin\clang-format.exe"
        }
    }
    $ClangFormatCommand = (Get-Command -ErrorAction SilentlyContinue "clang-format.exe")
    if ($ClangFormatCommand -ne $null) {
        return $ClangFormatCommand.Path
    }
    return $null
}

<#
    .Description
    Set-RetryableContent continues to retry writes to a file until it succeeds.
#>
function Set-RetryableContent([string] $Path, [string] $Value) {
    for ($i = 0; $i -lt 1000; $i++) {
        try {
            Set-Content -Force -Path $Path -Value $Value
            return
        } catch {
            if ($_.ToString().Contains("Stream was not readable.")) {
                Start-Sleep -Milliseconds 10
                continue
            }
        }
    }
    Write-Error "Unable to set value into path: $Path"
    exit 1
}

<#
    .Description
    Get-UnixTimestamp returns the Unix timestamp as an integer.
#>
function Get-UnixTimestamp() {
    return [int][double]::Parse((Get-Date -UFormat %s))
}

<#
    .Description
    Start-Section starts a block in the output. It takes into account whether
    the script is executing on GitLab or on Windows.
#>
function Start-Section([string] $SectionName, [string] $HeaderDescription) {
    if ($env:CI -eq "true" -and ($global:IsMacOS -or $global:IsLinux)) {
        Write-Host "`e[0Ksection_start:$(Get-UnixTimestamp):$($SectionName)[collapsed=false]`r`e[0K$($HeaderDescription)"
    } else {
        Write-Host ">> $HeaderDescription"
    }
}

<#
    .Description
    Stop-Section stops a block in the output. It takes into account whether
    the script is executing on GitLab or on Windows.
#>
function Stop-Section([string] $SectionName) {
    if ($env:CI -eq "true" -and ($global:IsMacOS -or $global:IsLinux)) {
        Write-Host "`e[0Ksection_end:$(Get-UnixTimestamp):$($SectionName)`r`e[0K"
    }
}

<#
    .Description
    Indent-String splits a string based on newline separators, indents each line,
    and then returned the rejoined string.
#>
function Indent-String([string] $Value, [int] $Indent) {
    $local:Split = $local:Value.Replace("`r`n", "`n").Split("`n")
    $local:IndentStr = ""
    for ($local:i = 0; $local:i -lt $local:Indent; $local:i += 1) {
        $local:IndentStr += " "
    }
    for ($local:i = 0; $local:i -lt $local:Split.Length; $local:i += 1) {
        $local:Split[$local:i] = "$($local:IndentStr)$($local:Split[$local:i])"
    }
    return $local:Split -join "`n"
}

<#
    .Description
    Map-WorkingDirectory maps the given path to a virtual drive if 
    $MapDriveForShorterPaths is true, and then executes the script block
    with either the working directory or mapped virtual drive as appropriate.
#>
function Map-WorkingDirectory([switch][bool] $MapDriveForShorterPaths, [string] $Path, [ScriptBlock] $ScriptBlock) {
    if ($local:MapDriveForShorterPaths) {
        Get-ChildItem function:[d-z]: -n | ForEach-Object { subst $_ /D | Out-Null }
        $local:MappedDrive = (Get-ChildItem function:[d-z]: -n | Where-Object { !(Test-Path -ErrorAction SilentlyContinue $_) } | Select-Object -Last 1)
        try {
            Write-Output "Mapping working directory $local:Path to virtual drive $local:MappedDrive..."
            subst $local:MappedDrive $local:Path
            if (!(Test-Path $local:MappedDrive)) {
                Write-Error "Unable to map working directory to virtual drive!"
                exit 1
            }

            & $local:ScriptBlock -Path $local:MappedDrive
        } finally {
            Write-Output "Unmapping virtual drive $local:MappedDrive..."
            subst $local:MappedDrive /D
        }
    } else {
        & $local:ScriptBlock -Path $local:Path
    }
}