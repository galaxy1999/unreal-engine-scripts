param()

function Get-BuildGraphSettingsForEngine(
    $DistributionConfig,
    [string] $EnginePath,
    [bool] $Allow2019,
    [bool] $IsUnrealEngine5
) {
    # Compute platform parameters.
    taskkill /f /t /im AutomationToolLauncher.exe | Out-Null
    $BuildGraphOptions = (& "$PSScriptRoot\Internal_RunUAT.ps1" -UATEnginePath "$EnginePath" BuildGraph -Script="$EnginePath\Engine\Build\InstalledEngineBuild.xml" -ListOnly)
    $AvailablePlatforms = @()
    $AvailablePlatformsMac = @()
    $InstalledEngineBuild = (Get-Content -Raw -Path "$EnginePath\Engine\Build\InstalledEngineBuild.xml")
    foreach ($Line in $BuildGraphOptions) {
        $Line = $Line.Trim()
        if ($Line.StartsWith("-set:With") -and !$Line.StartsWith("-set:WithDDC") -and !$Line.StartsWith("-set:WithClient") -and !$Line.StartsWith("-set:WithServer") -and !$Line.StartsWith("-set:WithFullDebugInfo")) {
            $Line = $Line.Substring("-set:With".Length)
            $Line = $Line.Split("=")[0]
            $AvailablePlatforms += $Line
            if ($InstalledEngineBuild.Contains("<Option Name=`"With$Line`"")) {
                # macOS only knows about public (non-console) platforms.
                $AvailablePlatformsMac += $Line
            }
        }
    }
    $Platforms = @{}
    $PlatformsMac = @{}
    foreach ($Platform in $AvailablePlatforms) {
        $Platforms[$Platform] = $false
    }
    foreach ($Platform in $AvailablePlatformsMac) {
        $PlatformsMac[$Platform] = $false
    }
    foreach ($Platform in $DistributionConfig.Build.Platforms) {
        $Platforms[$Platform] = $true
        if ($PlatformsMac[$Platform] -ne $null) {
            $PlatformsMac[$Platform] = $true
        }
    }
    
    $BuildGraphSettingsWindows = @{
        # Target types
        WithClient = if ($DistributionConfig.Build.TargetTypes.Contains("Client")) { "true" } else { "false" };
        WithServer = if ($DistributionConfig.Build.TargetTypes.Contains("Server")) { "true" } else { "false" };

        # Cook options
        WithDDC    = if ($DistributionConfig.Cook.GenerateDDC) { "true" } else { "false" };
    }
    if (!$local:IsUnrealEngine5) {
        # Build options
        $BuildGraphSettingsWindows.VS2019 = if ($Allow2019) { "true" } else { "false" };
    }
    $Platforms.Keys | % {
        $BuildGraphSettingsWindows["With$_"] = if ($Platforms[$_]) { "true" } else { "false" }
    }
    $BuildGraphSettingsMac = @{
        # Target types
        WithClient = if ($DistributionConfig.Build.TargetTypes.Contains("Client")) { "true" } else { "false" };
        WithServer = if ($DistributionConfig.Build.TargetTypes.Contains("Server")) { "true" } else { "false" };

        # Cook options
        WithDDC    = if ($DistributionConfig.Cook.GenerateDDC) { "true" } else { "false" };
    }
    if (!$local:IsUnrealEngine5) {
        # Build options
        $BuildGraphSettingsMac.VS2019 = if ($Allow2019) { "true" } else { "false" };
    }
    $PlatformsMac.Keys | % {
        $BuildGraphSettingsMac["With$_"] = if ($PlatformsMac[$_]) { "true" } else { "false" }
    }

    return @{
        Windows = $BuildGraphSettingsWindows;
        Mac     = $BuildGraphSettingsMac;
    };
}