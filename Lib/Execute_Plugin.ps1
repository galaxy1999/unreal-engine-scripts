param(
    # The engine you are targeting. Can be a version number or an absolute path.
    [Parameter(Mandatory=$true)][string] $Engine,
    # The distribution name, as it appears in BuildConfig.json.
    [Parameter(Mandatory=$true)][string] $Distribution,
    # If true, executes the build step in the build graph.
    [switch][bool] $ExecuteBuild,
    # If true, executes the test steps in the build graph.
    [switch][bool] $ExecuteTests,
    # If true, executes the deployment steps in the build graph.
    [switch][bool] $ExecuteDeployment,
    # If true, turns on IWYU and -StrictIncludes. This is much more time consuming, but required to pass Marketplace submission.
    [switch][bool] $StrictIncludes,
    # If true, allows Visual Studio 2019 to be used to build plugins.
    [switch][bool] $Allow2019,
    # If true, the working directory will be mapped as a drive on Windows to reduce the length of file paths. Engine builds
    # always map drives, but you can optionally enable this for project or plugin builds.
    [switch][bool] $MapDriveForShorterPaths
)

. $PSScriptRoot\Internal_SetupEnvironment.ps1

$BuildConfig = Get-BuildConfig
$EnginePath = Resolve-EnginePath $Engine
$DistributionConfig = Get-Distribution $Distribution

# Set up common variables.
$PluginName = $BuildConfig.PluginName

# Compute BuildGraph settings.
. $PSScriptRoot\BuildGraphSettings_Plugin.ps1
$Settings = Get-BuildGraphSettingsForPlugin $PluginName $DistributionConfig $EnginePath $StrictIncludes $Allow2019 $ExecuteBuild $ExecuteTests $ExecuteDeployment $BuildConfig.Copyright

# Patch BuildGraph.
& "$PSScriptRoot\Patch_BuildGraph.ps1" -Engine "$Engine"
if ($LastExitCode -ne 0) {
    exit $LastExitCode
}

# BuildGraph in Unreal Engine 5.0 causes input files to be unnecessarily modified. Just allow mutation since I'm not sure what the bug is.
$env:BUILD_GRAPH_ALLOW_MUTATION="true"

# Set up environment variables (for generate, these need to be set as
# GitLab CI/CD variables).
$env:BUILDING_FOR_REDISTRIBUTION="true"
if ($DistributionConfig.EnvironmentVariables -ne $null) {
    foreach ($EnvKey in $DistributionConfig.EnvironmentVariables | Get-Member -MemberType NoteProperty | Select -ExpandProperty Name) {
        New-Item env:\$EnvKey -Value $DistributionConfig.EnvironmentVariables.$EnvKey
    }
}

# Run any relevant preparation scripts.
if ($null -ne $DistributionConfig.Prepare) {
    foreach ($Prepare in $DistributionConfig.Prepare) {
        if ($local:Prepare.Type -eq "Custom") {
            if ($null -ne $local:Prepare.RunBefore -and $local:Prepare.RunBefore.Contains("BuildGraph")) {
                powershell.exe -ExecutionPolicy Bypass "$((Get-Location).Path)\$($local:Prepare.ScriptPath.Replace("/", "\"))"
                if ($LastExitCode -ne 0) { exit $LastExitCode }
            }
        }
    }
}

# Execute BuildGraph.
$BuildGraphArgsArray = @()
$Settings.Keys | % {
    $BuildGraphArgsArray += "-set:$_=`"$($Settings.Item($_).Replace("__REPOSITORY_ROOT__", "$ProjectRoot").Replace("/", "\").TrimEnd("\"))`""
}
$env:uebp_LOCAL_ROOT = $EnginePath
$env:BUILD_GRAPH_PROJECT_ROOT = $ProjectRoot
taskkill /f /t /im AutomationToolLauncher.exe
# note: We don't yet have this retry logic on BuildGraph invocations that happen
# as part of a GitLab build, but I haven't seen PCH memory errors occur there. Port
# this logic across if we see these errors happening there as well.
foreach ($Value in $BuildGraphArgsArray) {
    Write-Host $Value
}
while ($true) {
    & "$PSScriptRoot\Internal_RunUAT.ps1" -UATEnginePath "$EnginePath" BuildGraph -Script="$PSScriptRoot\BuildGraph_Plugin.xml" -Target="End" "-set:EnginePath=`"$EnginePath`"" $BuildGraphArgsArray | Tee-Object -FilePath $ProjectRoot\BuildScripts\Temp\Build.log
    if ($LASTEXITCODE -ne 0) {
        $BuildLogLines = (Get-Content "$ProjectRoot\BuildScripts\Temp\Build.log")
        if ($BuildLogLines -is [string]) {
            $BuildLogLines = @($BuildLogLines)
        }
        $PCHRetryOn = $true
        $NeedsRetry = $false
        for ($i = 0; $i -lt $BuildLogLines.Length; $i++) {
            if ($BuildLogLines[$i].Contains("error C3859") -and $PCHRetryOn) {
                $NeedsRetry = $true
                break
            }
            if ($BuildLogLines[$i].Contains("@buildgraph PCH-RETRY-ON")) {
                $PCHRetryOn = $true
            }
            if ($BuildLogLines[$i].Contains("@buildgraph PCH-RETRY-OFF")) {
                $PCHRetryOn = $false
            }
        }
        if ($NeedsRetry) {
            Write-Warning "Detected PCH memory error. Automatically retrying..."
            continue
        }
        exit $LASTEXITCODE
    } else {
        break
    }
}

exit $LastExitCode
