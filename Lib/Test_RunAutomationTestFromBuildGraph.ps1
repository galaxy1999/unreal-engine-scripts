param(
    # The path to the engine.
    [Parameter(Mandatory=$true)][string] $EnginePath,
    # The test project to execute automation tests under.
    [Parameter(Mandatory=$true)][string] $TestProjectPath,
    # The automation test prefix.
    [Parameter(Mandatory=$true)][string] $TestPrefix,
    # The output path for the automation test results.
    [Parameter(Mandatory=$true)][string] $TestResultsPath,
    # The minimum number of workers.
    [Parameter(Mandatory=$true)][int] $MinWorkerCount,
    # The timeout in minutes.
    [Parameter(Mandatory=$true)][int] $TimeoutMinutes
)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Internal_SetupEnvironment.ps1

# Apply firewall rules to allow automation tests to access the network.
$CurrentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
if ($CurrentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)) {
    Write-Host "Allowing UE4Editor network access in Windows Firewall..."
    $AppsToAllowInFirewall = @(
        "$EnginePath\Engine\Binaries\Win64\UE4Editor-Cmd.exe",
        "$EnginePath\Engine\Binaries\DotNET\IOS\DeploymentServerLauncher.exe"
    )
    foreach ($AppPath in $AppsToAllowInFirewall) {
        $StringAsStream = [System.IO.MemoryStream]::new()
        $Writer = [System.IO.StreamWriter]::new($StringAsStream)
        $Writer.Write("$AppPath")
        $Writer.Flush()
        $StringAsStream.Position = 0
        $RuleHash =  Get-FileHash -InputStream $StringAsStream | Select-Object Hash
        $RuleName = "UE4Editor-Cmd-$RuleHash"
        netsh advfirewall firewall add rule name="$RuleName" dir=in action=allow program="$AppPath" enable=yes
    }
}

# Remove the old test results if they exist.
if (Test-Path $TestResultsPath) {
    Remove-Item -Force $TestResultsPath
}

$TestProjectRoot = (Resolve-Path "$TestProjectPath/../").Path
Push-Location "$TestProjectRoot"
try {
    & "$PSScriptRoot\Test_RunAutomationTestsInParallel.ps1" `
        -Engine $EnginePath `
        -ProjectRoot $TestProjectRoot `
        -ProjectPath $TestProjectPath `
        -MinWorkerCount $MinWorkerCount `
        -TimeoutMinutes $TimeoutMinutes `
        -TestPrefix $TestPrefix
    if ($LastExitCode -ne 0) {
        exit $LastExitCode
    }
} finally {
    Pop-Location
    Write-Output "Converting test results..."
    $FilenamePrefixToCut = (Resolve-Path "$TestProjectPath\..").Path
    $TestsAllPassed = & $PSScriptRoot\Test_ConvertUE4TestsToJUnitTests.ps1 `
        -Path "$TestProjectRoot/Intermediate/TestReport/index.json" `
        -Destination "$TestResultsPath" `
        -ProjectName "T" `
        -FilenamePrefixToCut "$FilenamePrefixToCut" `
        | select -Last 1
    if (!$TestsAllPassed) {
        Write-Output "One or more tests failed, so the script is exiting with exit code 1."
        exit 1
    }
}
exit 0