param(
    # The engine to patch.
    [Parameter(Mandatory=$true)][string] $Engine,
    # The distribution we are testing under.
    [Parameter(Mandatory=$true)][string] $Distribution,
    # The test we are running (as defined in BuildConfig.json).
    [Parameter(Mandatory=$true)][string] $TestName
)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Internal_SetupEnvironment.ps1

$BuildConfig = Get-BuildConfig
$EnginePath = Resolve-EnginePath $Engine
$DistributionConfig = Get-Distribution $Distribution

# Set up common variables.
$ProjectName = $DistributionConfig.ProjectName
$FolderName = $DistributionConfig.FolderName

# Find the Gauntlet test information.
$GauntletConfig = $null
foreach ($Test in $DistributionConfig.Tests) {
    if ($Test.Type -eq "Gauntlet" -and $Test.Name -eq "$TestName") {
        $GauntletConfig = $Test.Gauntlet
        break
    }
}
if ($GauntletConfig -eq $null) {
    Write-Error "Gauntlet tests is not defined in BuildConfig.json"
    exit 1
}

# Figure out the Gauntlet script name.
$ScriptPath = "$ProjectRoot\$($GauntletConfig.ScriptPath)"

# Get the script content.
$GauntletScriptContent = Get-Content -Raw $ScriptPath

# Mark engine files as read-write.
Get-ChildItem -Path "$EnginePath\Engine\Source\Programs" -Recurse -Filter "*" | % {
    if ($_.IsReadOnly) {
        $_.IsReadOnly = $false
    }
}

# Set the script content into our expected Gauntlet location.
Set-RetryableContent -Path "$EnginePath\Engine\Source\Programs\AutomationTool\Gauntlet\Unreal\Game\ExternalPluginGauntletTest.cs" -Value $GauntletScriptContent

# If this is Unreal Engine 5, we need to copy across fastJSON if it doesn't exist.
$IsUnrealEngine5 = $false
if ((Get-Content -Raw "$EnginePath\Engine\Source\Programs\AutomationTool\BuildGraph\BuildGraph.Automation.csproj").Contains("netcoreapp")) {
    Write-Host "Detected Unreal Engine 5..."
    if (!(Test-Path "$EnginePath\Engine\Binaries\ThirdParty\fastJSON\netstandard2.0")) {
        New-Item -ItemType Directory "$EnginePath\Engine\Binaries\ThirdParty\fastJSON\netstandard2.0"
    }
    if (!(Test-Path "$EnginePath\Engine\Binaries\ThirdParty\fastJSON\netstandard2.0\fastJSON.dll")) {
        Copy-Item -Force "$PSScriptRoot\UE5\fastJSON.dll" "$EnginePath\Engine\Binaries\ThirdParty\fastJSON\netstandard2.0\fastJSON.dll"
    }
    if (!(Test-Path "$EnginePath\Engine\Binaries\ThirdParty\fastJSON\netstandard2.0\fastJSON.deps.json")) {
        Copy-Item -Force "$PSScriptRoot\UE5\fastJSON.deps.json" "$EnginePath\Engine\Binaries\ThirdParty\fastJSON\netstandard2.0\fastJSON.deps.json"
    }
    if (!(Test-Path "$EnginePath\Engine\Binaries\DotNET")) {
        New-Item -ItemType Directory "$EnginePath\Engine\Binaries\DotNET"
    }
    if (!(Test-Path "$EnginePath\Engine\Binaries\DotNET\Ionic.Zip.Reduced.dll")) {
        Copy-Item -Force "$PSScriptRoot\UE5\Ionic.Zip.Reduced.dll" "$EnginePath\Engine\Binaries\DotNET\Ionic.Zip.Reduced.dll"
    }
    if (!(Test-Path "$EnginePath\Engine\Binaries\DotNET\OneSky.dll")) {
        Copy-Item -Force "$PSScriptRoot\UE5\OneSky.dll" "$EnginePath\Engine\Binaries\DotNET\OneSky.dll"
    }
    $IsUnrealEngine5 = $true
}

# Patch our file into the Gauntlet .csproj file.
$GauntletProjectPath = "$EnginePath\Engine\Source\Programs\AutomationTool\Gauntlet\Gauntlet.Automation.csproj"
if (Test-Path $GauntletProjectPath) {
    $GauntletProject = Get-Content -Raw -Path $GauntletProjectPath
    if (!$GauntletProject.Contains("ExternalPluginGauntletTest.cs")) {
        $GauntletProject = $GauntletProject.Replace(
            "<Compile Include=`"Unreal\Game\Samples\ElementalDemoTest.cs`" />", 
            "<Compile Include=`"Unreal\Game\Samples\ElementalDemoTest.cs`" /><Compile Include=`"Unreal\Game\ExternalPluginGauntletTest.cs`" />"
        )
        Set-RetryableContent -Path $GauntletProjectPath -Value $GauntletProject
    }
    if ($IsUnrealEngine5) {
        if (!$GauntletProject.Contains(";IS_UE5")) {
            $GauntletProject = $GauntletProject.Replace(
                "</DefineConstants>",
                ";IS_UE5</DefineConstants>"
            )
            Set-RetryableContent -Path $GauntletProjectPath -Value $GauntletProject
        }
    }
}

# We need to patch a file in Gauntlet, because it's not designed to handle listen servers, which require the presence of a ? character
# in the command line. By default, Gauntlet will replace ? characters with spaces.
$GauntletUnrealBuildSourcePath = "$EnginePath\Engine\Source\Programs\AutomationTool\Gauntlet\Unreal\BuildSource\Gauntlet.UnrealBuildSource.cs"
if (Test-Path $GauntletUnrealBuildSourcePath) {
    $GauntletUnrealBuildSource = Get-Content -Raw -Path $GauntletUnrealBuildSourcePath
    $Old = 'MatchCollection Matches = Regex.Matches(InCommandLine, "(?<option>\\-?[\\w\\d.:\\[\\]\\/\\\\]+)(=(?<value>(\"([^\"]*)\")|(\\S+)))?");'
    $New = 'MatchCollection Matches = Regex.Matches(InCommandLine, "(?<option>\\-?[\\w\\d.:\\[\\]\\/\\\\\\?]+)(=(?<value>(\"([^\"]*)\")|(\\S+)))?");'
    if (!$GauntletUnrealBuildSource.Contains($New)) {
        $GauntletUnrealBuildSource = $GauntletUnrealBuildSource.Replace($Old, $New)
        Set-RetryableContent -Path $GauntletUnrealBuildSourcePath -Value $GauntletUnrealBuildSource
        Write-Output "Patched Gauntlet.UnrealBuildSource.cs (regex check)"
    }

    $Old = 'IEnumerable<IBuild> GetMatchingBuilds(UnrealTargetRole InRole, UnrealTargetPlatform? InPlatform, UnrealTargetConfiguration InConfiguration, BuildFlags InFlags)'
    $New = @"
static UnrealTargetConfiguration DebugConf(UnrealTargetConfiguration Config)
        {
            if (Config == UnrealTargetConfiguration.DebugGame)
            {
                Config = UnrealTargetConfiguration.Debug;
            }
            return Config;
        }

        IEnumerable<IBuild> GetMatchingBuilds(UnrealTargetRole InRole, UnrealTargetPlatform? InPlatform, UnrealTargetConfiguration InConfiguration, BuildFlags InFlags)
"@
    if (!$GauntletUnrealBuildSource.Contains("UnrealTargetConfiguration DebugConf(UnrealTargetConfiguration Config)")) {
        $GauntletUnrealBuildSource = $GauntletUnrealBuildSource.Replace($Old, $New)
        Set-RetryableContent -Path $GauntletUnrealBuildSourcePath -Value $GauntletUnrealBuildSource
        Write-Output "Patched Gauntlet.UnrealBuildSource.cs (DebugConf)"
    }

    $Old = '&& B.Configuration == InConfiguration'
    $New = '&& DebugConf(B.Configuration) == DebugConf(InConfiguration)'
    if (!$GauntletUnrealBuildSource.Contains($New)) {
        $GauntletUnrealBuildSource = $GauntletUnrealBuildSource.Replace($Old, $New)
        Set-RetryableContent -Path $GauntletUnrealBuildSourcePath -Value $GauntletUnrealBuildSource
        Write-Output "Patched Gauntlet.UnrealBuildSource.cs (config compare)"
    }
}

# We need to patch the Gauntlet log parser, so that if it's passed null, it treats it as an empty log.
$GauntletLogParserPath = "$EnginePath\Engine\Source\Programs\AutomationTool\Gauntlet\Unreal\Utils\Gauntlet.UnrealLogParser.cs"
if (Test-Path $GauntletLogParserPath) {
    $GauntletLogParserSource = Get-Content -Raw -Path $GauntletLogParserPath
    $GauntletLogParserSourceNew = $GauntletLogParserSource.Replace(
        "Content = InContent.Replace(Environment.NewLine, `"\n`");",
        "Content = (InContent ?? string.Empty).Replace(Environment.NewLine, `"\n`");")
    if ($GauntletLogParserSourceNew -ne $GauntletLogParserSource) {
        Set-RetryableContent -Path $GauntletLogParserPath -Value $GauntletLogParserSourceNew
        Write-Output "Patched Gauntlet.UnrealLogParser.cs (replace)"
    }
}

# We need to patch Gauntlet's Android support, because it installs the OBB to the wrong location and doesn't set up permissions correctly
# when an application is targeting a minimum SDK of 23 or greater.
$GauntletTargetDeviceAndroidSourcePath = "$EnginePath\Engine\Source\Programs\AutomationTool\Gauntlet\Platform\Android\Gauntlet.TargetDeviceAndroid.cs"
if (Test-Path $GauntletTargetDeviceAndroidSourcePath) {
    $Modified = $false
    $GauntletTargetDeviceAndroidSource = Get-Content -Raw -Path $GauntletTargetDeviceAndroidSourcePath
    if ($GauntletTargetDeviceAndroidSource -ne $null) {
        $Old = 'string OBBRemoteDestination = string.Format("{0}/obb/{1}", StorageLocation, Build.AndroidPackageName);'
        $New = 'string OBBRemoteDestination = string.Format("{0}/Android/obb/{1}", StorageLocation, Build.AndroidPackageName);'
        if (!$GauntletTargetDeviceAndroidSource.Contains($New)) {
            $GauntletTargetDeviceAndroidSource = $GauntletTargetDeviceAndroidSource.Replace($Old, $New)
            $Modified = $true
        }
        $Old = 'List<string> Permissions = new List<string>{ "WRITE_EXTERNAL_STORAGE", "GET_ACCOUNTS", "RECORD_AUDIO" };'
        $New = 'List<string> Permissions = new List<string>{ "READ_EXTERNAL_STORAGE", "WRITE_EXTERNAL_STORAGE", "GET_ACCOUNTS", "RECORD_AUDIO" };'
        if (!$GauntletTargetDeviceAndroidSource.Contains($New)) {
            $GauntletTargetDeviceAndroidSource = $GauntletTargetDeviceAndroidSource.Replace($Old, $New)
            $Modified = $true
        }
        if ($Modified) {
            Set-RetryableContent -Path $GauntletTargetDeviceAndroidSourcePath -Value $GauntletTargetDeviceAndroidSource
            Write-Output "Patched Gauntlet.TargetDeviceAndroid.cs"
        }
    }
}
$GauntletAndroidBuildSourceSourcePath = "$EnginePath\Engine\Source\Programs\AutomationTool\Gauntlet\Platform\Android\Gauntlet.AndroidBuildSource.cs"
if (Test-Path $GauntletAndroidBuildSourceSourcePath) {
    $Modified = $false
    $GauntletAndroidBuildSourceSource = Get-Content -Raw -Path $GauntletAndroidBuildSourceSourcePath
    $Old = 'Dictionary<string, string> FilesToInstall = OBBMatches.Cast<Match>().ToDictionary(M => Path.Combine(AbsPath, M.Groups[1].ToString()), M => M.Groups[2].ToString());'
    $New = 'Dictionary<string, string> FilesToInstall = OBBMatches.Cast<Match>().ToDictionary(M => Path.Combine(AbsPath, M.Groups[1].ToString()), M => M.Groups[2].ToString().Replace("%STORAGE%/obb/", "%STORAGE%/Android/obb/"));'
    if (!$GauntletAndroidBuildSourceSource.Contains($New)) {
        $GauntletAndroidBuildSourceSource = $GauntletAndroidBuildSourceSource.Replace($Old, $New)
        $Modified = $true
    }
    if ($Modified) {
        Set-RetryableContent -Path $GauntletAndroidBuildSourceSourcePath -Value $GauntletAndroidBuildSourceSource
        Write-Output "Patched Gauntlet.AndroidBuildSource.cs"
    }
}

# We need to patch Gauntlet build sources that try to locate .uproject files, as the existing logic only works if your project is inline with
# the engine.
Get-ChildItem -Path "$EnginePath\Engine\Source\Programs\AutomationTool\Gauntlet\" -Filter "*.cs" -Recurse | % {
    if (Test-Path $_.FullName) {
        $SourceCode = Get-Content -Raw -Path ($_.FullName)
        if ($SourceCode -ne $null) {
            $Old = "DirectoryReference ProjectRoot = ProjectUtils.FindProjectFileFromName(InProjectName).Directory;"
            $New = "DirectoryReference ProjectRoot = new DirectoryReference(Path.Combine(InPath, `"..`", `"..`", `"..`"));"
            if (!$SourceCode.Contains($New)) {
                $OldSourceCode = $SourceCode
                $SourceCode = $SourceCode.Replace($Old, $New)
                if ($OldSourceCode -ne $SourceCode) {
                    Set-RetryableContent -Path $_.FullName -Value $SourceCode
                    Write-Output "Patched Gauntlet .uproject lookup in: $($_.FullName)"
                }
            }
        }
    }
}
if (Test-Path "$EnginePath\Engine\Platforms") {
    Get-ChildItem -Path "$EnginePath\Engine\Platforms" | % {
        $PlatformPath = $_.FullName
        if (Test-Path "$PlatformPath\Source\Programs\AutomationTool\Gauntlet\") {
            Get-ChildItem -Path "$PlatformPath\Source\Programs\AutomationTool\Gauntlet\" -Filter "*.cs" -Recurse | % {
                if (Test-Path $_.FullName) {
                    $SourceCode = Get-Content -Raw -Path ($_.FullName)
                    if ($SourceCode -ne $null) {
                        $Old = "DirectoryReference ProjectRoot = ProjectUtils.FindProjectFileFromName(InProjectName).Directory;"
                        $New = "DirectoryReference ProjectRoot = new DirectoryReference(Path.Combine(InPath, `"..`", `"..`", `"..`"));"
                        if (!$SourceCode.Contains($New)) {
                            $OldSourceCode = $SourceCode
                            $SourceCode = $SourceCode.Replace($Old, $New)
                            if ($OldSourceCode -ne $SourceCode) {
                                Set-RetryableContent -Path $_.FullName -Value $SourceCode
                                Write-Output "Patched Gauntlet .uproject lookup in: $($_.FullName)"
                            }
                        }
                    }
                }
            }
        }
    }
}

# Now rebuild Gauntlet.
$GauntletProject = "$EnginePath\Engine\Source\Programs\AutomationTool\Gauntlet\Gauntlet.Automation.csproj"
if ($global:IsMacOS -or $global:IsLinux) {
    $GauntletProject = $GauntletProject.Replace("\", "/")
}
$MSBuildPath = Get-MSBuildPath
if ($MSBuildPath -eq $null) {
    Write-Host "Unable to locate MSBuild!"
    exit 1
}
if ($IsUnrealEngine5) {
    Get-ChildItem -Path "$EnginePath\Engine\Source\Programs\Shared\EpicGames.Core" -Recurse -Filter "*" | % {
        if ($_.IsReadOnly) {
            $_.IsReadOnly = $false
        }
    }

    if (Test-Path "$env:ProgramFiles\dotnet\dotnet.exe") {
        $Sources = (& "$env:ProgramFiles\dotnet\dotnet.exe" nuget list source | Out-String)
        if (!$Sources.Contains("https://api.nuget.org/v3/index.json")) {
            # Make sure we have the NuGet source for restore.
            & "$env:ProgramFiles\dotnet\dotnet.exe" nuget add source -n nuget.org "https://api.nuget.org/v3/index.json"
        }
    }
    
    Write-Host "Restoring packages..."
    & "$MSBuildPath" "/nologo" "/verbosity:quiet" "$GauntletProject" "/property:Configuration=Development" "/property:Platform=AnyCPU" "/target:Restore"
    if ($LastExitCode -ne 0) {
        exit $LastExitCode
    }
}
& "$MSBuildPath" "/nologo" "/verbosity:quiet" "$GauntletProject" "/property:Configuration=Development" "/property:Platform=AnyCPU"
if ($LastExitCode -ne 0) {
    exit $LastExitCode
}
exit 0