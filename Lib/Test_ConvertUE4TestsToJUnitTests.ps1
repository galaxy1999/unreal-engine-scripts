param(
    # The project name, which will be removed from the start of test names.
    [Parameter(Mandatory=$true)][string] $ProjectName, 
    # Path to the Unreal Engine 4 test results JSON file.
    [Parameter(Mandatory=$true)][string] $Path, 
    # Destination for the JUnit XML file.
    [Parameter(Mandatory=$true)][string] $Destination, 
    # The directory path prefix to remove from file paths.
    [Parameter(Mandatory=$true)][string] $FilenamePrefixToCut
)

if (Test-Path $Path) {
    $TestResults = Get-Content -Path $Path -Raw | ConvertFrom-Json
} else {
    $TestResults = "{`"tests`":[],`"totalDuration`":0}" | ConvertFrom-Json
}

$Stats_TotalTests = $TestResults.tests.Length
$Stats_SkippedTests = 0
$Stats_FailedTests = 0
$Stats_TotalTimeSeconds = $TestResults.totalDuration
foreach ($TestCase in $TestResults.tests) {
    if ($TestCase.state -eq "Fail") {
        $Stats_FailedTests++
    }
    if ($TestCase.state -eq "NotRun") {
        $Stats_SkippedTests++
    }
}

$AnyFailures = $false

$XmlSettings = New-Object System.Xml.XmlWriterSettings
$XmlSettings.Indent = $true
$XmlSettings.IndentChars = "  "

if (Test-Path $Destination) {
    Remove-Item -Force $Destination
}
$XmlWriter = [System.XML.XMLWriter]::Create($Destination, $XmlSettings)

$XmlWriter.WriteStartDocument()

$XmlWriter.WriteStartElement("testsuites")

    $XmlWriter.WriteStartElement("testsuite")

        $XmlWriter.WriteAttributeString("name", $TestResults.clientDescriptor)
        $XmlWriter.WriteAttributeString("tests", $Stats_TotalTests.ToString())
        $XmlWriter.WriteAttributeString("skipped", $Stats_SkippedTests.ToString())
        $XmlWriter.WriteAttributeString("failures", $Stats_FailedTests.ToString())
        $XmlWriter.WriteAttributeString("errors", "0")
        $XmlWriter.WriteAttributeString("time", $Stats_TotalTimeSeconds.ToString())

        foreach ($TestCase in $TestResults.tests) {

            $XmlWriter.WriteStartElement("testcase")

                $TestClassName = ""
                $TestName = ""
                if ($TestCase.fullTestPath.EndsWith("." + $TestCase.testDisplayName)) {
                    $TestClassName = $TestCase.fullTestPath.Substring(0, $TestCase.fullTestPath.Length - $TestCase.testDisplayName.Length - 1)
                    $TestName = $TestCase.testDisplayName

                    if ($TestClassName.StartsWith($ProjectName + ".")) {
                        $TestClassName = $TestClassName.Substring($ProjectName.Length + 1)
                    }
                } else {
                    $TestClassName = ""
                    $TestName = $TestCase.fullTestPath
                }

                $XmlWriter.WriteAttributeString("classname", $TestClassName)
                $XmlWriter.WriteAttributeString("name", $TestName)

                $StdOut = ""

                foreach ($TestEntry in $TestCase.entries) {

                    $Filename = $TestEntry.filename
                    if ($Filename.ToLowerInvariant().StartsWith(($FilenamePrefixToCut + "\").ToLowerInvariant())) {
                        $Filename = $Filename.Substring($FilenamePrefixToCut.Length + 1)
                        $Filename = "$ProjectName/Source/$Filename"
                    } elseif ($Filename.Trim() -ne "") {
                        # Write-Warning "Source file '$($TestEntry.filename)' did not have expected prefix '$FilenamePrefixToCut\'"
                    }
                    $Filename = $Filename.Replace("\", "/")

                    if ($TestEntry.event.type -eq "Error") {

                        $AnyFailures = $true

                        $XmlWriter.WriteStartElement("failure")

                            $XmlWriter.WriteAttributeString("type", $TestEntry.event.type)
                            $XmlWriter.WriteAttributeString("message", $TestEntry.event.message)
                            if ($Filename -ne "" -or $TestEntry.lineNumber -gt -1) {
                                $XmlWriter.WriteString(@"
ERROR: $($TestEntry.event.message)
File: $Filename
Line: $($TestEntry.lineNumber)
"@)
                            } else {
                                $XmlWriter.WriteString(@"
ERROR: $($TestEntry.event.message)
No filename or line number is available.
"@)
                            }
            
                        $XmlWriter.WriteEndElement()

                    }

                    if ($TestEntry.event.type -eq "Warning") {

                        if ($Filename -ne "" -or $TestEntry.lineNumber -gt -1) {
                            $StdOut += @"
WARNING: $($TestEntry.event.message)
File: $Filename
Line: $($TestEntry.lineNumber)

"@
                        } else {
                            $StdOut += @"
WARNING: $($TestEntry.event.message)
No filename or line number is available.

"@
                        }

                    }

                }

                if ($StdOut.Trim().Length -gt 0) {
                    $XmlWriter.WriteStartElement("system-out")
                        $XmlWriter.WriteString($StdOut)
                    $XmlWriter.WriteEndElement()
                }

            $XmlWriter.WriteEndElement()

        }

    $XmlWriter.WriteEndElement()

$XmlWriter.WriteEndElement()

$XmlWriter.WriteEndDocument()
$XmlWriter.Flush()
$XmlWriter.Close()

# Return true if all tests passed.
return !$AnyFailures