param()

function Get-BuildGraphSettingsForPlugin(
    $PluginName,
    $DistributionConfig,
    [string] $EnginePath, 
    [bool] $StrictIncludes,
    [bool] $Allow2019,
    [bool] $ExecuteBuild,
    [bool] $ExecuteTests,
    [bool] $ExecuteDeployment,
    $CopyrightSettings
) {
    # Determine if the engine is Unreal Engine 5.
    $local:IsUnrealEngine5 = $false
    if ($global:IsMacOS -or $global:IsLinux) {
        $local:RunUAT = Get-Content -Raw -Path "$EnginePath/Engine/Build/BatchFiles/RunUAT.sh"
        if ($local:RunUAT.Contains("Unreal Engine AutomationTool setup script")) {
            $local:IsUnrealEngine5 = $true
        }
    } else {
        $local:RunUAT = Get-Content -Raw -Path "$EnginePath\Engine\Build\BatchFiles\RunUAT.bat"
        if ($local:RunUAT.Contains("SET_TURNKEY_VARIABLES")) {
            $local:IsUnrealEngine5 = $true
        }
    }

    # Determine build matrix.
    $local:EditorConfig = Compute-BuildGraphInputArgs $local:DistributionConfig "Editor" $local:IsUnrealEngine5
    $local:GameConfig = Compute-BuildGraphInputArgs $local:DistributionConfig "Game" $local:IsUnrealEngine5

    # Compute directories to clean.
    $local:CleanDirectories = @()
    if ($local:DistributionConfig.Clean -ne $null -and $local:DistributionConfig.Clean.Filespecs -ne $null) {
        foreach ($local:Path in $local:DistributionConfig.Clean.Filespecs) {
            $local:CleanDirectories += $local:Path
        }
    }
    $local:CleanDirectories = $local:CleanDirectories -join ";"

    # Compute prepare scripts.
    $local:PrepareCustomAssembleFinalizeScripts = @()
    $local:PrepareCustomCompileScripts = @()
    $local:PrepareCustomTestScripts = @()
    if ($local:DistributionConfig.Prepare -ne $null) {
        foreach ($local:Prepare in $local:DistributionConfig.Prepare) {
            if ($local:Prepare.Type -eq "Custom" -and $local:Prepare.RunBefore -ne $null) {
                if ($local:Prepare.RunBefore.Contains("AssembleFinalize")) {
                    $local:PrepareCustomAssembleFinalizeScripts += $local:Prepare.ScriptPath
                }
                if ($local:Prepare.RunBefore.Contains("Compile")) {
                    $local:PrepareCustomCompileScripts += $local:Prepare.ScriptPath
                }
                if ($local:Prepare.RunBefore.Contains("Test")) {
                    $local:PrepareCustomTestScripts += $local:Prepare.ScriptPath
                }
            }
        }
    }
    $local:PrepareCustomAssembleFinalizeScripts = $local:PrepareCustomAssembleFinalizeScripts -join ";"
    $local:PrepareCustomCompileScripts = $local:PrepareCustomCompileScripts -join ";"
    $local:PrepareCustomTestScripts = $local:PrepareCustomTestScripts -join ";"

    # Compute packaging settings.
    $local:IsForMarketplaceSubmission = "false"
    if ($local:DistributionConfig.Package -ne $null -and
        $local:DistributionConfig.Package.Marketplace) {
        $local:IsForMarketplaceSubmission = "true"
    }
    $local:VersionInfo = Compute-VersionNameAndNumber $local:EnginePath

    # Compute automation tests.
    $local:AutomationTests = @()
    if ($local:DistributionConfig.Tests -ne $null) {
        foreach ($local:Test in $local:DistributionConfig.Tests) {
            if ($local:Test.Type -eq "Automation" -and $local:Test.Automation -ne $null) {
                $local:MinWorkerCount = 4;
                $local:TimeoutMinutes = 5;
                if ($local:Test.Automation.MinWorkerCount -ne $null) {
                    $local:MinWorkerCount = $local:Test.Automation.MinWorkerCount
                }
                if ($local:Test.Automation.TimeoutMinutes -ne $null) {
                    $local:TimeoutMinutes = $local:Test.Automation.TimeoutMinutes
                }
                $local:AutomationTestValues = @(
                    $local:Test.Name.ToString(),
                    ($local:Test.Automation.Platforms -join ";").ToString(),
                    ($local:Test.Automation.ConfigFiles -join ";").ToString(),
                    $local:Test.Automation.TestPrefix.ToString(),
                    $local:MinWorkerCount.ToString(),
                    $local:TimeoutMinutes.ToString()
                )
                for ($i = 0; $i -lt $local:AutomationTestValues.Length; $i++) {
                    if ($local:AutomationTestValues[$i] -eq "") {
                        $local:AutomationTestValues[$i] = "__EMPTY__"
                    }
                }
                $local:AutomationTests += $local:AutomationTestValues -join "~"
            }
        }
    }

    # Compute custom tests.
    $local:CustomTests = @()
    if ($local:DistributionConfig.Tests -ne $null) {
        foreach ($local:Test in $local:DistributionConfig.Tests) {
            if ($local:Test.Type -eq "Custom" -and $local:Test.Custom -ne $null) {
                $local:TestAgainst = "TestProject"
                if ($local:Test.Custom.TestAgainst -ne $null) {
                    $local:TestAgainst = $local:Test.Custom.TestAgainst
                }
                $local:CustomTests += "$($local:Test.Name)~$($local:TestAgainst)~$($local:Test.Custom.ScriptPath)"
            }
        }
    }

    # Compute deployment tasks.
    $local:DeploymentBackblazeB2 = @()
    if ($local:ExecuteDeployment -and $local:DistributionConfig.Deployment -ne $null) {
        foreach ($local:Deploy in $local:DistributionConfig.Deployment) {
            if ($local:Deploy.Type -eq "BackblazeB2" -and $local:Deploy.BackblazeB2 -ne $null) {
                $local:ManualDeploy = "false";
                if ($local:Deploy.Manual) {
                    $local:ManualDeploy = "true";
                }
                $local:DeploymentBackblazeB2 += "$($local:Deploy.Name);$($local:ManualDeploy);$($local:Deploy.BackblazeB2.BucketName);$($local:Deploy.BackblazeB2.FolderPrefixEnvVar)"
            }
        }
    }

    # Compute copyright header.
    $local:CopyrightHeader = ""
    $local:CopyrightExcludes = ""
    if ($local:IsForMarketplaceSubmission) {
        if ($null -eq $CopyrightSettings) {
            Write-Error "You must configure the 'Copyright' section in BuildConfig.json to package for the Marketplace."
        } elseif ($null -eq $CopyrightSettings.Header) {
            Write-Error "You must configure the 'Copyright.Header' value in BuildConfig.json to package for the Marketplace."
        } elseif (!$CopyrightSettings.Header.Contains("%Y")) {
            Write-Error "The configured copyright header must have a %Y placeholder for the current year to package for the Marketplace."
        } else {
            $local:CopyrightHeader = $CopyrightSettings.Header.Replace("%Y", (Get-Date).Year.ToString())
            if ($null -ne $CopyrightSettings.ExcludePaths) {
                $local:CopyrightExcludes = $CopyrightSettings.ExcludePaths -join ";"
            }
        }
    }

    # Compute final settings for BuildGraph.
    return @{
        # Build environment
        BuildScriptsPath = "__REPOSITORY_ROOT__/BuildScripts";
        BuildScriptsLibPath = "__REPOSITORY_ROOT__/BuildScripts/Lib";
        TempPath = "__REPOSITORY_ROOT__/BuildScripts/Temp";
        ProjectRoot = "__REPOSITORY_ROOT__";
        PluginDirectory = "__REPOSITORY_ROOT__/$($local:PluginName)";
        PluginName = $local:PluginName;
        Distribution = $local:DistributionConfig.Name;

        # Clean options
        CleanDirectories = $local:CleanDirectories;

        # Prepare options
        PrepareCustomAssembleFinalizeScripts = $local:PrepareCustomAssembleFinalizeScripts;
        PrepareCustomCompileScripts = $local:PrepareCustomCompileScripts;
        PrepareCustomTestScripts = $local:PrepareCustomTestScripts;

        # Build options
        ExecuteBuild = if ($local:ExecuteBuild) { "true" } else { "false" };
        EditorTargetPlatforms = $local:EditorConfig.TargetPlatforms;
        GameTargetPlatforms = $local:GameConfig.TargetPlatforms;
        GameConfigurations = $local:GameConfig.Configurations;
        # We can not build plugins for Mac/iOS from Windows, because the rsync remoting
        # feature in Unreal doesn't copy the right files back for plugin compilation to
        # work. Lock this to Mac;IOS so that those platforms get excluded if you're
        # building locally on Windows.
        MacPlatforms = "Mac;IOS";
        StrictIncludes = if ($local:StrictIncludes) { "true" } else { "false" };
        Allow2019 = if ($local:Allow2019) { "true" } else { "false" };
        EnginePrefix = (Get-EnginePrefix $EnginePath);

        # Package options
        VersionNumber = $local:VersionInfo.VersionNumber;
        VersionName = $local:VersionInfo.VersionName;
        PackageFolder = if ($local:DistributionConfig.Package -ne $null -and $local:DistributionConfig.Package.OutputFolderName -ne $null) {
            $local:DistributionConfig.Package.OutputFolderName
        } else {
            "Packaged"
        };
        PackageInclude = (Get-FilterInclude $local:DistributionConfig);
        PackageExclude = (Get-FilterExclude $local:DistributionConfig);
        IsForMarketplaceSubmission = $local:IsForMarketplaceSubmission;
        CopyrightHeader = $local:CopyrightHeader;
        CopyrightExcludes = $local:CopyrightExcludes;

        # Test options
        ExecuteTests = if ($local:ExecuteTests) { "true" } else { "false" };
        AutomationTests = ($local:AutomationTests -join "+");
        CustomTests = ($local:CustomTests -join "+");

        # Deploy options
        DeploymentBackblazeB2 = ($local:DeploymentBackblazeB2 -join "+");
    }
}