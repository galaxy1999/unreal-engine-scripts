#
# Optimized Git clone/fetch/checkout for BuildGraph jobs on GitLab CI/CD
# Tries to reduce the amount of operations we do to an absolute minimum based
# on what we know about the layout of Unreal Engine projects and plugins.
#
param(
    # The directory to clone to.
    [Parameter(Mandatory = $true)][string] $TargetDirectory,
    # The folder name for the project. This *only* applies to project clones, not plugins.
    [Parameter(Mandatory = $false)][string] $ProjectFolderName,
    # The URL override. This script is also used to clone the engine from GitHub.
    [Parameter(Mandatory = $false)][string] $SourceUrlOverride,
    # The ref override. This script is also used to clone the engine from GitHub.
    [Parameter(Mandatory = $false)][string] $RefOverride,
    # If true, this is an engine build. The script will not nuke or clean non-sensitive 
    # directories in engine builds, since it's assumed all directories are tracked
    # by the engine repository or are console files.
    [switch][bool] $IsEngineBuild
)

$ErrorActionPreference = "Stop"

function Get-UnixTimestamp() {
    return [int][double]::Parse((Get-Date -UFormat %s))
}

function Start-Section([string] $SectionName, [string] $HeaderDescription) {
    if ($env:CI -eq "true" -and ($global:IsMacOS -or $global:IsLinux)) {
        Write-Host "`e[0Ksection_start:$(Get-UnixTimestamp):$($SectionName)[collapsed=true]`r`e[0K$($HeaderDescription)"
    }
    else {
        Write-Host ">> $HeaderDescription"
    }
}

function Stop-Section([string] $SectionName) {
    if ($env:CI -eq "true" -and ($global:IsMacOS -or $global:IsLinux)) {
        Write-Host "`e[0Ksection_end:$(Get-UnixTimestamp):$($SectionName)`r`e[0K"
    }
}

# Functions for submodule processing.
function Parse-Submodules([string] $GitRepository) {
    if (!(Test-Path "$GitRepository\.gitmodules")) {
        # No submodules.
        return @()
    }
    $Results = @()
    $SubmoduleGroupName = ""
    $SubmodulePath = ""
    $SubmoduleUrl = ""
    $SubmoduleExcludeOnMac = ""
    foreach ($Line in (Get-Content "$GitRepository\.gitmodules")) {
        if ($Line.StartsWith("[submodule `"")) {
            if ($SubmoduleGroupName -ne "" -and $SubmodulePath -ne "" -and $SubmoduleUrl -ne "") {
                $Results += @{
                    Id           = $SubmoduleGroupName;
                    Path         = $SubmodulePath;
                    Url          = $SubmoduleUrl;
                    ExcludeOnMac = $SubmoduleExcludeOnMac;
                }
            }
            $SubmoduleGroupName = $Line.Substring("[submodule `"".Length)
            $SubmoduleGroupName = $SubmoduleGroupName.Substring(0, $SubmoduleGroupName.Length - 2)
            $SubmodulePath = ""
            $SubmoduleUrl = ""
            $SubmoduleExcludeOnMac = ""
        }
        elseif ($Line.Trim().StartsWith("path = ") -and $SubmoduleGroupName -ne "") {
            $SubmodulePath = $Line.Trim().Substring("path = ".Length).Trim()
        }
        elseif ($Line.Trim().StartsWith("url = ") -and $SubmoduleGroupName -ne "") {
            $SubmoduleUrl = $Line.Trim().Substring("url = ".Length).Trim()
        }
        elseif ($Line.Trim().StartsWith("exclude-on-mac = ") -and $SubmoduleGroupName -ne "") {
            $SubmoduleExcludeOnMac = $Line.Trim().Substring("exclude-on-mac = ".Length).Trim()
        }
    }
    if ($SubmoduleGroupName -ne "" -and $SubmodulePath -ne "" -and $SubmoduleUrl -ne "") {
        # If changing this, also update the one in the loop!
        $Results += @{
            Id           = $SubmoduleGroupName;
            Path         = $SubmodulePath;
            Url          = $SubmoduleUrl;
            ExcludeOnMac = $SubmoduleExcludeOnMac;
        }
    }
    return $Results
}

function Compute-StringHash([string] $InputString) {
    $StringAsStream = [System.IO.MemoryStream]::new()
    $Writer = [System.IO.StreamWriter]::new($StringAsStream)
    $Writer.write($InputString)
    $Writer.Flush()
    $StringAsStream.Position = 0
    return (Get-FileHash -InputStream $StringAsStream | Select-Object Hash).Hash
}

function Checkout-Submodule([string] $ParentDirectory, $SubmoduleInfo, $GitModulesPath) {
    $SubmodulePathHash = (Compute-StringHash "$ParentDirectory/$($SubmoduleInfo.Path)")

    # Write-Host "DEBUG: Checking submodule path $ParentDirectory/$($SubmoduleInfo.Path)..."

    # Initialize the submodule if needed.
    if (!(Test-Path "$ParentDirectory/$($SubmoduleInfo.Path)/.git")) {
        if (!(Test-Path "$ParentDirectory/$($SubmoduleInfo.Path)")) {
            New-Item -ItemType Directory -Path "$ParentDirectory/$($SubmoduleInfo.Path)"
        }
        if (Test-Path $GitModulesPath) {
            Push-Location "$ParentDirectory/$($SubmoduleInfo.Path)"
            try {
                $local:LinkedPath = (Resolve-Path -Relative -Path $GitModulesPath).Replace("\", "/")
                Start-Section "git-$SubmodulePathHash-init" "Attaching submodule $($SubmoduleInfo.Path) to $local:LinkedPath..."
                Set-Content -Path "$ParentDirectory/$($SubmoduleInfo.Path)/.git" -Value "gitdir: $local:LinkedPath"
            } finally {
                Pop-Location
            }
        } else {
            Start-Section "git-$SubmodulePathHash-init" "Initializing submodule $($SubmoduleInfo.Path)..."
            try {
                git -C "$ParentDirectory/$($SubmoduleInfo.Path)" init
                if ($LastExitCode -ne 0) {
                    Write-Output "git init failed with exit code: $LastExitCode"
                    exit $LastExitCode
                }
            }
            finally {
                Stop-Section "git-$SubmodulePathHash-init"
            }
        }
    } elseif (Test-Path "$ParentDirectory/$($SubmoduleInfo.Path)/.git" -PathType Container) {
        Start-Section "git-$SubmodulePathHash-absorb" "Absorbing submodule $($SubmoduleInfo.Path)..."
        try {
            git -C "$ParentDirectory" submodule absorbgitdirs -- "$($SubmoduleInfo.Path)"
            if ($LastExitCode -ne 0) {
                Write-Output "git submodule absorbgitdirs failed with exit code: $LastExitCode"
                exit $LastExitCode
            }
        }
        finally {
            Stop-Section "git-$SubmodulePathHash-absorb"
        }
    }

    # Compute the commit and URL for this submodule.
    $SubmoduleStatus = (git -C "$ParentDirectory" ls-tree -l HEAD "$($SubmoduleInfo.Path)")
    if ($LastExitCode -ne 0) {
        Write-Output "git ls-tree failed with exit code: $LastExitCode"
        exit $LastExitCode
    }
    $SubmoduleCommit = $SubmoduleStatus.Split(" ")[2].Trim()
    $SubmoduleUrl = $SubmoduleInfo.Url
    if ($SubmoduleUrl.StartsWith("../../")) {
        # Relative to GitLab server.
        if ($env:CI_JOB_TOKEN -ne $null) {
            $SubmoduleUrl = "https://gitlab-ci-token:$($env:CI_JOB_TOKEN)@$($env:CI_SERVER_HOST)/$($SubmoduleUrl.Substring("../../".Length))"
        }
        else {
            $SubmoduleUrl = "ssh://git@$($env:CI_SERVER_HOST)/$($SubmoduleUrl.Substring("../../".Length))"
        }
    }
    $SubmoduleDirectory = "$ParentDirectory/$($SubmoduleInfo.Path)"

    # -C doesn't look like it works properly for submodules...
    $CurrentHead = ""
    Push-Location "$SubmoduleDirectory"
    try {
        # Check if we've already got the commit checked out. If we do, do nothing.
        $ErrorActionPreference = "Continue"
        $CurrentHead = (git rev-parse HEAD 2> $null)
        $ErrorActionPreference = "Stop"
    }
    finally {
        Pop-Location
    }
    if ($CurrentHead -eq "$SubmoduleCommit") {
        # We have our own .gitcheckout file which we write after we finish all submodule work. That way,
        # we know the previous checkout completed successfully even if it failed during submodule work.
        if (Test-Path "$SubmoduleDirectory\.gitcheckout") {
            $LastSubmoduleCommit = (Get-Content -Raw "$SubmoduleDirectory\.gitcheckout").Trim()
            if ($LastSubmoduleCommit -eq "$SubmoduleCommit") {
                Write-Host "Git submodule $SubmoduleDirectory already up-to-date."
                return
            }
            else {
                Write-Host "Submodule needs checkout because '$LastSubmoduleCommit' (.gitcheckout) != '$SubmoduleCommit'"
            }
        }
        else {
            Write-Host "Submodule needs checkout because this file doesn't exist: $SubmoduleDirectory\.gitcheckout"
        }
    }
    else {
        Write-Host "Submodule needs checkout because '$CurrentHead' != '$SubmoduleCommit'"
    }

    Write-Host "Submodule commit: $SubmoduleCommit"
    Write-Host "Submodule URL: $SubmoduleUrl"
    Write-Host "Submodule directory: $SubmoduleDirectory"
    Write-Host "Submodule exclude on Mac: $($SubmoduleInfo.ExcludeOnMac)"
    
    Push-Location "$SubmoduleDirectory"
    try {
        # Check if we already have the target commit in history. If we do, skip fetch.
        $ErrorActionPreference = "Continue"
        $GitType = (git cat-file -t "$SubmoduleCommit" 2> $null)
        $ErrorActionPreference = "Stop"
        if ($GitType -ne "commit") {
            # Fetch the commit that we need.
            Start-Section "git-$SubmodulePathHash-fetch" "Fetching submodule $($SubmoduleInfo.Path) from remote server..."
            try {
                git fetch -f --recurse-submodules=no $SubmoduleUrl "$($SubmoduleCommit):FETCH_HEAD"
                if ($LastExitCode -ne 0) {
                    Write-Output "git fetch failed with exit code: $LastExitCode"
                    exit $LastExitCode
                }
            }
            finally {
                Stop-Section "git-$SubmodulePathHash-fetch"
            }

            # We don't fetch Git LFS in submodules. Git LFS should only be used in project Content folders,
            # and projects won't have other projects inside submodules.
        }

        # Checkout the target commit.
        Start-Section "git-$SubmodulePathHash-checkout" "Checking out submodule $($SubmoduleInfo.Path) target commit $($SubmoduleCommit)..."
        try {
            git -c "advice.detachedHead=false" checkout -f "$($SubmoduleCommit)"
            if ($LastExitCode -ne 0) {
                Write-Output "git checkout failed with exit code: $LastExitCode"
                exit $LastExitCode
            }
        }
        finally {
            Stop-Section "git-$SubmodulePathHash-checkout"
        }
    }
    finally {
        Pop-Location
    }

    # Write our .gitcheckout file which tells subsequent calls that we're up-to-date.
    Set-Content -Value "$SubmoduleCommit" -Path "$SubmoduleDirectory\.gitcheckout"
}

# Initialize the Git repository if needed.
if (!(Test-Path $TargetDirectory)) {
    New-Item -ItemType Directory $TargetDirectory | Out-Null
}
if (!(Test-Path "$TargetDirectory\.git")) {
    Start-Section "git-init" "Initializing Git repository because it doesn't already exist..."
    try {
        git init "$TargetDirectory"
        if ($LastExitCode -ne 0) {
            Write-Output "git init failed with exit code: $LastExitCode"
            exit $LastExitCode
        }
    }
    finally {
        Stop-Section "git-init"
    }
}

$TargetCommit = $env:CI_COMMIT_SHA
if ($null -ne $env:OVERRIDE_COMMIT -and $env:OVERRIDE_COMMIT -ne "") {
    $TargetCommit = $env:OVERRIDE_COMMIT
}
$TargetIsPotentialAnnotatedTag = $false
if ($RefOverride -ne $null -and $RefOverride.Trim() -ne "") {
    if ($SourceUrlOverride -eq $null -or $SourceUrlOverride.Trim() -eq "") {
        Write-Error "Expected -SourceUrlOverride if -RefOverride was set."
        exit 1
    }

    # Check if we need to resolve the ref.
    if (!("$RefOverride" -match "^[a-f0-9]{40}$")) {
        Start-Section "git-remote-resolve" "Resolving ref '$RefOverride' to commit on remote Git server..."
        $ResolvedRef = (git ls-remote --exit-code "$SourceUrlOverride" "$RefOverride")
        if ($LastExitCode -ne 0) {
            Write-Error "Could not resolve remote ref '$RefOverride'."
            exit 1
        }
        if ($ResolvedRef -eq $null -or $ResolvedRef.Trim() -eq "") {
            Write-Error "Could not resolve remote ref '$RefOverride'."
            exit 1
        }
        if ($ResolvedRef -is [string]) {
            $ResolvedRef = @($ResolvedRef)
        }
        foreach ($ResolvedLine in $ResolvedRef) {
            $ResolvedRef = $ResolvedLine.Replace("`t", " ").Split(" ")[0]
            if ($ResolvedLine.Contains("refs/tags/")) {
                $TargetIsPotentialAnnotatedTag = $true
            }
            break
        }
        if (!($ResolvedRef.Trim() -match "^[a-f0-9]{40}$")) {
            Write-Error "Ref '$RefOverride' resolved to non-SHA1 hash '$($ResolvedRef.Trim())'."
            exit 1
        }
        $RefOverride = $ResolvedRef.Trim()
        Stop-Section "git-remote-resolve"
    }
    else {
        $RefOverride = $RefOverride.Trim()
    }
    $TargetCommit = $RefOverride
}

# Check if we've already got the commit checked out. If we do, do nothing.
$ErrorActionPreference = "Continue"
$CurrentHead = (git -C "$TargetDirectory" rev-parse HEAD 2> $null)
$ErrorActionPreference = "Stop"
if ($CurrentHead -eq "$TargetCommit") {
    # We have our own .gitcheckout file which we write after we finish all submodule work. That way,
    # we know the previous checkout completed successfully even if it failed during submodule work.
    if (Test-Path "$TargetDirectory\.gitcheckout") {
        if ((Get-Content -Raw "$TargetDirectory\.gitcheckout").Trim() -eq "$TargetCommit") {

            # Just really quickly check to make sure the .git file exists in each submodule we care about. If it doesn't,
            # then the .gitcheckout file is stale and needs to be removed.
            $ValidSubmoduleLayout = $true
            Start-Section "git-submodules-quick" "Quickly checking submodules..."
            try {
                foreach ($TopLevelSubmodule in (Parse-Submodules "$TargetDirectory")) {
                    if ($TopLevelSubmodule.ExcludeOnMac.Trim() -eq "true" -and $global:IsMacOS) {
                        continue
                    }
                    if ($TopLevelSubmodule.Path -eq "BuildScripts" -or $TopLevelSubmodule.Path.Contains("/Source/") -or ($ProjectFolderName -ne $null -and $ProjectFolderName -ne "" -and $TopLevelSubmodule.Path.StartsWith("$ProjectFolderName/Plugins/"))) {
                        if (!(Test-Path "$TargetDirectory/$($TopLevelSubmodule.Path)/.git")) {
                            Write-Host "Missing .git file at $TargetDirectory/$($TopLevelSubmodule.Path)/.git, purging .gitcheckout..."
                            $ValidSubmoduleLayout = $false
                            break
                        }
                        if ($ProjectFolderName -ne $null -and $ProjectFolderName -ne "" -and $TopLevelSubmodule.Path.StartsWith("$ProjectFolderName/Plugins/")) {
                            foreach ($ChildSubmodule in (Parse-Submodules "$TargetDirectory/$($TopLevelSubmodule.Path)")) {
                                if ($ChildSubmodule.ExcludeOnMac.Trim() -eq "true" -and $global:IsMacOS) {
                                    continue
                                }
                                if ($ChildSubmodule.Path.Contains("/Source/")) {
                                    if (!(Test-Path "$TargetDirectory/$($TopLevelSubmodule.Path)/$($ChildSubmodule.Path)/.git")) {
                                        Write-Host "Missing .git file at $TargetDirectory/$($TopLevelSubmodule.Path)/$($ChildSubmodule.Path)/.git, purging .gitcheckout..."
                                        $ValidSubmoduleLayout = $false
                                        break
                                    }
                                }
                            }
                            if (!$ValidSubmoduleLayout) {
                                break
                            }
                        }
                    }
                }
            }
            finally {
                Stop-Section "git-submodules-quick"
            }
            
            if ($ValidSubmoduleLayout) {
                Write-Host "Git repository already up-to-date."
                exit 0
            }
            else {
                Remove-Item -Force "$TargetDirectory\.gitcheckout"
                # Continue with full process...
            }
        }
    }
}

# Compute source URL.
$Url = "ssh://git@$($env:CI_SERVER_HOST)/$($env:CI_PROJECT_PATH).git"
if ($env:CI_JOB_TOKEN -ne $null) {
    $Url = "https://gitlab-ci-token:$($env:CI_JOB_TOKEN)@$($env:CI_SERVER_HOST)/$($env:CI_PROJECT_PATH).git"
}
if ($SourceUrlOverride -ne $null -and $SourceUrlOverride.Trim() -ne "") {
    $Url = $SourceUrlOverride
}

# Check if we already have the target commit in history. If we do, skip fetch.
$ErrorActionPreference = "Continue"
$GitType = (git -C "$TargetDirectory" cat-file -t "$($TargetCommit)" 2> $null)
$ErrorActionPreference = "Stop"

# If we know this is an annotated commit, resolve which commit it points to.
if ($GitType -eq "tag") {
    $ErrorActionPreference = "Continue"
    $TargetCommit = (git -C "$TargetDirectory" rev-list -n 1 "$($TargetCommit)" 2> $null).Trim()
    $GitType = (git -C "$TargetDirectory" cat-file -t "$($TargetCommit)" 2> $null)
    $ErrorActionPreference = "Stop"
}

# If we couldn't resolve the reference, we don't have the commit.
if ($GitType -ne "commit") {
    # Fetch the commit that we need.
    Start-Section "git-fetch" "Fetching repository from remote server..."
    try {
        $DepthParam = ""
        if ($SourceUrlOverride -ne $null -and $SourceUrlOverride.Trim() -ne "") {
            $DepthParam = "--depth=1"
        }
        if ($TargetIsPotentialAnnotatedTag) {
            git -C "$TargetDirectory" fetch $DepthParam -f --recurse-submodules=no $Url "$($TargetCommit)"
            # Now that we've fetched the potential tag, check if it really is a tag. If it is, resolve it to the commit hash instead.
            $GitType = (git -C "$TargetDirectory" cat-file -t "$($TargetCommit)" 2> $null)
            if ($GitType -eq "tag") {
                $TargetCommit = (git -C "$TargetDirectory" rev-list -n 1 "$($TargetCommit)" 2> $null).Trim()
            }
        }
        else {
            git -C "$TargetDirectory" fetch $DepthParam -f --recurse-submodules=no $Url "$($TargetCommit):FETCH_HEAD"
        }
        if ($LastExitCode -ne 0) {
            Write-Output "git fetch failed with exit code: $LastExitCode"
            exit $LastExitCode
        }
    }
    finally {
        Stop-Section "git-fetch"
    }

    # Fetch the LFS files as well.
    Start-Section "git-lfs-fetch" "Fetching LFS files from remote server..."
    try {
        git -C "$TargetDirectory" lfs fetch "$Url" "$($TargetCommit)"
        if ($LastExitCode -ne 0) {
            Write-Output "git lfs fetch failed with exit code: $LastExitCode"
            exit $LastExitCode
        }
    }
    finally {
        Stop-Section "git-lfs-fetch"
    }
}

# Checkout the target commit.
Start-Section "git-checkout" "Checking out target commit $($TargetCommit)..."
try {
    git -C "$TargetDirectory" -c "advice.detachedHead=false" checkout -f "$($TargetCommit)"
    if ($LastExitCode -ne 0) {
        # Attempt to re-fetch LFS files, in case that was the error.
        Start-Section "git-lfs-fetch" "Fetching LFS files from remote server..."
        try {
            git -C "$TargetDirectory" lfs fetch "$Url" "$($TargetCommit)"
            if ($LastExitCode -ne 0) {
                Write-Output "git lfs fetch failed with exit code: $LastExitCode"
                exit $LastExitCode
            }
        }
        finally {
            Stop-Section "git-lfs-fetch"
        }
        
        # Re-attempt checkout...
        git -C "$TargetDirectory" -c "advice.detachedHead=false" checkout -f "$($TargetCommit)"
        if ($LastExitCode -ne 0) {
            Write-Output "git checkout failed with exit code: $LastExitCode"
            exit $LastExitCode
        }
    }
}
finally {
    Stop-Section "git-checkout"
}

function Get-GitBaseDirectoryForPath([string] $FilePath) {
    $local:DirectoryPath = [System.IO.Path]::GetDirectoryName($FilePath)
    if (Test-Path "$local:DirectoryPath\.git") {
        return $local:DirectoryPath
    }
    return (Get-GitBaseDirectoryForPath $local:DirectoryPath)
}

# Clean all Source, Config, Resources and Content folders so that we don't have stale files
# accidentally included in build steps.
Start-Section "git-source-clean" "Cleaning build sensitive directories..."
if (!$IsEngineBuild) {
    $SensitiveDirectories = @("Source", "Config", "Resources", "Content")
    $local:TargetDirectoryFull = (Get-Item "$TargetDirectory").FullName
    $CleanFiles = @()
    foreach ($CleanFile in Get-ChildItem -Filter "*.uproject" -Recurse -Path "$TargetDirectory") {
        $CleanFiles += $CleanFile
    }
    foreach ($CleanFile in Get-ChildItem -Filter "*.uplugin" -Recurse -Path "$TargetDirectory") {
        $CleanFiles += $CleanFile
    }
    foreach ($CleanFile in $CleanFiles) {
        if ($null -eq $CleanFile) {
            continue
        }
        $local:BasePath = Get-GitBaseDirectoryForPath $CleanFile.FullName
        $local:BasePathFull = (Get-Item $local:BasePath).FullName
        $local:FolderRelativePath = $CleanFile.Directory.FullName.Substring($local:BasePathFull.Length).Trim('/').Trim('\').Replace("\", "/")
        $local:BasePathRelativePath = $local:BasePathFull.Substring($local:TargetDirectoryFull.Length).Trim('/').Trim('\').Replace("\", "/")
        if ($local:BasePathRelativePath.Length -eq 0) {
            $local:BasePathRelativePath = "."
        }
        $local:DidSucceed = $true
        try {
            git -C "$local:BasePathFull" ls-files --error-unmatch "$local:FolderRelativePath/$($CleanFile.Name)" 2>&1 | Out-Null
        } catch {
            $local:DidSucceed = $false
        }
        if (!$local:DidSucceed -or $LastExitCode -ne 0) {
            # This is not a tracked project/plugin, nuke the build sensitive folders directly and expect BuildGraph to either
            # populate or download them.
            foreach ($SensitiveDirectory in $SensitiveDirectories) {
                if (Test-Path "$($CleanFile.Directory.FullName)\$SensitiveDirectory") {
                    Write-Output "Nuking: ($local:BasePathRelativePath) $local:FolderRelativePath/$SensitiveDirectory"
                    Remove-Item -Force -Recurse "$($CleanFile.Directory.FullName)\$SensitiveDirectory"
                }
            }
        }
        else {
            # This is a tracked project/plugin, use git clean to clear out unwanted files.
            foreach ($SensitiveDirectory in $SensitiveDirectories) {
                if (Test-Path "$($CleanFile.Directory.FullName)\$SensitiveDirectory") {
                    Write-Output "Cleaning: ($local:BasePathRelativePath) $local:FolderRelativePath/$SensitiveDirectory"
                    try {
                        git -C "$local:BasePathFull" clean -xdff "$local:FolderRelativePath/$SensitiveDirectory" 2>&1 | Out-Null
                    } catch {
                    }
                }
            }
        }
    }
}
Stop-Section "git-source-clean"

# Process the submodules, only checking out submodules that are either BuildScripts or
# sit underneath the target directory for compilation.
Start-Section "git-submodules" "Updating submodules..."
try {
    foreach ($TopLevelSubmodule in (Parse-Submodules "$TargetDirectory")) {
        if ($TopLevelSubmodule.ExcludeOnMac.Trim() -eq "true" -and $global:IsMacOS) {
            continue
        }
        # Write-Host "DEBUG: Considering submodule path: $($TopLevelSubmodule.Path)"
        # Write-Host "DEBUG: It would need to start with: $ProjectFolderName/Plugins/"
        if ($TopLevelSubmodule.Path -eq "BuildScripts" -or $TopLevelSubmodule.Path.Contains("/Source/") -or ($ProjectFolderName -ne $null -and $ProjectFolderName -ne "" -and $TopLevelSubmodule.Path.StartsWith("$ProjectFolderName/Plugins/"))) {
            Checkout-Submodule $TargetDirectory $TopLevelSubmodule "$TargetDirectory/.git/modules/$($TopLevelSubmodule.Id)"
            if ($ProjectFolderName -ne $null -and $ProjectFolderName -ne "" -and $TopLevelSubmodule.Path.StartsWith("$ProjectFolderName/Plugins/")) {
                foreach ($ChildSubmodule in (Parse-Submodules "$TargetDirectory/$($TopLevelSubmodule.Path)")) {
                    if ($ChildSubmodule.ExcludeOnMac.Trim() -eq "true" -and $global:IsMacOS) {
                        continue
                    }
                    if ($ChildSubmodule.Path.Contains("/Source/")) {
                        Checkout-Submodule "$TargetDirectory/$($TopLevelSubmodule.Path)" $ChildSubmodule "$TargetDirectory/.git/modules/$($TopLevelSubmodule.Id)/modules/$($ChildSubmodule.Id)"
                    }
                }
            }
        }
    }
}
finally {
    Stop-Section "git-submodules"
}

# Write our .gitcheckout file which tells subsequent calls that we're up-to-date.
Set-Content -Value "$TargetCommit" -Path "$TargetDirectory\.gitcheckout"

exit 0