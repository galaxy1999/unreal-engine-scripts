param(
    [Parameter(Mandatory=$true)][string] $PackageType,
    [Parameter(Mandatory=$true)][string] $PackageTarget,
    [Parameter(Mandatory=$true)][string] $PackagePlatform,
    [Parameter(Mandatory=$true)][string] $PackageConfiguration,
    [Parameter(Mandatory=$true)][string] $SteamAppID,
    [Parameter(Mandatory=$true)][string] $SteamDepotID,
    [Parameter(Mandatory=$false)][string] $SteamChannel,
    [Parameter(Mandatory=$true)][string] $StagedData
)

if ($null -eq $env:STEAM_USERNAME) {
    Write-Error "Missing STEAM_USERNAME environment variable, which is required for Steam deployments. Set this environment variable on the command line or in your build server configuration."
    exit 1
}
if ($null -eq $env:STEAM_STEAMCMD_PATH) {
    Write-Error "Missing STEAM_STEAMCMD_PATH environment variable, which is required for Steam deployments. This must point to the steamcmd.exe executable. Set this environment variable on the command line or in your build server configuration."
    exit 1
}

$TrackedProcess = $null
trap {
    if ($TrackedProcess -ne $null -and -not $TrackedProcess.HasExited) {
        $TrackedProcess.Kill();
    }
    Write-Error $_
    exit 1
}

function Wait-For-Process-Exit($Process) {
    while (!$Process.HasExited) {
        Start-Sleep -Seconds 1
    }
}

Write-Host "Preparing deployment directory..."
$TempDirectory = "$PSScriptRoot\..\Temp\Steam$SteamDepotID"
if (!(Test-Path "$TempDirectory")) {
    New-Item -ItemType Directory -Path $TempDirectory
}
if (Test-Path "$TempDirectory\Data") {
    Remove-Item -Force -Recurse -Path "$TempDirectory\Data"
}
New-Item -ItemType Directory -Path "$TempDirectory\Data"

Write-Host "Copying built data to deployment directory (this might take a while depending on the size of your game)..."
Copy-Item -Recurse -Force -Path "$StagedData\*" -Destination "$TempDirectory\Data\"

Write-Host "Writing steamcmd deployment script..."
Set-Content -Path "$TempDirectory\steam_push.txt" -Value @"
@NoPromptForPassword 1
login $env:STEAM_USERNAME
run_app_build $TempDirectory\steam_app_$SteamAppID.vdf
quit
"@

Write-Host "Writing application VDF..." 
Set-Content -Path "$TempDirectory\steam_app_$SteamAppID.vdf" -Value @"
"appbuild"
{
	"appid" "$SteamAppID"

	"desc" "Automatic deployment by Unreal Engine Scripts"
    
	"preview" "0"

	// Branch name to automatically set live after successful build, none if empty.
	// Note that the 'default' branch can not be set live automatically. That must be done through the App Admin panel.
	"setlive" "$SteamChannel"

	// The following paths can be absolute or relative to location of the script.

	// This directory will be the location for build logs, chunk cache, and intermediate output.
	// The cache stored within this causes future SteamPipe uploads to complete quicker by using diffing.
	//
	// NOTE: for best performance, use a separate disk for your build output. This splits the disk IO workload, letting your content root
	// disk handle the read requests and your output disk handle the write requests. 
	"buildoutput" ".\Cache\"

	// The root of the content folder.
	"contentroot" "$($TempDirectory.TrimEnd("\"))\Data"

	// The list of depots included in this build.
	"depots"
	{
		"$SteamDepotID" "$($TempDirectory.TrimEnd("\"))\steam_depot_$SteamDepotID.vdf"
	}
}
"@

if ($PackagePlatform -eq "Win64") {
    $InstallScript = ""
    if (Test-Path "$TempDirectory\Data\Engine\Extras\Redist\en-us\UE4PrereqSetup_x64.exe") {
        $InstallScript = "`"InstallScript`" `"Engine/Extras/steam_install_prereqs.vdf`""
    }

    Write-Host "Writing depot VDF..."
    Set-Content -Path "$TempDirectory\steam_depot_$SteamDepotID.vdf" -Value @"
"DepotBuildConfig"
{
    "DepotID" "$SteamDepotID"
    "ContentRoot" "."

    "FileMapping"
    {
        "LocalPath" "*"
        "DepotPath" "."
        "recursive" "1"
    }

    "FileExclusion" "Manifest_*.txt"
    "FileExclusion" "*/Saved/*"

    $InstallScript
}
"@

    if (Test-Path "$TempDirectory\Data\Engine\Extras\Redist\en-us\UE4PrereqSetup_x64.exe") {
        Write-Host "Adding prerequisites installation script..."
        Set-Content -Path "$TempDirectory\Data\Engine\Extras\steam_install_prereqs.vdf" -Value @"
"InstallScript"
{
	"Run Process"
	{
		"Unreal Engine Prerequisites"
		{
			"HasRunKey"     "HKEY_CLASSES_ROOT\\Installer\\Dependencies\\{4e242cc8-5e3c-4b08-9d55-dbc62ddd1208}"
			"Process 1"     "%INSTALLDIR%\\Engine\\Extras\\Redist\\en-us\\UE4PrereqSetup_x64.exe"
			"Command 1"     "/quiet /norestart"
			"NoCleanUp"     "1"
		}
	}
}
"@
    } else {
        Write-Warning "No prerequisites installer was found; this Steam deploy will not automatically install prerequisites for the player!"
    }
} else {
    Write-Error "$PackagePlatform isn't supported for Steam deployments yet. Please open an issue."
    exit 1
}

Set-Content -Value "" -Path "$TempDirectory\Empty.txt"
$TrackedProcess = Start-Process `
    -FilePath "$env:STEAM_STEAMCMD_PATH" `
    -ArgumentList @(
        "+runscript",
        "$TempDirectory\steam_push.txt"
    ) `
    -NoNewWindow `
    -PassThru `
    -WorkingDirectory "$TempDirectory" `
    -RedirectStandardInput "$TempDirectory\Empty.txt"
$TrackedProcess.Handle
Wait-For-Process-Exit -Process $TrackedProcess
if ($TrackedProcess.ExitCode -ne 0) {
    Write-Error -Message "Failed to deploy to Steam!"
    exit 1
}

Write-Host "Steam deployment completed successfully."
exit 0
