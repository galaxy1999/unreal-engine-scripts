param(
    # The engine you are targeting. Can be a version number or an absolute path.
    [Parameter(Mandatory=$true)][string] $Engine,
    # The directory which contains the project to test.
    [Parameter(Mandatory=$true)][string] $ProjectRoot,
    # The full path to the .uproject file.
    [Parameter(Mandatory=$true)][string] $ProjectPath,
    # The prefix to use when selecting the tests to run.
    [Parameter(Mandatory=$true)][string] $TestPrefix,
    # Selects how tests will be monitored.
    [ValidateSet("StdOut", "JSON")][string] $TestMonitor = "JSON",
    # The number of automation workers to launch.
    [int] $WorkerCount = 0,
    # The minimum number of automation workers to launch. If you specify this instead of -WorkerCount, the
    # script will try to launch up to MinWorkerCount*2 workers depending on available memory. It assumes
    # each Unreal Engine instance needs around 4GB of non-paged memory.
    [int] $MinWorkerCount = 0,
    # The number of minutes after which the test run should timeout. Defaults to 5 minutes.
    [int] $TimeoutMinutes = 5,
    # If true, displays the starting log messages. If you're using StdOut as the test monitor, these messages are inaccurate due to UE bugs.
    [switch][bool] $DisplayStartingMessages,
    # If true, displays all logs from all workers (which makes it hard to read the test results).
    [switch][bool] $DisplayFullLogs
)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Internal_SetupEnvironment.ps1

$EnginePath = Resolve-EnginePath $Engine

# Import the ThreadJob module.
if ($null -eq (Get-Module -Name ThreadJob)) {
    $NuGetPackageProvider = (Get-PackageProvider -Name NuGet -ErrorAction SilentlyContinue)
    if ($null -eq $NuGetPackageProvider -or $NuGetPackageProvider.Version -lt "2.8.5.201") {
        Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force -Scope CurrentUser -Confirm:$False | Out-Null
    }
    $ThreadJobModule = (Get-InstalledModule -Name ThreadJob -ErrorAction SilentlyContinue)
    if ($null -eq $ThreadJobModule) {
        Install-Module -Name ThreadJob -Force -Confirm:$False -Scope CurrentUser | Out-Null
    }
    Import-Module "$((Get-InstalledModule ThreadJob).InstalledLocation)\ThreadJob.psd1"
}

# Worker count must be at least 1.
if ($WorkerCount -eq 0 -and $MinWorkerCount -eq 0) {
    Write-Error "You must provide either -WorkerCount or -MinWorkerCount."
    exit 1
}
$WorkerCountMessage = ""
if ($WorkerCount -ne 0) {
    if ($WorkerCount -lt 1) {
        $WorkerCount = 1
    }
    $WorkerCountMessage = "Using $WorkerCount workers"
} elseif ($MinWorkerCount -ne 0) {
    $MaxWorkerCount = $MinWorkerCount * 2
    $WorkerCount = $MinWorkerCount
    $AvailableMemory = (Get-CIMInstance Win32_OperatingSystem | Select-Object FreePhysicalMemory).FreePhysicalMemory
    $UnrealEngineMemorySizeKb = (9 * 1024 * 1024)
    $ConsumedMemory = $WorkerCount * $UnrealEngineMemorySizeKb
    while (($AvailableMemory -gt ($ConsumedMemory + $UnrealEngineMemorySizeKb)) -and ($WorkerCount -lt $MaxWorkerCount)) {
        $WorkerCount += 1
        $ConsumedMemory += $UnrealEngineMemorySizeKb
    }
    $AvailableMemoryMb = [Math]::Round($AvailableMemory / 1024)
    $ConsumedMemoryMb = [Math]::Round($ConsumedMemory / 1024)
    $WorkerCountMessage = "Using $WorkerCount workers (${AvailableMemoryMb}MB available, ${ConsumedMemoryMb}MB planned to use)"
}
if ($WorkerCount -ge 10) {
    $SessionLogPrefix = "[         ]"
} else {
    $SessionLogPrefix = "[        ]"
}
Write-Host "$SessionLogPrefix $WorkerCountMessage."

# Generate a session GUID.
$SessionGuid = [guid]::NewGuid().ToString()
$SessionName = "Automation"
if ($null -eq $env:CI_JOB_ID) {
    $SessionName = "Automation$($env:CI_JOB_ID)"
}

# Define the script block for monitoring tests results and emitting them to the log.
$ExecuteTestMonitor = {
    param(
        $ProjectRoot,
        $SessionLogPrefix,
        $DisplayStartingMessages,
        $TestMonitorExitFlagFile
    )

    Write-Host "$SessionLogPrefix Monitoring test results at: $ProjectRoot\Intermediate\TestReport\index.json"
    if (!(Test-Path "$ProjectRoot\Intermediate\TestReport")) {
        New-Item -ItemType Directory -Path "$ProjectRoot\Intermediate\TestReport"
    }

    $global:Map_LastTestResults = @{}
    function Update-TestResults([bool] $FinalChance) {
        $Color_Red = "$([char]27)[5;31m"
        $Color_Green = "$([char]27)[5;32m"
        $Color_Blue = "$([char]27)[5;34m"
        $Color_Cyan = "$([char]27)[5;36m"
        $Color_Reset = "$([char]27)[0m"

        $local:TestResults = Get-Content -Path "$ProjectRoot/Intermediate/TestReport/index.json" -Raw | ConvertFrom-Json
        foreach ($Test in $TestResults.tests) {
            if ($null -eq $global:Map_LastTestResults.Item($Test.fullTestPath)) {
                # We haven't seen this test before (it has just been discovered).
                Write-Host "$SessionLogPrefix [${Color_Cyan}Listing${Color_Reset}] $($Test.fullTestPath)"
                $global:Map_LastTestResults[$Test.fullTestPath] = @{
                    FullTestPath = $Test.fullTestPath;
                    State = "NotRun";
                    Stopwatch = $null;
                    Duration = $null;
                    EventEntries = @();
                }
            }
        }
        foreach ($Test in $TestResults.tests) {
            $local:LastTestResult = $global:Map_LastTestResults.Item($Test.fullTestPath)
            if ($local:LastTestResult.State -eq "NotRun" -and $Test.state -eq "InProcess") {
                if ($null -eq $local:LastTestResult.Stopwatch) {
                    $local:LastTestResult.State = $Test.state;
                    $local:LastTestResult.Stopwatch = [system.diagnostics.stopwatch]::StartNew()
                    if ($DisplayStartingMessages) {
                        Write-Host "$SessionLogPrefix [${Color_Blue}Started${Color_Reset}] $($Test.fullTestPath)"
                    }
                }
            }
            if ($local:LastTestResult.State -ne "Success" -and $Test.state -eq "Success") {
                if ($local:LastTestResult.State -eq "NotRun") {
                    $local:LastTestResult.Duration = 0;
                    if ($DisplayStartingMessages) {
                        Write-Host "$SessionLogPrefix [${Color_Blue}Started${Color_Reset}] $($Test.fullTestPath)"
                    }
                }
                if ($null -ne $local:LastTestResult.Stopwatch) {
                    $local:LastTestResult.Stopwatch.Stop()
                    $local:LastTestResult.Duration = $local:LastTestResult.Stopwatch.Elapsed.TotalSeconds;
                    $local:LastTestResult.Stopwatch = $null
                }
                $local:LastTestResult.State = $Test.state;
                $local:LastTestResult.EventEntries = $Test.entries;
                Write-Host "$SessionLogPrefix [${Color_Green}Success${Color_Reset}] $($Test.fullTestPath) ($($local:LastTestResult.Duration) secs)"
            }
            if ($local:LastTestResult.State -ne "Fail" -and $Test.state -eq "Fail") {
                if ($local:LastTestResult.State -eq "NotRun") {
                    $local:LastTestResult.Duration = 0;
                    Write-Host "$SessionLogPrefix [${Color_Blue}Started${Color_Reset}] $($Test.fullTestPath)"
                }
                if ($null -ne $local:LastTestResult.Stopwatch) {
                    $local:LastTestResult.Stopwatch.Stop()
                    $local:LastTestResult.Duration = $local:LastTestResult.Stopwatch.Elapsed.TotalSeconds;
                    $local:LastTestResult.Stopwatch = $null
                }
                $local:LastTestResult.State = $Test.state;
                $local:LastTestResult.EventEntries = $Test.entries;
                foreach ($Entry in $Test.entries) {
                    if ($null -ne $Entry.event) {
                        Write-Host "$SessionLogPrefix [Event  ] $($Entry.event.type): $($Entry.event.message)"
                    }
                }
                Write-Host "$SessionLogPrefix [${Color_Red}Failure${Color_Reset}] $($Test.fullTestPath) ($($local:LastTestResult.Duration) secs)"
            }
            if ($Test.state -ne "Success" -and $Test.state -ne "Fail" -and $local:FinalChance) {
                # This test didn't run or was still running at the time the monitor shut down. It counts as a failure.
                $local:LastTestResult.State = "Fail";
                Write-Host "$SessionLogPrefix [${Color_Red}Missing${Color_Reset}] $($Test.fullTestPath)"
            }
        }

        if ($local:FinalChance) {
            $local:IsOverallFailure = $false
            foreach ($Key in $global:Map_LastTestResults.Keys) {
                if ($global:Map_LastTestResults.Item($Key).State -ne "Success") {
                    $local:IsOverallFailure = $true
                }
            }
            return !$local:IsOverallFailure
        }
    }

    $Watcher = New-Object IO.FileSystemWatcher "$ProjectRoot/Intermediate/TestReport", "index.json" -Property @{ 
        IncludeSubdirectories = $false
        EnableRaisingEvents = $true
    }
    $OnChange = Register-ObjectEvent $Watcher Changed -Action {
        Update-TestResults $false
    }
    $OnCreated = Register-ObjectEvent $Watcher Created -Action {
        Update-TestResults $false
    }

    while (!(Test-Path "$TestMonitorExitFlagFile")) {
        Start-Sleep -Milliseconds 500
    }

    Unregister-Event -SubscriptionId $OnChange.Id
    Unregister-Event -SubscriptionId $OnCreated.Id

    if (Update-TestResults $true) {
        # Success
        Set-Content -Path "$TestMonitorExitFlagFile" -Value "pass"
    } else {
        # Failure
        Set-Content -Path "$TestMonitorExitFlagFile" -Value "fail"
    }

    Write-Host "$SessionLogPrefix Finished test results at: $ProjectRoot\Intermediate\TestReport\index.json"
}

# Define the script block for executing Unreal Engine as a PowerShell job.
$ExecuteUnrealWorker = {
    param(
        $SessionGuid,
        $SessionName,
        $TestPrefix,
        $TestMonitor,
        $WorkerNum,
        $WorkerLogPrefix,
        $SessionLogPrefix,
        $IsPrimaryWorker,
        $DisplayStartingMessages,
        $DisplayFullLogs,
        $EnginePath,
        $ProjectRoot,
        $ProjectPath
    )

    try {
        $ArgList = @(
            "$ProjectPath"
            "-skipcompile",
            "-nosplash",
            "-Unattended",
            "-NullRHI",
            "-NOSOUND",
            "-stdout",
            "-FullStdOutLogOutput",
            "-stompmalloc",
            "-poisonmallocproxy",
            "-purgatorymallocproxy",
            "-SessionId=$SessionGuid",
            "-SessionName=$SessionName",
            "-AutomationWorkerNum=$WorkerNum",
            "-abslog=$ProjectRoot\Saved\Logs\Worker$WorkerNum.log",
            "-TestExit=`"Found 0 Automation Tests`"+`"Automation Test Queue Empty`"",
            # Prevents errors from other workers relayed onto the primary worker from causing tests on
            # the primary worker to unexpectedly fail.
            "-ini:Engine:[Core.Log]:LogAutomationController=NoLogging"
        )
        if ($IsPrimaryWorker) {
            Write-Host "$WorkerLogPrefix Launching primary worker for automation tests..."
            $ArgList += "-ExecCmds=`"Automation RunTests $TestPrefix`""
            $ArgList += "-ReportExportPath=`"$ProjectRoot/Intermediate/TestReport`""
            if (Test-Path "$EnginePath\Engine\Binaries\Win64\UnrealEditor-Cmd.exe") {
                # In UE5, this is required to get index.json written as we go.
                $ArgList += "-ResumeRunTest"
            }
        } else {
            Write-Host "$WorkerLogPrefix Launching secondary worker for automation tests..."
        }

        $Regex_FoundAutomationTests = [regex]::New("LogAutomationCommandLine: ([^:]+): Found ([0-9]+) automation tests based on '([^']+)'")
        $Regex_ListedAutomationTest = [regex]::New("LogAutomationCommandLine: ([^:]+): \s*(.+)")
        $Regex_TestStart = [regex]::New("LogAutomationController: ([^:]+): Test Started\. Name=\{([^\}]+)\}")
        $Regex_TestResult = [regex]::New("LogAutomationController: ([^:]+): Test Completed\. Result=\{([^\}]+)\} Name=\{([^\}]+)\} Path=\{([^\}]+)\}")
        $Regex_TestQueueEmpty = [regex]::New("LogAutomationCommandLine: ([^:]+): ...Automation Test Queue Empty [0-9]+ tests performed.")

        $State_FindingAutomationTests = $false
        $State_HasListedAutomationTests = $false
        $State_AutomationTestsToGo = 0
        $State_CachedAutomationTestList = @()

        $Color_Red = "$([char]27)[5;31m"
        $Color_Green = "$([char]27)[5;32m"
        $Color_Blue = "$([char]27)[5;34m"
        $Color_Cyan = "$([char]27)[5;36m"
        $Color_Reset = "$([char]27)[0m"

        $Map_NamesToFullNames = @{}
        $Map_FullNamesToGotResults = @{}
        $DebugLines = @()

        $ShouldRestart = $false
        $TestsStopwatch = $null
        do {
            $ShouldRestart = $false

            $StartupStopwatch = [system.diagnostics.stopwatch]::StartNew()
            $DiscoveryStopwatch = $null
            $TestsStopwatch = $null
            $HasStartedDiscovery = $false
            $FinishedStartup = $false
            $Cmd = "$EnginePath\Engine\Binaries\Win64\UE4Editor-Cmd.exe"
            if (Test-Path "$EnginePath\Engine\Binaries\Win64\UnrealEditor-Cmd.exe") {
                $Cmd = "$EnginePath\Engine\Binaries\Win64\UnrealEditor-Cmd.exe"
            }
            & "$Cmd" $ArgList | % {
                if ($_ -eq $null) {
                    return
                }

                $Line = $_.Trim()

                if ($DisplayFullLogs) {
                    Write-Host "$WorkerLogPrefix $Line"
                }
                $DebugLines += "$WorkerLogPrefix $Line"
                @{
                    MessageType = "DebugLine";
                    DebugLine = "$WorkerLogPrefix $Line";
                }

                if ($Line.Contains("LogAutomationWorker: Received FindWorkersMessage")) {
                    if (!$FinishedStartup) {
                        $StartupStopwatch.Stop()
                        $FinishedStartup = $true
                        Write-Host "$WorkerLogPrefix Worker started in $($StartupStopwatch.Elapsed.TotalSeconds) seconds."
                        if ($IsPrimaryWorker -and $TestMonitor -eq "StdOut") {
                            $DiscoveryStopwatch = [system.diagnostics.stopwatch]::StartNew()
                        }
                    }
                }
                if ($TestMonitor -eq "StdOut") {
                    if ($Regex_TestQueueEmpty.IsMatch($Line)) {
                        if ($TestsStopwatch -ne $null) {
                            $TestsStopwatch.Stop()
                            Write-Host "$SessionLogPrefix Test execution finished in $($TestsStopwatch.Elapsed.TotalSeconds) seconds."
                        }
                        Write-Host "$WorkerLogPrefix The test automation queue is now empty. This process should exit shortly."
                        @{
                            MessageType = "TestExecutionComplete";
                        }
                    }
                    if (!$State_HasListedAutomationTests) {
                        $R = $Regex_FoundAutomationTests.Match($Line)
                        if ($R.Success) {
                            $State_AutomationTestsToGo = [int]::Parse($R.Groups[2].Value)
                            $State_FindingAutomationTests = $true
                            $State_HasListedAutomationTests = $true
                            return
                        }
                    }
                    if ($State_FindingAutomationTests) {
                        $R = $Regex_ListedAutomationTest.Match($Line)
                        if ($R.Success) {
                            if ($DiscoveryStopwatch -ne $null -and !$HasStartedDiscovery) {
                                $DiscoveryStopwatch.Stop()
                                $HasStartedDiscovery = $true
                                Write-Host "$WorkerLogPrefix Worker discovered tests in $($DiscoveryStopwatch.Elapsed.TotalSeconds) seconds."
                            }
                            $Name = $R.Groups[2].Value
                            $State_CachedAutomationTestList += $Name
                            $State_AutomationTestsToGo--
                            Write-Host "$WorkerLogPrefix [${Color_Cyan}Listing${Color_Reset}] $Name"
                            $Map_FullNamesToGotResults[$Name] = $false
                            $NameSplit = $Name.Split(".")
                            $NameSplit = $NameSplit[$NameSplit.Length - 1]
                            $Map_NamesToFullNames[$NameSplit] = $Name
                            if ($State_AutomationTestsToGo -eq 0) {
                                $State_FindingAutomationTests = $false
                                if ($DisplayStartingMessages) {
                                    Write-Host "$WorkerLogPrefix Due to an Unreal Engine bug, `"Started`" messages will have incorrect test names when the tests are starting in parallel."
                                }
                                if ($IsPrimaryWorker) {
                                    $TestsStopwatch = [system.diagnostics.stopwatch]::StartNew()
                                }
                                @{
                                    MessageType = "FinishedDiscovery";
                                }
                                
                                # Emit test discovery to pipeline
                                <#
                                @{
                                    MessageType = "TestDiscovery",
                                    DiscoveredTests = $State_CachedAutomationTestList
                                }
                                #>
                            }
                        }
                    }
                    if ($State_HasListedAutomationTests -and !$State_FindingAutomationTests) {
                        $R = $Regex_TestStart.Match($Line)
                        if ($R.Success) {
                            $Name = $R.Groups[2].Value
                            $NameFull = $Map_NamesToFullNames[$Name]
                            if ($DisplayStartingMessages) {
                                Write-Host "$SessionLogPrefix [${Color_Blue}Started${Color_Reset}] $NameFull"
                            }
                        }
                        $R = $Regex_TestResult.Match($Line)
                        if ($R.Success) {
                            $Result = $R.Groups[2].Value
                            $Name = $R.Groups[3].Value
                            $Path = $R.Groups[4].Value
                            if ($Result -eq "Passed") {
                                Write-Host "$SessionLogPrefix [${Color_Green}Success${Color_Reset}] $Path"
                            } else {
                                Write-Host "$SessionLogPrefix [${Color_Red}Failure${Color_Reset}] $Path"
                            }
                            $Map_FullNamesToGotResults[$Path] = $true
                        }
                    }
                }
            }

            $ShouldRestart = $false
            $RestartReason = ""
            if ($LastExitCode -eq 3) {
                foreach ($DebugLine in $DebugLines) {
                    if ($DebugLine.Contains("FSourceFileDatabase::UpdateIfNeeded()")) {
                        $ShouldRestart = $true
                        $RestartReason = "handle the FSourceFileDatabase crash bug"
                        break
                    }
                    if ($DebugLine.Contains("(RequestedTestFilter & EAutomationTestFlags::FilterMask) != EAutomationTestFlags::SmokeFilter")) {
                        $ShouldRestart = $true
                        $RestartReason = "handle the smoke test filter crash bug"
                        break
                    }
                }
            }
            
            if (!$ShouldRestart) {
                if ($LastExitCode -eq 0) {
                    $Map_FullNamesToGotResults.Keys | % {
                        if ($Map_FullNamesToGotResults.Item($_) -eq $false) {
                            Write-Host "$SessionLogPrefix [${Color_Red}Failure${Color_Reset}] $_"
                            # todo: This should cause the result to be a failure.
                        }
                    }
                }
                Write-Host "$WorkerLogPrefix The process exited with exit code $LastExitCode."
                if ($LastExitCode -ne 0) {
                    foreach ($DebugLine in $DebugLines) {
                        Write-Host $DebugLine
                    }
                }
            } else {
                Write-Host "$WorkerLogPrefix Restarting the process to $RestartReason."
            }
        } while ($ShouldRestart)
    } finally {
    }
}

# Remove any test results on disk.
if (Test-Path "$ProjectRoot/Intermediate/TestReport") {
    Remove-Item -Force -Recurse "$ProjectRoot/Intermediate/TestReport"
}
if (Test-Path "$ProjectRoot/Saved/Logs") {
    Remove-Item -Force -Recurse "$ProjectRoot/Saved/Logs"
}

# Create the path that will be used to determine when the test monitor should exit.
$TestMonitorExitFlagFile = "$ProjectRoot\Intermediate\TestReport\exit.flag"
if (Test-Path "$TestMonitorExitFlagFile") {
    Remove-Item -Force "$TestMonitorExitFlagFile"
}

# Launch all of the Unreal Engine jobs.
$Jobs = @()
for ($i = 1; $i -le $WorkerCount; $i++) {
    $IsPrimaryWorker = $i -eq 1
    if ($WorkerCount -ge 10) {
        $WorkerLogPrefix = "[Worker $($i.ToString().PadLeft(2))]"
    } else {
        $WorkerLogPrefix = "[Worker $i]"
    }
    $Jobs += Start-ThreadJob -StreamingHost $Host -ScriptBlock $ExecuteUnrealWorker -ArgumentList @(
        $SessionGuid, 
        $SessionName, 
        $TestPrefix,
        $TestMonitor,
        $i,
        $WorkerLogPrefix,
        $SessionLogPrefix,
        $IsPrimaryWorker,
        $DisplayStartingMessages,
        $DisplayFullLogs,
        $EnginePath,
        $ProjectRoot,
        $ProjectPath)
}
if ($TestMonitor -eq "JSON") {
    $local:TestMonitorExitFlagDirectory = [System.IO.Path]::GetDirectoryName($TestMonitorExitFlagFile)
    if (!(Test-Path $local:TestMonitorExitFlagDirectory)) {
        New-Item -Path $local:TestMonitorExitFlagDirectory -ItemType Directory
    }
    $Jobs += Start-ThreadJob -StreamingHost $Host -ScriptBlock $ExecuteTestMonitor -ArgumentList @(
        $ProjectRoot,
        $SessionLogPrefix,
        $DisplayStartingMessages,
        $TestMonitorExitFlagFile
    )
}

# Wait for the primary job to complete. The secondary workers will not automatically exit when
# their test queues are empty.
$local:DidFailFromMonitor = $false
if ($TestMonitor -eq "JSON") {
    Wait-Job -Job $Jobs[0] -Timeout ($TimeoutMinutes * 60) | Out-Null
    Set-Content -Path "$TestMonitorExitFlagFile" -Value "exiting"
    Wait-Job -Job $Jobs[$Jobs.Length-1] -Timeout (5 * 60) | Out-Null
    $local:DidFailFromMonitor = ((Get-Content -Raw -Path "$TestMonitorExitFlagFile").Trim() -eq "fail")
} else {
    Wait-Job -Job $Jobs[0] -Timeout ($TimeoutMinutes * 60) | Out-Null
}
$IsTimedOut = $false
if ($Jobs[0].State -ne "Failed" -and $Jobs[0].State -ne "Completed") {
    $IsTimedOut = $true
}
foreach ($Job in $Jobs) {
    $Job
}
foreach ($Job in $Jobs) {
    if ($Job.State -ne "Failed" -and $Job.State -ne "Completed") {
        $Job | Stop-Job | Out-Null
    }
}
if ($IsTimedOut -and $TestMonitor -eq "StdOut") {
    $DidFinishDiscovery = $false
    $DidNotActuallyTimeOut = $false
    foreach ($Job in Receive-Job $Jobs[0] -Keep) {
        if ($Job.MessageType -eq "FinishedDiscovery") {
            $DidFinishDiscovery = $true
        }
        if ($Job.MessageType -eq "TestExecutionComplete") {
            $DidNotActuallyTimeOut = $true
        }
    }
    if (!$DidNotActuallyTimeOut) {
        if (!$DidFinishDiscovery) {
            Write-Host "$SessionLogPrefix Automation tests timed out (took longer than $TimeoutMinutes minutes), but didn't even discover tests."
            Receive-Job $Jobs[0] -Keep | % {
                if ($_.MessageType -eq "DebugLine") {
                    Write-Host $($_.DebugLine)
                }
            }
        }
        Write-Error "Automation tests timed out (took longer than $TimeoutMinutes minutes). Try increasing the number of automation workers to reduce the test run time."
        exit 1
    }
}
if ($IsTimedOut -and $TestMonitor -eq "JSON") {
    Write-Error "Automation tests timed out (took longer than $TimeoutMinutes minutes). Try increasing the number of automation workers to reduce the test run time."
    exit 1
}
if ($local:DidFailFromMonitor) {
    Write-Error "One or more automation tests either failed or did not run."
    exit 1
}
if ($TestMonitor -eq "JSON" -and !(Test-Path "$ProjectRoot\Intermediate\TestReport\index.json")) {
    Write-Error "Automation tests didn't generate an index.json file, so this test run can not have completed successfully."
    exit 1
}

exit 0