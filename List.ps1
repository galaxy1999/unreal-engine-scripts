param()

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Lib\Internal_SetupEnvironment.ps1

$local:BuildConfig = Get-BuildConfig

Write-Output "There are $($local:BuildConfig.Distributions.Length) distributions:"
foreach ($local:Distribution in $local:BuildConfig.Distributions) {
    Write-Output $local:Distribution.Name
}

exit 0