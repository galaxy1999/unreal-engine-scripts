param(
    # The engine you are targeting. Can be a version number or an absolute path.
    [Parameter(Mandatory = $false)][string] $Engine,
    # The path to the engine for builds that run on macOS machines. Not required
    # unless you're building either Mac or IOS platforms.
    [Parameter(Mandatory = $false)][string] $MacEnginePath,
    # The distribution name, as it appears in BuildConfig.json.
    [Parameter(Mandatory = $true)][string] $Distribution,
    # The path to output the .gitlab-ci.yml file to.
    [Parameter(Mandatory = $true)][string] $GitLabYamlPath,
    # The prefix for the GitLab agent tag for running builds. If this is "abc", then jobs will be defined
    # to use the tags abc-windows and abc-mac.
    [Parameter(Mandatory = $true)][string] $GitLabAgentTagPrefix,
    # The absolute path to shared storage on Windows. Must start with a drive letter (like X:\). Must have a trailing path.
    [Parameter(Mandatory = $true)][string] $WindowsSharedStorageAbsolutePath,
    # The absolute path to shared storage on macOS. If present, must have a trailing path. If not specified, Backblaze B2 will be used as the artifact transport.
    [Parameter(Mandatory = $false)][string] $MacSharedStorageAbsolutePath,
    # The path to the Linux toolchain on Windows.
    [string] $WindowsLinuxToolchainPath,
    # If true, executes the build phase.
    [switch][bool] $ExecuteBuild = $true,
    # If true, executes the test phase.
    [switch][bool] $ExecuteTests = $true,
    # If true, executes the deployment phase.
    [switch][bool] $ExecuteDeployment = $true,
    # If true, the working directory will be mapped as a drive on Windows to reduce the length of file paths. Engine builds
    # always map drives, but you can optionally enable this for project or plugin builds.
    [switch][bool] $MapDriveForShorterPaths
)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Lib\Internal_SetupEnvironment.ps1

if ((Get-IsProject)) {
    if ($Engine -eq $null -or $Engine.Trim() -eq "") {
        Write-Error "-Engine parameter is mandatory when not building the engine."
        exit 1
    }
    & "$PSScriptRoot\Lib\Generate_Project.ps1" `
        -Engine "$Engine" `
        -MacEnginePath "$MacEnginePath" `
        -Distribution "$Distribution" `
        -GitLabYamlPath "$GitLabYamlPath" `
        -GitLabAgentTagPrefix "$GitLabAgentTagPrefix" `
        -WindowsSharedStorageAbsolutePath "$WindowsSharedStorageAbsolutePath" `
        -MacSharedStorageAbsolutePath:$MacSharedStorageAbsolutePath `
        -WindowsLinuxToolchainPath:$WindowsLinuxToolchainPath `
        -ExecuteBuild:$ExecuteBuild `
        -ExecuteTests:$ExecuteTests `
        -ExecuteDeployment:$ExecuteDeployment `
        -MapDriveForShorterPaths:$MapDriveForShorterPaths
}
elseif ((Get-IsEngine)) {
    & "$PSScriptRoot\Lib\Generate_Engine.ps1" `
        -Distribution "$Distribution" `
        -GitLabYamlPath "$GitLabYamlPath" `
        -GitLabAgentTagPrefix "$GitLabAgentTagPrefix" `
        -WindowsSharedStorageAbsolutePath "$WindowsSharedStorageAbsolutePath" `
        -MacSharedStorageAbsolutePath:$MacSharedStorageAbsolutePath `
        -WindowsLinuxToolchainPath:$WindowsLinuxToolchainPath
}
else {
    if ($Engine -eq $null -or $Engine.Trim() -eq "") {
        Write-Error "-Engine parameter is mandatory when not building the engine."
        exit 1
    }
    & "$PSScriptRoot\Lib\Generate_Plugin.ps1" `
        -Engine "$Engine" `
        -MacEnginePath "$MacEnginePath" `
        -Distribution "$Distribution" `
        -GitLabYamlPath "$GitLabYamlPath" `
        -GitLabAgentTagPrefix "$GitLabAgentTagPrefix" `
        -WindowsSharedStorageAbsolutePath "$WindowsSharedStorageAbsolutePath" `
        -MacSharedStorageAbsolutePath:$MacSharedStorageAbsolutePath `
        -WindowsLinuxToolchainPath:$WindowsLinuxToolchainPath `
        -ExecuteBuild:$ExecuteBuild `
        -ExecuteTests:$ExecuteTests `
        -ExecuteDeployment:$ExecuteDeployment `
        -MapDriveForShorterPaths:$MapDriveForShorterPaths
}

exit $LastExitCode