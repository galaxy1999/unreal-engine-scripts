param()

$ErrorActionPreference = "Stop"

# Determine project root.
$ProjectRoot = (Resolve-Path -Path $PSScriptRoot\..).Path

# Load project configuration.
if (!(Test-Path $ProjectRoot\BuildConfig.json)) {
    Write-Error "Please create BuildConfig.json in your project root."
    return
}
$BuildConfig = Get-Content -Raw $ProjectRoot\BuildConfig.json | ConvertFrom-Json
if ($null -eq $BuildConfig.Copyright -or $null -eq $BuildConfig.Copyright.Header) {
    Write-Error "To use the UpdateCopyrightHeaders.ps1 script, you must set the CopyrightHeader in BuildConfig.json"
    return
}
$CopyrightHeader = $BuildConfig.Copyright.Header
if ($CopyrightHeader.IndexOf("%Y") -ne -1) {
    # Remove the year placement from the actual source code. We only add the year
    # when packaging for Marketplace Guidelines.
    $CopyrightHeader = ([regex]"\s*\%Y\s*").Replace($CopyrightHeader, "")
}

$ExcludePaths = @()
if ($null -ne $BuildConfig.Copyright.ExcludePaths) {
    $ExcludePaths = $BuildConfig.Copyright.ExcludePaths | ForEach-Object {
        [System.IO.Path]::Combine($ProjectRoot, $_).Replace("/", [System.IO.Path]::DirectorySeparatorChar).Replace("\", [System.IO.Path]::DirectorySeparatorChar)
    }
}

Write-Output "Updating copyright header to: $CopyrightHeader"

function Get-IsFileExcluded($TargetPath) {
    foreach ($ExcludePath in $ExcludePaths) {
        if ($TargetPath.StartsWith($ExcludePath)) {
            return $true;
        }
    }
    return $false;
}

foreach ($File in (Get-ChildItem -Recurse -Path $ProjectRoot | Where-Object { 
    $_ -ne $null -and
    (
        $_.Name.EndsWith(".h") -or 
        $_.Name.EndsWith(".hpp") -or 
        $_.Name.EndsWith(".cs") -or 
        $_.Name.EndsWith(".c") -or 
        $_.Name.EndsWith(".cpp")
    ) -and !(
        $_.FullName.Substring($ProjectRoot.Length).Contains("Binaries") -or 
        $_.FullName.Substring($ProjectRoot.Length).Contains("Intermediate") -or 
        $_.FullName.Substring($ProjectRoot.Length).Contains("BuildScripts") -or 
        $_.FullName.Substring($ProjectRoot.Length).Contains("Plugins") -or
        (Get-IsFileExcluded $_.FullName)
    ) }))
{
    $Name = $File.Name
    Write-Output "Updating $Name..."

    $RawContent = Get-Content $File.FullName -Raw
    if ($null -eq $RawContent) {
        continue
    }
    $Content = [System.Linq.Enumerable]::ToList(($RawContent).Split("`n"))
    while ($Content.Count -gt 0 -and ($Content[0].StartsWith("// ") -or [System.String]::IsNullOrWhiteSpace($Content[0]))) {
        $Content.RemoveAt(0) | Out-Null
    }
    $Content.Insert(0, "");
    $Content.Insert(0, "// $CopyrightHeader");
    while ($Content.Count -gt 0 -and [System.String]::IsNullOrWhiteSpace($Content[$Content.Count - 1])) {
        $Content.RemoveAt($Content.Count - 1) | Out-Null
    }
    for ($i = 0; $i -lt $Content.Count; $i++) {
        $Content[$i] = $Content[$i].TrimEnd()
    }
    $JoinedContent = [System.String]::Join("`n", $Content)
    Set-Content -Path $File.FullName -Value $JoinedContent -NoNewLine
}