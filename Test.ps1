param(
    # The engine you are targeting. Can be a version number or an absolute path.
    [Parameter(Mandatory=$true)][string] $Engine,
    # The distribution name, as it appears in BuildConfig.json.
    [Parameter(Mandatory=$true)][string] $Distribution
)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Lib\Internal_SetupEnvironment.ps1

if ((Get-IsProject)) {
    & "$PSScriptRoot\Lib\Execute_Project.ps1" `
        -Engine "$Engine" `
        -Distribution "$Distribution" `
        -ExecuteBuild:$false `
        -ExecuteTests:$true
} else {
    & "$PSScriptRoot\Lib\Execute_Plugin.ps1" `
        -Engine "$Engine" `
        -Distribution "$Distribution" `
        -ExecuteBuild:$false `
        -ExecuteTests:$true `
        -Allow2019
}

exit $LastExitCode