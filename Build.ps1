param(
    # The engine you are targeting. Can be a version number or an absolute path.
    [Parameter(Mandatory=$false)][string] $Engine,
    # The distribution name, as it appears in BuildConfig.json.
    [Parameter(Mandatory=$true)][string] $Distribution,
    # If true, runs the tests after running a build.
    [switch][bool] $ThenTest,
    # If true, runs the deployment after running a build.
    [switch][bool] $ThenDeploy,
    # If true, turns on IWYU and -StrictIncludes. This is much more time consuming, but required to pass Marketplace submission.
    [switch][bool] $StrictIncludes,
    # If true, allows Visual Studio 2019 to be used to build plugins. Has no effect for projects.
    [switch][bool] $Allow2019 = $true,
    # If true, the working directory will be mapped as a drive on Windows to reduce the length of file paths. Engine builds
    # always map drives, but you can optionally enable this for project or plugin builds.
    [switch][bool] $MapDriveForShorterPaths
)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Lib\Internal_SetupEnvironment.ps1

if ((Get-IsProject)) {
    if ($Engine -eq $null -or $Engine.Trim() -eq "") {
        Write-Error "-Engine parameter is mandatory when not building the engine."
        exit 1
    }
    & "$PSScriptRoot\Lib\Execute_Project.ps1" `
        -Engine "$Engine" `
        -Distribution "$Distribution" `
        -ExecuteBuild `
        -ExecuteTests:$ThenTest `
        -ExecuteDeployment:$ThenDeploy `
        -StrictIncludes:$StrictIncludes `
        -MapDriveForShorterPaths:$MapDriveForShorterPaths
} elseif ((Get-IsEngine)) {
    & "$PSScriptRoot\Lib\Execute_Engine.ps1" `
        -Distribution "$Distribution" `
        -Allow2019:$Allow2019
} else {
    if ($Engine -eq $null -or $Engine.Trim() -eq "") {
        Write-Error "-Engine parameter is mandatory when not building the engine."
        exit 1
    }
    & "$PSScriptRoot\Lib\Execute_Plugin.ps1" `
        -Engine "$Engine" `
        -Distribution "$Distribution" `
        -ExecuteBuild `
        -ExecuteTests:$ThenTest `
        -ExecuteDeployment:$ThenDeploy `
        -StrictIncludes:$StrictIncludes `
        -Allow2019:$Allow2019 `
        -MapDriveForShorterPaths:$MapDriveForShorterPaths
}

exit $LastExitCode