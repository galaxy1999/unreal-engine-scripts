using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationTool;
using UnrealBuildTool;
using Gauntlet;
using System.IO;
using System.IO.Compression;

namespace UE4Game
{
    public class MyGauntletTest : UnrealTestNode<UE4TestConfig>
    {
        public MyGauntletTest(UnrealTestContext InContext) : base(InContext)
        {
        }

        public override UE4TestConfig GetConfiguration()
        {
            UE4TestConfig Config = base.GetConfiguration();

            // Create four Win64 roles, with one as the listen server.
            List<UnrealTestRole> Roles = Config.RequireRoles(UnrealTargetRole.Client, UnrealTargetPlatform.Win64, 4).ToList();
            var ServerRole = Roles[0];
            var ClientRole1 = Roles[1];
            var ClientRole2 = Roles[2];
            var ClientRole3 = Roles[3];

            // Example: Use extra command line arguments to activate tests in your project. You can look at the Replicated
            // Sublevel Instances example project on how to implement this.
            //
            // The -testmyserver and -testmyclient flags below are custom flags that are detected in blueprints
            // with the "Has Launch Option" node. This allows the game to activate special testing logic when it's
            // launched through Gauntlet.
            //
            // For Gauntlet tests, you need to make sure all of the created roles exit normally before MaxDuration
            // is elapsed.

            // Set up the listen server.
            ServerRole.MapOverride = "/Game/DemoWorld?listen";
            ServerRole.CommandLine += " -testmyserver -nullrhi -NOSOUND";

            // Set up the clients.
            ClientRole1.MapOverride = "127.0.0.1:7777";
            ClientRole1.CommandLine += " -testmyclient -nullrhi -NOSOUND";
            ClientRole2.MapOverride = "127.0.0.1:7777";
            ClientRole2.CommandLine += " -testmyclient -nullrhi -NOSOUND";
            ClientRole3.MapOverride = "127.0.0.1:7777";
            ClientRole3.CommandLine += " -testmyclient -nullrhi -NOSOUND";

            // Launch an Android device as a client as well.
            // todo: We currently need to find a suitable Android N device.
            //UnrealTestRole AndroidRole = Config.RequireRole(UnrealTargetRole.Client, UnrealTargetPlatform.Android);
            //AndroidRole.MapOverride = "<todo find IP>";
            //AndroidRole.CommandLine += " -testmyclient";

            // 1 minute (60 seconds)
            Config.MaxDuration = 60;

            return Config;
        }
    }
}