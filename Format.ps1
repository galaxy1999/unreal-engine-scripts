param([switch] $CheckOnly, [string] $Path)

$global:Redpoint_PublicScriptWasCalled = $true
. $PSScriptRoot\Lib\Internal_SetupEnvironment.ps1

if ($env:CI -eq "true") {
    $CheckOnly = $true
}

. $PSScriptRoot\Lib\ClangFormat.ps1 -CheckOnly:$CheckOnly -OverridePath:$Path